<?php

return [

  /*
  |--------------------------------------------------------------------------
  | oAuth Config
  |--------------------------------------------------------------------------
  */

  /**
   * Storage
   */
    'storage' => 'Session',

  /**
   * Consumers
   */
    'consumers' => [

        'Facebook' => [
            'client_id'     => '',
            'client_secret' => '',
            'scope'         => [],
        ],
        'Linkedin' => [
            'client_id'     => '77k6jfxizx41fz',
            'client_secret' => 'bIprQrB6xlRapYmP',
        ],
        'Twitter' => [
            'client_id'     => '',
            'client_secret' => '',
        ],
        'Google' => [
            'client_id'     => '',
            'client_secret' => '',
            'scope'         => ['userinfo_email', 'userinfo_profile'],
        ],

    ]

];