set :format, :pretty
set :log_level, :error
set :stages,        %w(production staging)
set :default_stage, "staging"
set :default_run_options, {:pty => true}
set :application, "Hoojobs"
set :repo_url,  "git@bitbucket.org:socialsnack/job-board.git"
set :file_permissions_roles, :all
set :file_permissions_paths, ['public/uploads','storage/app','storage/framework','storage/framework/cache','storage/framework/sessions','storage/framework/views','storage/logs']
set :file_permissions_chmod_mode, "0777"
set :ssh_options, {
  forward_agent: true,
  paranoid: true,
  keys: "~/.ssh/id_rsa"
}
set :keep_releases, 3
set :tmp_dir, "/var/www/vhosts/hoojobs.com/tmp/"


namespace :composer do
 desc "Running Composer Self-Update"
 task :update do
 puts "--> Composer SelfUpdate......"
   on roles(:app), in: :sequence, wait: 5 do
     if test "[", "-e", "/usr/local/bin/composer", "]"
       execute :composer, "self-update"
     else
       execute :curl, "-sS", "https://getcomposer.org/installer", "|", :php
       execute :mv, "composer.phar", "/usr/local/bin/composer"
     end
   end
   puts "Done".green
 end

 desc "Running Composer Install"
 task :install do
 puts "--> Composer Update dependencies......"
   on roles(:app), in: :sequence, wait: 5 do
     within release_path do
       execute :composer, "update --no-dev --quiet" # install dependencies
     end
   end
   puts "Done".green
 end
end

namespace :laravel do

 desc "Setup Laravel folder permissions"
   task :permissions do
   puts "--> Setup Laravel folder permissions....."
     on roles(:app), in: :sequence, wait: 5 do
       within release_path do
         execute :chmod, "u+x artisan"
         execute :php, "artisan cache:clear"
         execute :chmod, "-R 777 #{release_path}/storage/app"
         execute :chmod, "-R 777 #{release_path}/storage/framework/cache"
         execute :chmod, "-R 777 #{release_path}/storage/framework/sessions"
         execute :chmod, "-R 777 #{release_path}/storage/framework/views"
         execute :chmod, "-R 777 #{release_path}/storage/logs"
         execute :composer, "dump-autoload"
      end
    end
    puts "Done".green
  end

  desc "Optimize Laravel Class Loader"
  task :optimize do
  puts "--> Optimize Laravel Class Loader....."
    on roles(:app), in: :sequence, wait: 5 do
      within release_path do
        execute :php, "artisan clear-compiled"
        execute :php, "artisan optimize"
      end
    end
    puts "Done".green
  end

  desc "Run Laravel Artisan migrate task."
  task :migrate do
  puts "--> Run Laravel artisan migrate tasks....."
    on roles(:app), in: :sequence, wait: 5 do
      within release_path do
        execute :php, "artisan migrate"
      end
    end
  end
  puts "Done".green
end

before "deploy:updated", "deploy:set_permissions:chmod"
#after "deploy", "composer:update"
after "deploy", "composer:install"
#after "deploy", "laravel:permissions"
after "deploy", "laravel:optimize"
after "deploy", "laravel:migrate"