role :app, %w{hoojobs@hoojobs.com}
set :deploy_to, "/var/www/vhosts/hoojobs.com/staging.hoojobs.com"
set :ssh_options, {
    auth_methods: %w(password),
    password: "g$zkj;s3C1:d"
}
set :branch, 'staging'