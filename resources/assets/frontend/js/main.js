/* Main Hoojobs JS file */

jQuery.extend({
  highlight: function (node, re, nodeName, className) {
    if (node.nodeType === 3) {
      var match = node.data.match(re);
      if (match) {
        var highlight = document.createElement(nodeName || 'span');
        highlight.className = className || 'highlight';
        var wordNode = node.splitText(match.index);
        wordNode.splitText(match[0].length);
        var wordClone = wordNode.cloneNode(true);
        highlight.appendChild(wordClone);
        wordNode.parentNode.replaceChild(highlight, wordNode);
        return 1; //skip added node in parent
      }
    } else if ((node.nodeType === 1 && node.childNodes) && // only element nodes that have children
        !/(script|style)/i.test(node.tagName) && // ignore script and style nodes
        !(node.tagName === nodeName.toUpperCase() && node.className === className)) { // skip if already highlighted
      for (var i = 0; i < node.childNodes.length; i++) {
        i += jQuery.highlight(node.childNodes[i], re, nodeName, className);
      }
    }
    return 0;
  }
});

jQuery.fn.unhighlight = function (options) {
  var settings = { className: 'highlight', element: 'span' };
  jQuery.extend(settings, options);

  return this.find(settings.element + "." + settings.className).each(function () {
    var parent = this.parentNode;
    parent.replaceChild(this.firstChild, this);
    parent.normalize();
  }).end();
};

jQuery.fn.highlight = function (words, options) {
  var settings = { className: 'highlight', element: 'span', caseSensitive: false, wordsOnly: false };
  jQuery.extend(settings, options);

  if (words.constructor === String) {
    words = [words];
  }
  words = jQuery.grep(words, function(word, i){
    return word != '';
  });
  words = jQuery.map(words, function(word, i) {
    return word.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
  });
  if (words.length == 0) { return this; };

  var flag = settings.caseSensitive ? "" : "i";
  var pattern = "(" + words.join("|") + ")";
  if (settings.wordsOnly) {
    pattern = "\\b" + pattern + "\\b";
  }
  var re = new RegExp(pattern, flag);

  return this.each(function () {
    jQuery.highlight(this, re, settings.element, settings.className);
  });
};

var app = {

  globalPath:public = (window.location.hostname == 'dev.hoojobs.com') ? '/' : 'http://v2.hoojobs.com/',

  init:function () {
    this.displayMenu();
    //this.user_nav();
    this.addRemoveFav();
    this.search();
    this.addFileFromModal();
    this.GDrive();
    this.companyUploadLogo();
    this.share();
    this.loadMore();
    this.cardLinking();
    this.getZipLocation();
  },

  initTinyMce:function(target){
    tinymce.init({
      selector: "textarea#"+target,
      theme: "modern",
      width: '100%',
      height: 300,
      menubar: false,
      statusbar: false,
      plugins: [
        "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
        "save table directionality emoticons template paste textcolor placeholder"
      ],
      contextmenu: false,
      content_css: "/assets/frontend/css/tinymce/content.css?" + new Date().getTime(),
      toolbar: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
      paste_auto_cleanup_on_paste : true,
      paste_retain_style_properties:'font-weight, text-align, ul, ol, li, text-decoration',
      setup: function (theEditor) {
        theEditor.on('focus', function () {
          $(this.contentAreaContainer.parentElement).find("div.mce-toolbar-grp").fadeIn();
          $(this.contentAreaContainer.parentElement).parent().parent().addClass('focused');
          $(this.contentAreaContainer.parentElement).parent().parent().css('border','1px solid #49a8ae')

        });
        theEditor.on('blur', function () {
          $(this.contentAreaContainer.parentElement).find("div.mce-toolbar-grp").fadeOut();
          $(this.contentAreaContainer.parentElement).parent().parent().removeClass('focused');
          $(this.contentAreaContainer.parentElement).parent().parent().css('border','none')
        });
        theEditor.on("init", function() {
          $(this.contentAreaContainer.parentElement).find("div.mce-toolbar-grp").fadeOut();
          $(this.contentAreaContainer.parentElement).parent().parent().removeClass('focused');
          $(this.contentAreaContainer.parentElement).parent().parent().css('border','none')
        });
      }
    });
  },

  loadMore:function(){
    $('a.loadMore').on('click',function(e){
      e.preventDefault();
      var offset = $(this).attr('data-offset');
      $(this).text('');
      $(this).css('height',56);
      app.createLoader('a.loadMore','white');
      $.ajax({
        type:'GET',
        url:app.globalPath+'jobs/loadMore/'+offset,
        dataType:'html',
        success:function(html){
          if (html != false){
            $(html).insertAfter('.jobCard:last-of-type');
            $('.loadMore').attr('data-offset', offset+16);
          } else {
            $.notify('You have reached the last job! May want to try a search instead?', 'warning');
          }
          app.destroyLoader();
          $('a.loadMore').text('Load More').css('height',43);
        }
      })
    })
  },

  share:function(){
    $('a.share').on('click',function(e){
      e.preventDefault();
      var url = $(this).data('url');
      var title = $(this).data('title');
      var descr = $(this).data('descr');
      var image = $(this).data('image');
      app.fbShare(url,title,descr,image);
    })

  },

  fbShare:function (url,title,descr,image) {
    var winTop = (screen.height / 2) - (520 / 2);
    var winLeft = (screen.width / 2) - (350 / 2);
    window.open('http://www.facebook.com/sharer.php?s=100&p[title]=' + encodeURI(title) + '&p[summary]=' + encodeURI(descr) + '&p[url]=' + escape(url) + '&p[images][0]=' + escape(image), 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + 520 + ',height=' + 350);
  },

  ajaxSetup:function(){
    $.ajaxSetup({
      headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
    });
    $.ajaxPrefilter(function(options, originalOptions, xhr) { // this will run before each request
      var token = $('meta[name="csrf-token"]').attr('content'); // or _token, whichever you are using
      if (token) {
        return xhr.setRequestHeader('X-CSRF-TOKEN', token); // adds directly to the XmlHttpRequest Object
      }
    });

  },

  createLoader:function(target,color){
    var color = (typeof color == 'undefined') ? '#0f9d58' : color;
    $.get(app.globalPath+'loader').done(function(data){
      $(target).append(data);
      $(target).find('.spinner-layer').css('border-color',color);
    });

  },
  destroyLoader:function(){
    $('body').find('.preloader-wrapper').remove();
  },

  duplicateCard:function(data){
    var featured = (data['featured']) ? '' : 'featured';
    var activeClass = (data['status']) ? '' : 'active';
    var pauseResume = (data['status']) ? 'Pause' : 'Resume';
    var card = '<div class="col s3 card medium manage_job '+featured+'" data-id="'+data['id']+'">'
        +'<div class="top-content">'
        +'<div class="location">'
        + '<i class="mdi-communication-location-on"></i>'
        + data['location']
        +'</div>'
        +'<div class="divider"></div>'
        +'<h2>'+data['title']+'</h2>'
        +'<h3>'+data['company_name']+'</h3>'
        +'</div>'
        +'<div class="company-logo"><img src="'+data['company_logo']+'" class="circle" width="82px" height="82px"> </div>'
        +'<div class="bottom-content">'
        +'<p>'+data['the_job']+'</p>'
        +'</div>'
        +'<div class="card-actions hide">'
        +'<ul class="top">'
        +'<li>'
        +'<i class="fa fa-pencil circle"></i>'
        +'<a href="'+data['edit_url']+'" data-id="'+data['id']+'" data-action="edit">Edit</a>'
        +'</li>'
        +'<li>'
        +'<i class="fa fa-files-o circle"></i>'
        +'<a href="#" data-id="'+data['id']+'" data-action="copy">Copy</a>'
        +'</li>'
        +'<li class="'+activeClass+'">'
        +'<i class="fa fa-pause circle"></i>'
        +'<a href="#" data-id="'+data['id']+'" data-action="pause">'+pauseResume+'</a>'
        +'</li>'
        +'<li>'
        +'<i class="fa fa-close circle"></i>'
        +'<a href="#" data-id="'+data['id']+'" data-action="destroy">Discontinue</a>'
        +'</li>'
        +'</ul>'
        +'<ul class="bottom">'
        +'<li>'
        +'<i class="fa fa-user circle"></i>'
        +'<a href="'+data['applicants_url']+'">Applicants</a>'
        +'</li>'
        +'<li>'
        +'<i class="fa fa-bar-chart circle"></i>'
        +'<a href="'+data['analytics_url']+'">Analytics</a>'
        +'</li>'
        +'</ul>'
        +'</div>'
        +'</div>';

    return card;
  },

  cardAction:function(id,action,callback){
    $.ajax({
      type:'GET',
      url:app.globalPath+'company/job/'+id+'/'+action,
      dataType:'json',
      success:function(data){
        callback(data);
      },
      error: function(data){
        return data;
      }
    })
  },

  addFilesFromHD:function(file){
    app.createLoader('.preloader');
    var fd = new FormData();
    fd.append("file", file);
    $.ajax({
      url: app.globalPath+'profile/addFilesFromHD',
      type: 'POST',
      dataType:'json',
      success: function(html){
        app.destroyLoader();
        if (!$('.uploaded-content').is(':visible')){
          $('.uploaded-content').show();
        }
        $('.uploaded-content ul.selected-files').append(
            '<li class="fileElement" data-fileId="'+html['id']+'">'+ html['fileName']+
            '<a href="'+html['filePath']+'" target="_blank"><i class="mdi-action-visibility"></i></a>'+
            '<a class="removeFile"><i class="mdi-navigation-close"></i></a>'+
            '</li>'
        )
      },
      error: function(){

      },
      // Form data
      data: fd,
      mimeType:"multipart/form-data",
      cache: false,
      contentType: false,
      processData: false
    });
  },

  addFileFromModal:function(){
    $('.modal a.addSelectedFile').on('click',function(e){
      e.preventDefault();

      var fileName = $(this).attr('data-fileName');
      var filePath = $(this).attr('data-filePath');
      var fileId   = $(this).attr('data-fileId');



      if (!$('.uploaded-content').is(':visible')){
        $('.uploaded-content').show();
      }
      $('.uploaded-content ul.selected-files').append(
          '<li class="fileElement" data-fileId="'+fileId+'">'+ fileName+
          '<a href="'+filePath+'" target="_blank"><i class="mdi-action-visibility"></i></a>'+
          '<a class="removeFile"><i class="mdi-navigation-close"></i></a>'+
          '</li>'
      )

      $('#user_files').closeModal();
    })


  },

  GDrive:function(){
    $().gdrive('init', {
      'devkey': 'AIzaSyDESHbK7bbzytjk80i57S52Q9hVna1YQvU',
      'appid': '651827379542-7idgq510dfig8ieoa3n4ucqh8m2elcq3.apps.googleusercontent.com'
    });
    $('#result').gdrive('set', {
      'trigger': 'fromGDrive',
      'header': 'Select a file',
      'filter': ''
    });


  },

  Dropbox:function(){
    options = {
      // Required. Called when a user selects an item in the Chooser.
      success: function(files) {
        app.createLoader('.preloader');
        $.ajax({
          type:'POST',
          url:app.globalPath+'profile/addFilesFromDropbox',
          data:{file:files[0].link, return_path:false},
          dataType:'json',
          success:function(html){
            app.destroyLoader();
            if (!$('.uploaded-content').is(':visible')){
              $('.uploaded-content').show();
            }
            $('.uploaded-content ul.selected-files').append(
                '<li class="fileElement" data-fileId="'+html['id']+'">'+ html['fileName']+
                '<a href="'+html['filePath']+'" target="_blank"><i class="mdi-action-visibility"></i></a>'+
                '<a class="removeFile"><i class="mdi-navigation-close"></i></a>'+
                '</li>'
            )
          }
        })
      },

      cancel: function() {

      },
      linkType: "direct", // or "direct"

      multiselect: false, // or true

      extensions: ['.pdf', '.doc', '.docx', '.txt']
    };
    return Dropbox.choose(options);
  },

  displayMenu:function () {
    $("#main-nav").mmenu({
      slidingSubmenus: false,
      offCanvas: {
        position: "right",
        zposition: "next"
      }
    }, {
      // configuration
      classNames: {
        fixedElements: {
          fixed: "header"
        }
      }
    });


    var menu = $('#main-nav').data('mmenu');
    menu.bind('opened', function () {
      //$('body').css('margin-top',0)
    });
    menu.bind('closed', function() {
      //console.log('a');
    })
  },

  /*user_nav:function () {
   $('header .user-avatar.logged_user, header .user-nav').on('mouseover',function(){
   $('header .user-nav').show();
   })

   $('header .user-avatar.logged_user, header .user-nav').on('mouseout',function(){
   $('header .user-nav').hide();
   })
   $('header .user-avatar.not_logged').attr('href', app.globalPath+'login');
   },*/

  cardLinking: function(){
    $('body').on('click','.jobCard', function(e){
      if (!$(this).hasClass('disable-link')){
        window.location.href = $(this).data('link');
      }
    })
  },

  addRemoveFav: function(){
    $('body').on('mouseover', '.card-actions a', function(e){
      $(this).closest('.card.jobCard').addClass('disable-link');
    });
    /*$('body').on('mouseover', 'a.removeFav', function(e){
     $(this).closest('.card.jobCard').addClass('disable-link');
     });*/
    $('body').on('mouseleave', '.card-actions a', function(e){
      $(this).closest('.card.jobCard').removeClass('disable-link');
    });
    /*$('body').on('mouseleave', 'a.removeFav', function(e){
     $(this).closest('.card.jobCard').removeClass('disable-link');
     });*/


    $('body').on('click', 'a.addToFav', function(e){
      e.preventDefault();
      e.stopPropagation();
      e.stopImmediatePropagation();
      var item  = $(this);
      var id    = $(this).attr('data-id');
      $.ajax({
        type:'GET',
        url:app.globalPath+'profile/fav_add/'+id,
        dataType:'json',
        success:function(data){
          item.addClass('selected');
          item.removeClass('addToFav');
          item.addClass('removeFav');
          $.notify('The Job has been added to your favourites', 'success')
        },
        error: function(data){

        }
      })
    });
    $('body').on('click', 'a.removeFav', function(e){
      e.preventDefault();
      e.stopPropagation();
      e.stopImmediatePropagation();
      var item  = $(this);
      var id    = $(this).attr('data-id');
      $.ajax({
        type:'GET',
        url:app.globalPath+'profile/fav_remove/'+id,
        dataType:'json',
        success:function(data){
          item.removeClass('selected');
          item.removeClass('removeFav');
          item.addClass('addToFav');
          $.notify('The Job has been removed from your favourites', 'success')
        },
        error: function(data){

        }
      })
    });
  },

  pushUrl: function(url){
    window.history.pushState('foo', null, url);
  },

  search: function(){
    var form    = $('form.searchBar');
    var term    = form.find('input[name="search_term"]');
    var city    = form.find('select[name="search_city"]');
    var jt      = form.find('select[name="search_job_type"]');
    var Submit  = form.find('a');


    Submit.on('click',function(e){
      e.preventDefault();
      if ($.trim(term.val()) == ''){
        /*term.notify('Please write your search term','warning');
         return false;*/
        term.val(false);
      }
      window.location.href = app.globalPath+'search/'+term.val()+'/'+city.val()+'/'+jt.val();
    });

    term.on('keyup',function(e){
      clearTimeout($(this).data('timer'));
      var search = this.value;
      if (search.length >= 3 && $.trim(search) != '') {
        $(this).data('timer', setTimeout(function() {
          app.pushUrl(app.globalPath+'search/' + term.val() + '/' + city.val() + '/' + jt.val());
          $.ajax({
            type: 'GET',
            url: app.globalPath + 'search/' + term.val() + '/' + city.val() + '/' + jt.val(),
            dataType: 'json',
            success: function (html) {
              if (html['status'] == true) {
                $('header').addClass('mini');
                if ($('section:not(.top-menu) .container:first-of-type').is(':visible')) {
                  $('section:not(.top-menu) .container:first-of-type').fadeOut('slow');
                }

                if ($('.container.home').is(':visible')){
                  $('.container.home').fadeOut('slow');
                }

                if ($('.search-results').length > 0) {
                  $('.search-results').fadeOut(300, function () {
                    $(this).remove();
                  })
                }
                $(html['data']).insertBefore(".body-menu footer");


                $('body').find('.card').highlight(search);

              }
            },
            error: function (html) {
              console.log(html)
            }
          });
        }, 500));
      }
    });

  },

  dropUpload:function(e,type){

    var dt = e.originalEvent.dataTransfer;
    var file = dt.files[0];

    //var file = input.files[0];

    var url = (type == 'company') ? app.globalPath+'company/updateLogo' : app.globalPath+'profile/updateAvatar';
    var fd = new FormData();
    fd.append("file", file);
    $.ajax({
      url: url,
      type: 'POST',
      dataType:'json',
      success: function(html){
        app.destroyLoader();
        $('.company_logo').attr('src',html['filePath']);
      },
      error: function(){
        app.destroyLoader();
        $.notify('Upps! Something went wrong. Please try again', 'error');
      },
      // Form data
      data: fd,
      mimeType:"multipart/form-data",
      cache: false,
      contentType: false,
      processData: false
    });
  },

  dynamicScroll:function(scrollHeight,mainElement,classElement){
    if ($(window).scrollTop() >= scrollHeight) {
      $(mainElement).addClass(classElement);
      $('body').css('margin-top', 537);
    }

    $(window).scroll(function() {

      var scroll = $(window).scrollTop();



      var windowWidth = $(document).outerWidth(true);

      if(windowWidth <= 1010  && scroll >= 320){
        $(mainElement).addClass('mini3');
        $('body').css('margin-top', 537);
      } else if (windowWidth <= 1010 && scroll < 320) {
        $(mainElement).removeClass('mini3');
        $('body').css('margin-top', 0);
      } else {
        $(mainElement).removeClass('mini3');
        $('body').css('margin-top', 0);
      }

      if (scroll >= scrollHeight && windowWidth > 1010) {
        $(mainElement).addClass(classElement);
        $('body').css('margin-top', 537);
      }
      if (scroll < scrollHeight && windowWidth > 1010){
        $(mainElement).removeClass(classElement);
        $('body').css('margin-top', 0);
      }


    });
  },

  companyUploadLogo:function(){
    $('a.uploadFileCompany').on('click',function(e){
      console.log('a');
      e.preventDefault();
      $('input[name="fileCompany"]').click();
    })
    $('input[name="fileCompany"]').on('change',function(e){
      app.createLoader('.box');
      var file = this.files[0];

      var fd = new FormData();
      fd.append("file", file);
      $.ajax({
        url: app.globalPath+'company/updateLogo',
        type: 'POST',
        dataType:'json',
        success: function(html){
          app.destroyLoader();
          $('.company_logo').attr('src',html['filePath']);
        },
        error: function(){
          app.destroyLoader();
          $.notify('Upps! Something went wrong. Please try again', 'error');
        },
        // Form data
        data: fd,
        mimeType:"multipart/form-data",
        cache: false,
        contentType: false,
        processData: false
      });
    })
  },

  validateJobPost:function(form){

    var success = true;

    var newUser       = form.find('input[name="new_user"]');
    var jobName       = form.find('input[name="job_name"]');
    var companyName   = form.find('input[name="company_name"]');
    var ZipCode       = form.find('input[name="zip_code"]');
    var location      = form.find('input[name="location"]');
    var theJob        = tinyMCE.get('the_job').getContent();
    var theCandidate  = tinyMCE.get('the_candidate').getContent();
    var theCompany    = form.find('input[name="the_company"]');
    var jobType       = form.find('input[name="job_type"]');
    var industryId    = form.find('select[name="industry_id"]');
    var industryOther = form.find('input[name="industry_other"]');
    var categories    = form.find('input[name="categories[]"]');
    var appBy         = form.find('input[name="app_by"]');
    var appByText     = form.find('input[name="app_by_text"]');



    $('span.error').hide();

    if (newUser.val() == 1){
      var name       = form.find('input[name="new_user_name"]');
      var email      = form.find('input[name="new_user_email"]');
      var phone      = form.find('input[name="new_user_phone"]');

      if ($.trim(name.val()) == ''){
        $('.error[data-name="new_user_name"]').show();
        name.addClass('error')
        success = false;

      }

      if (!app.isEmail(email.val())){
        $('.error[data-name="new_user_email"]').show();
        email.addClass('error');
        success = false;
      }



      if ($.trim(phone.val()) == '' || !$.isNumeric(phone.val()) || $.trim(phone.val()).length < 10){
        $('.error[data-name="new_user_phone"]').show();
        phone.addClass('error');
        success = false;
      }

    }

    if ($.trim(jobName.val()) == ''){
      $('.error[data-name="job_name"]').show();
      jobName.addClass('error');
      success = false;
    }

    if ($.trim(companyName.val()) == ''){
      $('.error[data-name="company_name"]').show();
      companyName.addClass('error');
      success = false;
    }


    if ($.trim(ZipCode.val()) == '' && $.trim(location.val()) == ''){
      console.log('a');
      $('.error[data-name="zip_code"]').show();
      ZipCode.addClass('error');
      success = false;
    }

    if (theJob == ''){
      $('.error[data-name="the_job"]').show();
      //theJob.focus();
      success = false;
    }

    if (theCandidate == ''){
      $('.error[data-name="the_candidate"]').show();
      //theCandidate.focus();
      success = false;
    }

    /*if ($.trim(theCompany.val()) == ''){
     $('.error[data-name="the_company"]').show();
     theCompany.focus();
     return false;
     }*/

    if ($.trim(jobType.val()) == ''){
      $('.error[data-name="job_type"]').show();
      jobType.addClass('error');
      success = false;
    }

    if ($.trim(industryId.val()) == ''){
      $('.error[data-name="industry"]').show();
      industryId.addClass('error');
      success = false;
    }

    if (industryId.val() == 'other' && $.trim(industryOther.val()) == ''){
      $('.error[data-name="industry_other"]').show();
      industryOther.addClass('error');
      success = false;
    }

    if ($.trim(categories.val()) == ''){
      $('.error[data-name="categories"]').show();
      categories.addClass('error');
      success = false;
    }

    if ($.trim(appBy.val()) == '' || $.trim(appByText.val()) == '' ){
      $('.error[data-name="app_by_text"]').show();
      appByText.addClass('error');
      success = false;
    }



    return success;



  },

  getZipLocation:function(){
    if($('.zipLocation').length > 0){
      $.each($('.zipLocation'), function(i,v){
        var elem = $(this);
        var zip = elem.data('zip').toString();


        // IMPORTANT: Fill in your client key
        var clientKey = "js-O3doSjvnBIcSx3rq3WG7zYV6knZYnTUbW8AOr5c2NoVlrmLvisDSryYLVXKiQEuM";

        var cache = {};

        /** Handle successful response */
        function handleResp(data)
        {
          // Check for error
          if (data.error_msg)
            errorDiv.text(data.error_msg);
          else if ("city" in data)
          {
            // Set city and state
            elem.text(data.state+', '+data.city);

          }
        }

        // Set up event handlers

        if (zip.length == 5)
        {


          // Check cache
          if (zip in cache)
          {
            handleResp(cache[zip]);
          }
          else
          {
            // Build url
            var url = "https://www.zipcodeapi.com/rest/"+clientKey+"/info.json/" + zip + "/radians";

            // Make AJAX request
            $.ajax({
              "url": url,
              "dataType": "json"
            }).done(function(data) {
              handleResp(data);

              // Store in cache
              cache[zip] = data;
            }).fail(function(data) {
              if (data.responseText && (json = $.parseJSON(data.responseText)))
              {
                // Store in cache
                cache[zip] = json;

              }
              else
                console.log('Request failed.');
            });
          }
        }


        /*$.get('http://www.zipcodeapi.com/rest/js-O3doSjvnBIcSx3rq3WG7zYV6knZYnTUbW8AOr5c2NoVlrmLvisDSryYLVXKiQEuM/info.json/'+zip+'/radians', function(data){
         console.log(data);
         })*/
      })

    }

  },
  isEmail:function(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
  },

  countChars:function(val,maxChars){
    var len = val.value.length;
    if (len >= maxChars) {
      val.value = val.value.substring(0, maxChars);
      $('span.charCounter').css('color','red');
    } else {
      $('span.charCounter').text(maxChars - len);
      $('span.charCounter').css('color','gray');
    }
  },

  pageHeader:function() {
    if($(window).width() < 992){
      var headerHeight = 170;
    } else {
      var headerHeight = 120;
    }


    if(!$('header').hasClass('mini')) {
      app.dynamicScroll(400, 'header', 'mini2');
    } else {
      $('body').css('margin-top', headerHeight);
      //$('.body-menu').css('margin-top', 120);
    }
  }

};
//app.ajaxSetup();
$(document).ready(function(){
  app.init();
  app.pageHeader();
  $(window).resize(function() {
    app.pageHeader();
  });

});

/* End HooJobs JS */