Hello {{ $name }}!<br>
Thank you for purchasing in Hoojobs.

Here is the list of coupons for your publications<br>
@foreach($vouchers as $v)
    {{ $v->token }}<br>
@endforeach