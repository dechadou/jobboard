@extends('admin.base')
@section('breadcum')
    <section class="content-header">
        <h1>
            Dashboard
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/themes') }}"><i class="fa fa-users"></i> Themes</a></li>
            <li class="active">Add Theme</li>
        </ol>
    </section>
@endsection
@section('content')
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <?php
            if (Session::has('message')) {  $message = Session::get('message'); ?>
            <label class="control-label" for="inputSuccess"><i class="fa fa-check"></i> <?php echo $message ?></label>
            <?php } ?>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Theme</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!! Form::open(array('url' => 'admin/themes/add/store','files'=>true,  'request' => 'form')) !!}
                <div class="box-body">
                    <div class="form-group">
                        {!! Form::label('Theme Name') !!}
                        {!! Form::text('theme_name', null, array('required', 'class'=>'form-control', 'placeholder'=>'Enter Name')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('Theme Description') !!}
                        {!! Form::text('theme_description', null, array('required', 'class'=>'form-control')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('Zip File') !!}
                        {!! Form::file('theme_file', null, array('required', 'class'=>'form-control')) !!}
                    </div>
                </div><!-- /.box-body -->

                <div class="box-footer">
                    {!! Form::submit('Send', array('class'=>'btn btn-primary')) !!}
                </div>
                {!! Form::close() !!}
            </div><!-- /.box -->
        </div>
    </div>
@endsection
@section('custom_js')
    <script>
        $(document).ready(function(e){
            $('select[name="discount_type"]').on('change',function(e){
                var selected = $(this).val();
                if (selected == 0 || selected == 1){
                    $('.discount').removeClass('hide');
                } else {
                    $('.discount').addClass('hide');
                }
            })
        })
    </script>
@endsection