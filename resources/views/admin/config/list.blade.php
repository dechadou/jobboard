<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 14/5/15
 * Time: 13:18
 */
?>
@extends('admin.base')
@section('breadcum')
    <section class="content-header">
        <h1>
            Dashboard
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">General Config</li>
        </ol>
    </section>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <?php
            if (Session::has('message')) {  $message = Session::get('message'); ?>
            <label class="control-label" for="inputSuccess"><i class="fa fa-check"></i> <?php echo $message ?></label>
            <?php } ?>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
                {!! Form::model($config, array('url' => 'admin/config/update', 'files'=>true, 'role' => 'form')) !!}
                <div class="box box-primary">
                    <div class="box-header">
                        <h3>Theme</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <lable>Select Main Theme</lable>
                            <select name="theme" class="form-control">
                                @foreach($config as $cfg)
                                    @if($cfg->name == 'theme' || $cfg->name == 'theme_selected')
                                        <option value="{{ $cfg->id }}" @if($cfg->name == 'theme_selected') selected @endif>{{$cfg->value}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Site Data</h3>
                    </div><!-- /.box-header -->


                    <div class="box-body">
                        @foreach($config as $cfg)
                            @if($cfg->id >= 9 && $cfg->id <=12)
                                <div class="form-group">
                                    @if($cfg->name == 'site_favicon' || $cfg->name == 'site_logo' || $cfg->name == 'home_background')
                                        {!! Form::file($cfg->name, null, array('required', 'class'=>'form-control')) !!}
                                        @if($cfg->value != '')<img src="{{ asset('uploads/general/'.$cfg->value) }}" width="200px">@endif
                                    @else
                                        {!! Form::text($cfg->id, $cfg->value ,array('required', 'class'=>'form-control', 'placeholder'=>$cfg->description)) !!}
                                    @endif
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Site Meta</h3>
                </div><!-- /.box-header -->

                <div class="box-body">
                    @foreach($config as $cfg)
                        @if($cfg->id <= 3)
                            <div class="form-group">
                                {!! Form::label($cfg->name) !!}
                                @if($cfg->name == 'description')
                                    {!! Form::textarea($cfg->id, $cfg->value ,array('required', 'class'=>'form-control', 'placeholder'=>$cfg->description)) !!}
                                @else
                                    {!! Form::text($cfg->id, $cfg->value ,array('required', 'class'=>'form-control', 'placeholder'=>$cfg->description)) !!}
                                @endif
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>


            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Footer</h3>
                </div><!-- /.box-header -->

                <div class="box-body">
                    @foreach($config as $cfg)
                        @if($cfg->id >= 4 && $cfg->id <=5)
                            <div class="form-group">
                                {!! Form::label($cfg->name) !!}
                                {!! Form::textarea($cfg->id, $cfg->value ,array('class'=>'form-control', 'placeholder'=>$cfg->description)) !!}
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Social</h3>
                </div><!-- /.box-header -->

                <div class="box-body">
                    @foreach($config as $cfg)
                        @if($cfg->id > 5 && $cfg->id < 9)
                            <div class="form-group">
                                {!! Form::label($cfg->name) !!}
                                {!! Form::text($cfg->id, $cfg->value ,array('class'=>'form-control', 'placeholder'=>$cfg->description)) !!}
                            </div>
                        @endif
                    @endforeach
                    <div class="box-footer">
                        {!! Form::submit('Send', array('class'=>'btn btn-primary')) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>



@endsection
@section('custom_js')

@endsection