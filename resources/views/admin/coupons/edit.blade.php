@extends('admin.base')
@section('breadcum')
    <section class="content-header">
        <h1>
            Dashboard
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/coupons') }}"><i class="fa fa-users"></i> Coupons</a></li>
            <li class="active">Edit Coupon {{ $coupon->id }}</li>
        </ol>
    </section>
@endsection
@section('content')
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <?php
            if (Session::has('message')) {  $message = Session::get('message'); ?>
            <label class="control-label" for="inputSuccess"><i class="fa fa-check"></i> <?php echo $message ?></label>
            <?php } ?>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Coupon</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                {!! Form::open(array('url' => 'admin/coupons/edit/'.$coupon->id.'/update', 'request' => 'form')) !!}
                <div class="box-body">
                    <div class="form-group">
                        {!! Form::label('Description') !!}
                        {!! Form::text('description', $coupon->description, array('required', 'class'=>'form-control', 'placeholder'=>'Enter Description')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('Discount Type') !!}
                        {!! Form::select('discount_type', [0=>'Money',1=>'Percentage',2=>'Free Publication',4=>'Free Featured Publication'], $coupon->discount_type, array('required', 'class'=>'form-control')) !!}
                    </div>
                    <div class="form-group discount">
                        {!! Form::label('Discount Value') !!}
                        {!! Form::text('discount', $coupon->discount, array('class'=>'form-control', 'placeholder'=>'Enter Discount Value (Dont add $ or % sign)')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('Coupon') !!}
                        {!! Form::text('token', $coupon->token, array('class'=>'form-control')) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('Status') !!}
                        @if ($coupon->status)
                            {!! Form::checkbox('active', 1, true) !!}
                        @else
                            {!! Form::checkbox('active', 1) !!}
                        @endif
                    </div>

                </div><!-- /.box-body -->

                <div class="box-footer">
                    {!! Form::submit('Send', array('class'=>'btn btn-primary')) !!}
                </div>
                {!! Form::close() !!}
            </div><!-- /.box -->
        </div>
    </div>
@endsection
@section('custom_js')
    <script>
        $(document).ready(function(e){

            if ($('select[name="discount_type"]').val() == 0 || $('select[name="discount_type"]').val() == 1){
                $('.discount').removeClass('hide');
            } else {
                $('.discount').addClass('hide');
            }

            $('select[name="discount_type"]').on('change',function(e){
                var selected = $(this).val();
                if (selected == 0 || selected == 1){
                    $('.discount').removeClass('hide');
                } else {
                    $('.discount').addClass('hide');
                }
            })
        })
    </script>
@endsection