@extends('admin.base')
@section('breadcum')
    <section class="content-header">
        <h1>
            Dashboard
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Categories</li>
        </ol>
    </section>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Categories</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <table id="data" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Created at</th>
                            <th>Updated at</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!$categories->isEmpty())
                        @foreach ($categories as $c)
                            <tr>
                                <td>{{$c->id}}</td>
                                <td>{{$c->name}}</td>
                                <td>{{$c->created_at}}</td>
                                <td>{{$c->updated_at}}</td>
                                <td>
                                    <a href="{{ url('admin/categories/edit/'.$c->id) }}" class="btn btn-small"><i class="fa fa-edit"></i>Edit</a>
                                    <a href="{{ url('admin/categories/delete/'.$c->id) }}" class="btn btn-small delete"><i class="fa fa-trash"> Delete</i></a>
                                </td>

                            </tr>
                        @endforeach
                            @endif
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
                @if($logged_user->role_id == 1)
                    <div class="box-footer">
                        <a href="{{ url('admin/categories/add') }}" class="btn btn-primary">Add New</a>
                    </div>
                @endif
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->

@endsection
@section('custom_js')
    <script type="text/javascript">
        $(function () {
            $("#data").dataTable();

            $('.delete').on('click',function(e){
                e.preventDefault();
                var href = $(this).attr('href');
                if (confirm('Are you sure you want to delete this item?')){
                    window.location.href = href;
                }
            })
        })
    </script>
@endsection