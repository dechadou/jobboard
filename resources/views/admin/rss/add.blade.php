<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 14/5/15
 * Time: 13:18
 */
?>
@extends('admin.base')
@section('breadcum')
    <section class="content-header">
        <h1>
            Dashboard
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">RSS Feeds</li>
        </ol>
    </section>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <?php
            if (Session::has('message')) {  $message = Session::get('message'); ?>
            <label class="control-label" for="inputSuccess"><i class="fa fa-check"></i> <?php echo $message ?></label>
            <?php } ?>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Add Feed</h3>
                </div><!-- /.box-header -->
                {!! Form::open(array('url' => 'admin/rss/add/store', 'role' => 'form')) !!}
                <div class="box-body">
                    {!! Form::label('Name') !!}
                    {!! Form::text('name', null ,array('required', 'class'=>'form-control', 'placeholder'=>'Feed Name')) !!}
                </div>
                <div class="box-body">
                    {!! Form::label('Url') !!}
                    {!! Form::text('url', null ,array('required', 'class'=>'form-control', 'placeholder'=>'Feed Url')) !!}
                </div>
                <div class="box-body">
                    {!! Form::label('Description') !!}
                    {!! Form::textarea('description', null ,array('required', 'class'=>'form-control', 'placeholder'=>'Feed Description')) !!}
                </div>
                <div class="box-body">
                    {!! Form::label('Active') !!}
                    {!! Form::checkbox('active', 1, true) !!}
                </div>
                <div class="box-footer">
                    {!! Form::submit('Send', array('class'=>'btn btn-primary')) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    </div>
@endsection
@section('custom_js')

@endsection