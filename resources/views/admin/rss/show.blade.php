<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 14/5/15
 * Time: 13:18
 */
?>
@extends('admin.base')
@section('breadcum')
    <section class="content-header">
        <h1>
            Dashboard
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/rss') }}"><i class="fa fa-rss"></i> RSS Feeds</a></li>
            <li class="active">RSS: {{ $rss->name }} ( {{ $rss->id }} )</li>
        </ol>
    </section>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <?php
            if (Session::has('message')) {  $message = Session::get('message'); ?>
            <label class="control-label" for="inputSuccess"><i class="fa fa-check"></i> <?php echo $message ?></label>
            <?php } ?>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Feed {{ $rss->name }}</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <label>Name:</label>
                    {{ $rss->name }}
                </div>
                <div class="box-body">
                    <label>Url:</label>
                    {{ $rss->url }}
                </div>
                <div class="box-body">
                    <label>Description</label>
                    {{ $rss->description }}
                </div>
                <div class="box-body">
                    <label>Created At:</label>
                    {{ $rss->created_at }}
                </div>
                <div class="box-body">
                    <label>Updated At:</label>
                    {{ $rss->updated_at }}
                </div>
                <div class="box-footer">
                    <a href="{{ url('admin/rss/edit/'.$rss->id) }}" class="btn btn-primary btn-small">Editar</a>
                    <a href="{{ url('admin/rss') }}" class="btn btn-warning btn-small">Cancelar</a>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
@section('custom_js')

@endsection