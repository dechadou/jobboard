@extends('admin.base')
@section('breadcum')
    <section class="content-header">
        <h1>
            Dashboard
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Users & Companies</li>
        </ol>
    </section>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Companies</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Company ID</th>
                            <th>Logo</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!$companies->isEmpty())
                            @foreach ($companies as $_c)
                                <tr>
                                    <td><a href="{{ url('admin/companies/show/'.$_c->id) }}">{{$_c->id}}</a></td>
                                    <td><img src="{{ CompanyLogo($_c->user) }}" class="img-circle" style="width:100px"><br></td>
                                    <td>{{$_c->name}}</td>
                                    <td>{{$_c->description}}</td>
                                    <td>
                                        <a href="{{ url('admin/companies/show/'.$_c->id) }}">View Transactions & Jobs</a>
                                        <a href="{{ url('admin/companies/edit/'.$_c->id) }}"  class="btn btn-small"><i class="fa fa-edit"></i>Edit</a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->

@endsection
