@extends('admin.base')
@section('breadcum')
    <section class="content-header">
        <h1>
            Dashboard
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/users') }}"><i class="fa fa-users"></i> Users & Companies</a></li>
            <li class="active">Company: {{ $companies->name }} ( {{ $companies->id }} )</li>
        </ol>
    </section>
@endsection
@section('content')
		<div class="row">
			<!-- left column -->
			<div class="col-md-12">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title">Company Edit</h3>
					</div><!-- /.box-header -->
					<!-- form start -->
					{!! Form::model($companies, array('url' => 'admin/companies/edit/'.$companies->id.'/update', 'files'=>true, 'request' => 'form')) !!}
						<div class="box-body">
							<div class="form-group">
								{!! Form::label('Id') !!}
								{!! Form::text('id', $companies->id, array('disabled', 'class'=>'form-control')) !!}
							</div>
							<div class="form-group">
								{!! Form::label('Name') !!}
								{!! Form::text('name', $companies->name, array('required', 'class'=>'form-control', 'placeholder'=>'Enter name')) !!}
							</div>
                            <div class="form-group">
                                {!! Form::label('Description') !!}
                                {!! Form::textarea('description', $companies->description, array('required', 'class'=>'form-control ckeditor', 'placeholder'=>'Enter the company Description')) !!}
                            </div>
                            <div class="form-group">
                                <img src="{{ CompanyLogo($companies->user) }}" class="img-circle"><br>
                                <hr>
                                {!! Form::label('Logo') !!}
                                {!! Form::file('logo', null, array('required', 'class'=>'form-control')) !!}
                            </div>
						</div><!-- /.box-body -->

						<div class="box-footer">
							{!! Form::submit('Send', array('class'=>'btn btn-primary')) !!}
						</div>
					{!! Form::close() !!}
				</div><!-- /.box -->
			</div>
		</div>
@endsection
