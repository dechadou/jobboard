@extends('admin.base')
@section('breadcum')
    <section class="content-header">
        <h1>
            Dashboard
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/users') }}"><i class="fa fa-users"></i> Users & Companies</a></li>
            <li class="active">Company: {{ $company->name }} ( {{ $company->id }} )</li>
        </ol>
    </section>
@endsection
@section('content')
		<div class="row">
			<!-- left column -->
			<div class="col-md-12">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title">Job: {{ $job->title }}</h3>
					</div><!-- /.box-header -->
					<!-- form start -->
                    <?php
                        foreach($job->categories as $c){
                           $catSelected[] =  $c->id;
                        }
                    ?>
                    <?php
                    if (Session::has('message')) {  $message = Session::get('message'); ?>
                    <label class="control-label" for="inputSuccess"><i class="fa fa-check"></i> <?php echo $message ?></label>
                    <?php } ?>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
					{!! Form::model($job, array('url' => 'admin/companies/'.$company->id.'/jobs/update/'.$job->id, 'files'=>true, 'request' => 'form')) !!}
						<div class="box-body">
							<div class="form-group">
								{!! Form::label('Id') !!}
								{!! Form::text('id', $job->id, array('disabled', 'class'=>'form-control')) !!}
							</div>
							<div class="form-group">
								{!! Form::label('Title') !!}
								{!! Form::text('title', $job->title, array('required', 'class'=>'form-control', 'placeholder'=>'Enter title')) !!}
							</div>
                            <div class="form-group">
                                {!! Form::label('Job Description') !!}
                                {!! Form::textarea('the_job', $job->the_job, array('required', 'class'=>'form-control ckeditor', 'placeholder'=>'Enter Job Description')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Candidate Description') !!}
                                {!! Form::textarea('the_candidate', $job->the_candidate, array('required', 'class'=>'form-control ckeditor', 'placeholder'=>'Enter Candidate Description')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Location') !!}
                                {!! Form::select('location_id', $locations, null, ['class' => 'form-control chosen']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Job Type') !!}
                                {!! Form::select('app_by', [
                                    '0' => 'Full-time',
                                    '1' => 'Part-time',
                                    '2' => 'Freelance'
                                ]
                                , $job->job_type, array('required', 'class'=>'form-control')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Categories') !!}
                                {!! Form::select('category_id[]', $categories, $catSelected, array('multiple'=>'multiple', 'class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Applications By') !!}
                                {!! Form::select('app_by', [
                                    '0' => 'URL',
                                    '1' => 'Email'
                                ]
                                , $job->app_by, array('required', 'class'=>'form-control')) !!}
                                {!! Form::text('app_by_text', $job->app_by_text, array('required', 'class'=>'form-control')) !!}
                            </div>
                            <div class="box-body">
                                {!! Form::label('Featured Job') !!}
                                @if ($job->featured)
                                    {!! Form::checkbox('featured', 1, true) !!}
                                @else
                                    {!! Form::checkbox('featured', 1) !!}
                                @endif
                            </div>
                            <div class="box-body">
                                {!! Form::label('Sticky Job') !!}
                                @if ($job->sticky)
                                    {!! Form::checkbox('sticky', 1, true) !!}
                                @else
                                    {!! Form::checkbox('sticky', 1) !!}
                                @endif
                            </div>
                            <div class="box-body">
                                {!! Form::label('Published') !!}
                                @if ($job->status)
                                    {!! Form::checkbox('status', 1, true) !!}
                                @else
                                    {!! Form::checkbox('status', 1) !!}
                                @endif
                            </div>


						</div><!-- /.box-body -->

						<div class="box-footer">
							{!! Form::submit('Send', array('class'=>'btn btn-primary')) !!}
						</div>
					{!! Form::close() !!}
				</div><!-- /.box -->
			</div>
		</div>
@endsection
@section('custom_js')
    <script>
        $(document).ready(function(){
            $(".chosen").chosen()

            $("select[name='category_id']").on("click", "option", function(){
                if ( 3 <= $(this).siblings(":selected").length ) {
                    $(this).removeAttr("selected");
                    alert('You can only select 3 categories');
                }
            })
        })
    </script>
@endsection
