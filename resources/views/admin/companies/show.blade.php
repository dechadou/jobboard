@extends('admin.base')
@section('breadcum')
    <section class="content-header">
        <h1>
            Dashboard
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/users') }}"><i class="fa fa-users"></i> Users & Companies</a></li>
            <li class="active">Company: {{ $companies->name }} ( {{ $companies->id }} )</li>
        </ol>
    </section>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Company {{ $companies->name }}</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <label>ID:</label>
                    {{ $companies->id }}
                </div>
                <div class="box-body">
                    <label>Name:</label>
                    {{ $companies->name }}
                </div>
                <div class="box-body">
                    <label>Description</label>
                    {{ $companies->description }}
                </div>

                <div class="box-body">
                    <label>Logo</label><br>
                    <img src="{{ CompanyLogo($companies->user) }}" class="img-circle" style="width:200px">
                </div>
                <div class="box-body">
                    <label>Created At:</label>
                    {{ $companies->created_at }}
                </div>
                <div class="box-body">
                    <label>Updated At:</label>
                    {{ $companies->updated_at }}
                </div>
                <div class="box-footer">
                    <a href="{{ url('admin/companies/edit/'.$companies->id) }}" class="btn btn-primary btn-small">Editar</a>
                    <a href="{{ url('admin/companies') }}" class="btn btn-warning btn-small">Cancelar</a>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Transactions</h3>
                </div>

                <div class="box-body">
                    @if(!$companies->transactions->isEmpty())
                        <table id="data" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Transaction ID</th>
                                <th>Payed With</th>
                                <th>Data</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($companies->transactions as $transactions)
                                <tr>
                                    <td>{{ $transactions->id }}</td>
                                    <td>{{ $transactions->modules->name }}</td>
                                    <td>
                                        <? $data = json_decode($transactions->data,true) ?>

                                        @foreach($data as $k => $v)
                                            <strong>{{ $k }}</strong>: {{ $v }}<br>
                                        @endforeach
                                        </td>
                                    <td>{{ $transactions->created_at }}</td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    @else
                        There are no transactions for this company
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Jobs</h3>
                </div>
                <div class="box-body">
                    @if(!$companies->jobs->isEmpty())
                    <table id="data2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Job ID</th>
                            <th>Title</th>
                            <th>Job Description</th>
                            <th>Candidate Description</th>
                            <th>Location</th>
                            <th>Job Type</th>
                            <th>Category</th>
                            <th>App By </th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($companies->jobs as $jobs)
                            <tr>
                                <td>{{ $jobs->id }}</td>
                                <td>{{ $jobs->title }}</td>
                                <td><span class="truncate">{!! $jobs->the_job !!}</span></td>
                                <td><span class="truncate">{!! $jobs->the_candidate !!}</span></td>
                                <td>
                                    @if($jobs->locations != null)
                                        {{ $jobs->locations->name }}
                                    @else
                                        {{ $jobs->zip_code }}
                                    @endif
                                </td>
                                <td>{{ $jobs->types->name }}</td>
                                <td>
                                    <ul>
                                    @foreach($jobs->categories as $category)
                                      <li>{{ $category->name }}</li>
                                    @endforeach
                                    </ul>

                                </td>
                                <td>
                                    @foreach([0=>'URL',1=>'Email'] as $key => $value )
                                        @if($jobs->app_by == $key)
                                            {{ $value }}
                                        @endif
                                    @endforeach
                                </td>
                                <td>@if($jobs->status == 1) <i class="fa fa-check"></i> Published @else <i class="fa fa-close"></i> Unpublished @endif</td>
                                <td>
                                    <a href="{{ url('admin/companies/'.$companies->id.'/jobs/edit/'.$jobs->id) }}"  class="btn btn-small"><i class="fa fa-edit"></i>Edit</a>
                                    <a href="{{ url('admin/companies/'.$companies->id.'/jobs/delete/'.$jobs->id) }}"  class="btn btn-small delete"><i class="fa fa-trash"> Delete</i></a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    @else
                        There are no jobs for this company
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
@section('custom_js')
    <script type="text/javascript">
        $(function () {
            $("#data").dataTable();
            $("#data2").dataTable();
            $('.delete').on('click',function(e){
                e.preventDefault();
                var href = $(this).attr('href');
                if (confirm('Are you sure you want to delete this item?')){
                    window.location.href = href;
                }
            })
        })
    </script>
@endsection
