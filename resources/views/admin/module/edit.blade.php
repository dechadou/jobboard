<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 14/5/15
 * Time: 13:18
 */
?>
@extends('admin.base')
<?php $module_data = json_decode($module->data, true); ?>
@section('breadcum')
    <section class="content-header">
        <h1>Dashboard
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/modules') }}"><i class="fa fa-file-o"></i> Modules</a></li>
            <li class="active">Module: {{ $module->name }} ( {{ $module->id }} )</li>
        </ol>
    </section>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <?php
            if (Session::has('message')) {  $message = Session::get('message'); ?>
            <label class="control-label" for="inputSuccess"><i class="fa fa-check"></i> <?php echo $message ?></label>
            <?php } ?>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Module Info</h3>
                </div><!-- /.box-header -->
                {!! Form::model($module, array('url' => 'admin/modules/edit/'.$module->id.'/update', 'role' => 'form')) !!}
                <div class="box-body">
                    {!! Form::label('Name') !!}
                    {!! Form::text('name', $module->name ,array('required', 'class'=>'form-control', 'placeholder'=>'Page title')) !!}
                </div>
                <div class="box-body">
                    {!! Form::label('Description') !!}
                    {!! Form::textarea('description', $module->description ,array('required', 'class'=>'form-control', 'placeholder'=>'Page Content')) !!}
                </div>
                <div class="box-header">
                    <h3 class="box-title">Module Data</h3>
                </div>
                <div class="box-body">
                    {!! Form::label('App Key') !!}
                    {!! Form::text('data[app_key]', $module_data['app_key'], array('required', 'class'=>'form-control', 'placeholder'=>'App Key')) !!}
                </div>
                <div class="box-body">
                    {!! Form::label('App Secret') !!}
                    {!! Form::text('data[app_secret]', $module_data['app_secret'], array('required', 'class'=>'form-control', 'placeholder'=>'App Secret')) !!}
                </div>
                <div class="box-body">
                    {!! Form::label('Active') !!}
                    @if ($module->status)
                        {!! Form::checkbox('active', 1, true) !!}
                    @else
                        {!! Form::checkbox('active', 1) !!}
                    @endif
                </div>
                <div class="box-footer">
                    {!! Form::submit('Send', array('class'=>'btn btn-primary')) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    </div>
@endsection