<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 14/5/15
 * Time: 13:18
 */
?>
@extends('admin.base')
@section('breadcum')
    <section class="content-header">
        <h1>
            Dashboard
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Modules</li>
        </ol>
    </section>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <?php
            if (Session::has('message')) {  $message = Session::get('message'); ?>
            <label class="control-label" for="inputSuccess"><i class="fa fa-check"></i> <?php echo $message ?></label>
            <?php } ?>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Modules</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <table id="data" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if (!$modules->isEmpty())
                            @foreach($modules as $m)
                                <tr>
                                    <td>{{ $m->id }}</td>
                                    <td>{{ $m->name }}</td>
                                    <td>{{ $m->description }}</td>
                                    <td>@if($m->status == 1) <i class="fa fa-check"></i> Enabled @else <i class="fa fa-close"></i> Disabled @endif</td>
                                    <td>{{ $m->created_at }}</td>
                                    <td>{{ $m->updated_at }}</td>
                                    <td>
                                        <a href="{{ url('admin/modules/edit/'.$m->id) }}" class="btn btn-small"><i class="fa fa-edit"> Administrate</i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Actions</th>
                        </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
@endsection
@section('custom_js')
    <script type="text/javascript">
        $(function () {
            $("#data").dataTable();

            $('.delete').on('click',function(e){
                e.preventDefault();
                var href = $(this).attr('href');
                if (confirm('Are you sure you want to delete this item?')){
                    window.location.href = href;
                }
            })
        })
    </script>
@endsection