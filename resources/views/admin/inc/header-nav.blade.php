<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
    </a>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="{{ userAvatar($logged_user) }}" class="user-image" alt="User Image"/>
                    <span class="hidden-xs">{{ $logged_user->name }}.</span>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                        <img src="{{{ ($logged_user->avatar != '') ? asset('uploads/user_avatar/'.$logged_user->avatar) : asset('assets/backend/images/default.gif') }}}" class="img-circle" alt="User Image" />
                        <p>
                            {{ $logged_user->name }}
                            <small>Member since {{ date('F d, Y', strtotime($logged_user->created_at)) }}</small>
                        </p>
                    </li>
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-right">
                            <a href="{{ url('/auth/logout') }}" class="btn btn-default btn-flat">Sign out</a>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>

</nav>
