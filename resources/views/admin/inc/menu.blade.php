<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{{ ($logged_user->avatar !='') ? asset('uploads/user_avatar/'.$logged_user->avatar) : asset('assets/backend/images/default.gif') }}}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>{{ $logged_user->name }}</p>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
                <a href="{{ url('admin/') }}">
                    <i class="fa fa-home"></i> <span>Home</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gear"></i> <span>General Config</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('admin/pricing') }}"><i class="fa fa-circle-o"></i> Pricing List</a></li>
                    <li><a href="{{ url('admin/config') }}"><i class="fa fa-circle-o"></i> Manage Config</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gear"></i> <span>Themes</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('admin/themes') }}"><i class="fa fa-circle-o"></i> Theme List</a></li>
                    <li><a href="{{ url('admin/themes/add') }}"><i class="fa fa-circle-o"></i> Add New</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-rss"></i> <span>Job Attributes</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('admin/categories') }}"><i class="fa fa-circle-o"></i> Categories</a></li>
                    <li><a href="{{ url('admin/jobtypes') }}"><i class="fa fa-circle-o"></i> Job Types</a></li>
                    <li><a href="{{ url('admin/industries') }}"><i class="fa fa-circle-o"></i> Industries</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-rss"></i> <span>RSS Feeds</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('admin/rss/add') }}"><i class="fa fa-circle-o text-aqua"></i> Add New</a></li>
                    <li><a href="{{ url('admin/rss') }}"><i class="fa fa-circle-o"></i> List of RSS Feeds</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-rss"></i> <span>Coupons</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('admin/coupons/add') }}"><i class="fa fa-circle-o text-aqua"></i> Add New</a></li>
                    <li><a href="{{ url('admin/coupons') }}"><i class="fa fa-circle-o"></i> List of Coupons</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i> <span>Users & Companies</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('admin/users') }}"><i class="fa fa-circle-o"></i> List of Users</a></li>
                    <li><a href="{{ url('admin/companies') }}"><i class="fa fa-circle-o"></i> List of Companies</a></li>
                    <li><a href="{{ url('admin/roles') }}"><i class="fa fa-circle-o"></i> User Roles</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-file-o"></i> <span>Static Pages</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('admin/pages/add') }}"><i class="fa fa-circle-o"></i> Add New</a></li>
                    <li><a href="{{ url('admin/pages') }}"><i class="fa fa-circle-o"></i> List of Pages</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Modules</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('admin/modules') }}"><i class="fa fa-circle-o"></i> Manage Modules</a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
