<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="{{ URL::asset('assets/backend/js/vendors.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/backend/js/plugins.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/backend/js/dashboard2.js') }}" type="text/javascript"></script>
<script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.4.5/adapters/jquery.js"></script>
<script src="{{ URL::asset('assets/backend/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/backend/js/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
<script>
    CKEDITOR.config.autoParagraph = false;
</script>