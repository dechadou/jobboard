<!-- Logo -->
<a href="{{ url('/admin') }}" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>JOB</b>B</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>Job</b>Board</span>
</a>