@extends('admin.base')
@section('breadcum')
    <section class="content-header">
        <h1>
            Dashboard
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/users') }}"><i class="fa fa-users"></i> Users & Companies</a></li>
            <li class="active">User: {{ $user->name }} ( {{ $user->id }} )</li>
        </ol>
    </section>
@endsection
@section('content')
		<div class="row">
			<!-- left column -->
			<div class="col-md-12">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title">User Edit</h3>
					</div><!-- /.box-header -->
					<!-- form start -->
					{!! Form::model($user, array('url' => 'admin/users/edit/'.$user->id.'/update', 'files'=>true, 'request' => 'form')) !!}
						<div class="box-body">
							<div class="form-group">
								{!! Form::label('Id') !!}
								{!! Form::text('id', $user->id, array('disabled', 'class'=>'form-control')) !!}
							</div>
							<div class="form-group">
								{!! Form::label('Name') !!}
								{!! Form::text('name', $user->name, array('required', 'class'=>'form-control', 'placeholder'=>'Enter name')) !!}
							</div>
							<div class="form-group">
								{!! Form::label('Password') !!}
								{!! Form::password('password', array('class'=>'form-control', 'placeholder'=>'Enter new password')) !!}
							</div>
							<div class="form-group">
								{!! Form::label('Email') !!}
								{!! Form::email('email', $user->email, array('required', 'class'=>'form-control', 'placeholder'=>'Enter email')) !!}
							</div>

                            <div class="form-group">
                                <img src="{{{ ($user->avatar !='') ? asset('uploads/user_avatar/'.$user->avatar) : asset('assets/backend/images/default.gif') }}}" class="img-circle"><br>
                                <hr>
                                {!! Form::label('Avatar') !!}
                                {!! Form::file('avatar', null, array('required', 'class'=>'form-control', 'placeholder'=>'Enter email')) !!}
                            </div>
                            @if($logged_user->role_id == 1)
                                <div class="form-group">
                                    {!! Form::label('Role') !!}
                                    {!! Form::select('role', $roles, $user->role->id, array('required', 'class'=>'form-control')) !!}
                                </div>
                            @endif
						</div><!-- /.box-body -->

						<div class="box-footer">
							{!! Form::submit('Send', array('class'=>'btn btn-primary')) !!}
						</div>
					{!! Form::close() !!}
				</div><!-- /.box -->
			</div>
		</div>
@endsection
