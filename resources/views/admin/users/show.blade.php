@extends('admin.base')
@section('breadcum')
    <section class="content-header">
        <h1>
            Dashboard
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/users') }}"><i class="fa fa-users"></i> Users & Companies</a></li>
            <li class="active">User: {{ $users->name }} ( {{ $users->id }} )</li>
        </ol>
    </section>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">User {{ $users->name }}</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <label>ID:</label>
                    {{ $users->id }}
                </div>
                <div class="box-body">
                    <label>Name:</label>
                    {{ $users->name }}
                </div>
                <div class="box-body">
                    <label>Email</label>
                    {{ $users->email }}
                </div>
                <div class="box-body">
                    <label>Role</label>
                    {{ $users->role->name }}
                </div>
                <div class="box-body">
                    <label>Avatar</label><br>
                    <img src="{{{ ($users->avatar !='') ? asset('uploads/user_avatar/'.$users->avatar) : asset('assets/backend/images/default.gif') }}}" class="img-circle">
                </div>
                <div class="box-body">
                    <label>Created At:</label>
                    {{ $users->created_at }}
                </div>
                <div class="box-body">
                    <label>Updated At:</label>
                    {{ $users->updated_at }}
                </div>
                <div class="box-footer">
                    <a href="{{ url('admin/users/edit/'.$users->id) }}" class="btn btn-primary btn-small">Editar</a>
                    <a href="{{ url('admin/users') }}" class="btn btn-warning btn-small">Cancelar</a>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
