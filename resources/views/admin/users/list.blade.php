@extends('admin.base')
@section('breadcum')
    <section class="content-header">
        <h1>
            Dashboard
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Users & Companies</li>
        </ol>
    </section>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Users</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>User ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!$users->isEmpty())
                        @foreach ($users as $user)
                            <tr>
                                <td><a href="{{ url('admin/users/show/'.$user->id) }}">{{$user->id}}</a></td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->role->name}}</td>
                                <td><a href="{{ url('admin/users/edit/'.$user->id) }}" class="btn btn-small"><i class="fa fa-edit"></i>Edit</a></td>
                            </tr>
                        @endforeach
                            @endif
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
                @if($logged_user->role_id == 1)
                    <div class="box-footer">
                        <a href="{{ url('admin/users/add') }}" class="btn btn-primary">Add New</a>
                    </div>
                @endif
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->

@endsection
