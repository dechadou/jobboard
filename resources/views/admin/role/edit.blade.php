@extends('admin.base')
@section('breadcum')
    <section class="content-header">
        <h1>
            Dashboard
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/roles') }}"><i class="fa fa-users"></i> Roles</a></li>
            <li class="active">User: {{ $roles->name }} ( {{ $roles->id }} )</li>
        </ol>
    </section>
@endsection
@section('content')
		<div class="row">
			<!-- left column -->
			<div class="col-md-12">
                <?php
                if (Session::has('message')) {  $message = Session::get('message'); ?>
                <label class="control-label" for="inputSuccess"><i class="fa fa-check"></i> <?php echo $message ?></label>
                <?php } ?>
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title">User Edit</h3>
					</div><!-- /.box-header -->
					<!-- form start -->
					{!! Form::model($roles, array('url' => 'admin/roles/edit/'.$roles->id.'/update', 'request' => 'form')) !!}
						<div class="box-body">
							<div class="form-group">
								{!! Form::label('Id') !!}
								{!! Form::text('id', $roles->id, array('disabled', 'class'=>'form-control')) !!}
							</div>
							<div class="form-group">
                                {!! Form::label('Name') !!}
                                {!! Form::text('name', $roles->name, array('required', 'class'=>'form-control', 'placeholder'=>'Role Name')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Description') !!}
                                {!! Form::text('description', $roles->description, array('required', 'class'=>'form-control', 'placeholder'=>'Role Description')) !!}
                            </div>

						</div><!-- /.box-body -->

						<div class="box-footer">
							{!! Form::submit('Send', array('class'=>'btn btn-primary')) !!}
						</div>
					{!! Form::close() !!}
				</div><!-- /.box -->
			</div>
		</div>
@endsection
