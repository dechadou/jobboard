<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 14/5/15
 * Time: 13:18
 */
?>
@extends('admin.base')
@section('breadcum')
    <section class="content-header">
        <h1>
            Dashboard
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/pages') }}"><i class="fa fa-file-o"></i>Static Pages</a></li>
            <li class="active">Page: {{ $page->name }} ( {{ $page->id }} )</li>
        </ol>
    </section>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <?php
            if (Session::has('message')) {  $message = Session::get('message'); ?>
            <label class="control-label" for="inputSuccess"><i class="fa fa-check"></i> <?php echo $message ?></label>
            <?php } ?>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Add Feed</h3>
                </div><!-- /.box-header -->
                {!! Form::open(array('url' => 'admin/pages/edit/'.$page->id.'/update', 'role' => 'form')) !!}
                <div class="box-body">
                    {!! Form::label('Title') !!}
                    {!! Form::text('title', $page->title ,array('required', 'class'=>'form-control', 'placeholder'=>'Page title')) !!}
                </div>
                <div class="box-body">
                    {!! Form::label('Permalink') !!}
                    <a href="{{  url('pages/'.$page->slug) }}" target="_blank">{{  url('pages/'.$page->slug) }}</a>
                </div>
                <div class="box-body">
                    {!! Form::label('Content') !!}
                    {!! Form::textarea('content', $page->content ,array('id'=>'editor1', 'required', 'class'=>'form-control', 'placeholder'=>'Page Content')) !!}
                </div>
                <div class="box-body">
                    {!! Form::label('Active') !!}
                    @if ($page->status)
                        {!! Form::checkbox('active', 1, true) !!}
                    @else
                        {!! Form::checkbox('active', 1) !!}
                    @endif

                </div>
                <div class="box-footer">
                    {!! Form::submit('Send', array('class'=>'btn btn-primary')) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    </div>
@endsection
@section('custom_js')
    <script>
        $(function () {
            CKEDITOR.replace('editor1');
        });
    </script>
@endsection