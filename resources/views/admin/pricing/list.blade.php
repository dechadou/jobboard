@extends('admin.base')
@section('breadcum')
    <section class="content-header">
        <h1>
            Dashboard
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Pricing</li>
        </ol>
    </section>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Pricing</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <table id="data" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Description</th>
                            <th>Value</th>
                            <th>Created at</th>
                            <th>Updated at</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!$pricing->isEmpty())
                        @foreach ($pricing as $p)
                            <tr>
                                <td>{{$p->id}}</td>
                                <td>{{$p->description}}</td>
                                <td>${{$p->value}}</td>
                                <td>{{$p->created_at}}</td>
                                <td>{{$p->updated_at}}</td>
                                <td>
                                    <a href="{{ url('admin/pricing/edit/'.$p->id) }}" class="btn btn-small"><i class="fa fa-edit"></i>Edit</a>
                                </td>

                            </tr>
                        @endforeach
                            @endif
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->

@endsection
@section('custom_js')
    <script type="text/javascript">
        $(function () {
            $("#data").dataTable();
        })
    </script>
@endsection