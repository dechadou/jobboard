@include('admin.inc.header')
@include('admin.inc.menu')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
@yield('breadcum')
<!-- Main content -->
<section class="content">
@yield('content')
</section><!-- /.content -->
</div><!-- /.content-wrapper -->

@include('admin.inc.footer')
@include('admin.inc.control-sidebar')

</div><!-- ./wrapper -->
@include('admin.inc.scripts')
@yield('custom_js')
</body>
</html>
