@include($theme.'.inc.header')
<section class="container register success">
    <div class="forms">
        <div class="row">
            <h2>Bienvenido <br>{{ $user->name }}!</h2>
            <img src="{{ userAvatar($logged_user) }}" class="circle">
            <div class="card">
                <p>Remember complete your Profile and upload your CV </p>
                @if ($is_company)
                    <a href="{{ url('/company/') }}" class="btn blue width232 margin45 no-shadow radius5">Complete my profile <i class="fa fa-chevron-right"></i> </a>
                @else
                    <a href="{{ url('/profile/') }}" class="btn blue width232 margin45 no-shadow radius5">Complete my profile <i class="fa fa-chevron-right"></i> </a>
                @endif
                <a href="{{ url('/') }}" class="btn blue width232 margin45 no-shadow radius5">Search Jobs <i class="fa fa-chevron-right"></i> </a>
            </div>
        </div>
    </div>
</section>
@include($theme.'.inc.scripts')

<script>
    $(document).ready(function(){

            @foreach ($errors->all() as $error)
                $.notify("{{ $error }}",'error');
            @endforeach
    })
</script>
