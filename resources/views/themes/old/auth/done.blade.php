@include($theme.'.inc.header')
<section class="container forgot">
    <div class="alignVertical">
        <div class="row center-align header">
            <div class="logo"></div>
        </div>
        <div class="row">
            <div class="card">
                <h2>Done</h2>
                <p style="margin-bottom:40px"><p>Please check your email for<br>
                    password reset instructions</p></p>
                <a href="{{ url('/') }}" class="btn blue width232 margin45 no-shadow radius5 center">Close </a>
            </div>
        </div>
    </div>
</section>
@include($theme.'.inc.scripts')
