@include($theme.'.inc.header')
<section class="container register create">
    <div class="forms">
        <div class="row header">
            <a class="back left-align" href="{{ url('login/register') }}"><i class="fa fa-chevron-left"></i> </a>
            <h2>Create an Account</h2>
        </div>
        <div class="row">
            <div class="col s12">
                <ul class="tab">
                    <li class="col s4 active"><a href="#" data-target="#candidates">Candidates</a></li>
                    <li class="col s4"><a href="#" data-target="#recruiters">Recruiters</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col s8" style="margin-top:-4px">
                <div id="candidates" class="col s4 active create-form">
                    <a href="{{ url('/login/service/Linkedin') }}" class="btn darkblue width232 no-shadow radius5 margin45"><i class="fa fa-linkedin"></i> Connect with LinkedIn</a>
                    <div class="separador"><hr> or <hr></div>
                    <form action="{{ url('/login/create/candidate') }}" method="post">
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}">
                        <input type="text" placeholder="name" name="name">
                        <input type="text" placeholder="email" name="email">
                        <input type="password" placeholder="password" name="password">
                        <button type="submit" class="btn blue width232 no-shadow radius5 margin45">Create Account</button>
                    </form>

                </div>
                <div id="recruiters" class="col s4 create-form">
                    <a href="{{ url('/login/service/Linkedin') }}" class="btn darkblue width232 no-shadow radius5 margin45"><i class="fa fa-linkedin"></i> Connect with LinkedIn</a>
                    <div class="separador"><hr> or <hr></div>
                    <form action="{{ url('/login/create/recruiter') }}" method="post">
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}">
                        <input type="text" placeholder="Company Name" name="name">
                        <input type="text" placeholder="email" name="email">
                        <input type="password" placeholder="password" name="password">
                        <button type="submit" class="btn red width232 no-shadow radius5 margin45">Create Account</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@include($theme.'.inc.scripts')

<script>
    $(document).ready(function(){

            @foreach ($errors->all() as $error)
                $.notify("{{ $error }}",'error');
            @endforeach
            if (window.location.hash == '#recruiter'){
                $('ul.tab li a[data-target="#candidates"]').parent().removeClass('active');
                $('ul.tab li a[data-target="#recruiters"]').parent().addClass('active');
                $('#candidates').removeClass('active');
                $('#recruiters').addClass('active');
            }
                $('ul.tab li a').on('click',function(e){
                    e.preventDefault();
                    $('ul.tab li a').parent().removeClass('active');
                    if (!$(this).parent().hasClass('active')){
                        $(this).parent().addClass('active');
                    }
                    var elm = $($(this).data('target'));
                    $('#candidates, #recruiters').removeClass('active');
                    if (!elm.hasClass('active')){
                        elm.addClass('active')
                    }
                })
    })
</script>
