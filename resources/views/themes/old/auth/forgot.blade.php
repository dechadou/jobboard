@include($theme.'.inc.header')
<section class="container forgot">
    <div class="alignVertical">
        <div class="row center-align header">
            <div class="logo"></div>
        </div>
        <div class="row">
            <div class="card">
                <h2>Forgot your Password?</h2>
                <p style="margin-bottom:40px">Enter your email address and<br>
                    we'll help you reset your password.</p>
                <form action="{{ url('login/forgot/send') }}" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="text" placeholder="Email" name="email">
                <a href="#" class="btn blue width232 margin45 no-shadow radius5 center">Continue </a>
                </form>
            </div>
        </div>
    </div>
</section>
@include($theme.'.inc.scripts')
<script>
    $(document).ready(function(){
        $('.row a').on('click',function(e){
            e.preventDefault();
            $('form').submit();
        })
        @if (session()->has('error'))
            $.notify("{{ session('error') }}", "error");
        @endif
    })
</script>