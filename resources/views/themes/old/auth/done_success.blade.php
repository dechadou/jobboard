@include($theme.'.inc.header')
<section class="container forgot">
    <div class="alignVertical">
        <div class="row center-align header">
            <div class="logo"></div>
        </div>
        <div class="row">
            <div class="card">
                <h2>Done</h2>
                <p style="margin-bottom:40px"><p>Your password has been updated. <br>Please Login to continue</p></p>
                <a href="{{ url('/login') }}" class="btn blue width232 margin45 no-shadow radius5 center">Login </a>
            </div>
        </div>
    </div>
</section>
@include($theme.'.inc.scripts')
