@include($theme.'.inc.header')
<section class="container login">

    <div class="row center-align header">
        <div class="logo"></div>
        <h1>Lorem ipsum dolor sit amet consectetur.</h1>
        <h2>Adipiscing elit enean feugiat mollis imperdiet ellentesque ex eget sapien </h2>
    </div>

    <div class="row forms">
        <div class="login-form col s5">
            <h1> Login </h1>
            <a href="{{ url('/login/service/Linkedin') }}" class="btn darkblue width232 no-shadow radius5 margin45"><i class="fa fa-linkedin"></i> Connect with LinkedIn</a>
            <div class="separador"><hr> or <hr></div>
            <form action="{{ url('/login/auth') }}" method="post">
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}">
                <input type="text" placeholder="email" name="email" value="{{ old('email') }}">
                <input type="password" placeholder="password" name="password">
                <input type="hidden" name="return_url" value="{{ $return_url }}">
                <input type="hidden" name="action" value="{{ $action }}">
                <input type="hidden" name="job_id" value="{{ $job_id }}">
                <a class="forgot" href="{{ url('/login/forgot') }}">Forgot your password?</a>
                <button type="submit" class="btn blue width232 no-shadow radius5 margin45">Log In</button>
            </form>
        </div>

        <div class="register col s5">
            <h1> Not a Member? </h1>
            <p>At vero eos et accusamus imos
                us qui ditiis praesentium tatum deleniti atque corrupti.</p>

            <ul>
                <li>Quos dolores et quas moles.</li>
                <li>Non provident, similique.</li>
                <li>Culpa qui officia deserunt.</li>
            </ul>

            <a href="{{ url('login/register') }}" class="btn red width232 margin45 no-shadow radius5">
                Create an account
            </a>
        </div>

    </div>
</section>
@include($theme.'.inc.scripts')
@if (session()->has('error'))
    <script>
        $(document).ready(function(){
            $('input[name="email"]').notify("{{ session('error') }}", "error");
            })
    </script>
@endif