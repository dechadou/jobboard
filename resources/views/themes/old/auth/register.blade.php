@include($theme.'.inc.header')
<section class="container register">
    <div class="row forms">
        <div class="row header">
            <a class="back left-align" href="{{ url('login') }}"><i class="fa fa-chevron-left"></i> </a>
            <h2 class="center-align">Create an Account</h2>
        </div>
        <div class="candidates col s5">
            <h1> Candidates </h1>
            <p>At vero eos et accusamus imos
                us qui ditiis praesentium tatum deleniti atque corrupti.</p>

            <ul>
                <li>Quos dolores et quas moles.</li>
                <li>Non provident, similique.</li>
                <li>Culpa qui officia deserunt.</li>
            </ul>

            <a href="{{ url('login/create#candidate') }}" class="btn blue width232 margin45 no-shadow radius5">
                Create an account
            </a>
        </div>

        <div class="recruiters col s5">
            <h1> Recruiters </h1>
            <p>At vero eos et accusamus imos
                us qui ditiis praesentium tatum deleniti atque corrupti.</p>

            <ul>
                <li>Quos dolores et quas moles.</li>
                <li>Non provident, similique.</li>
                <li>Culpa qui officia deserunt.</li>
            </ul>

            <a href="{{ url('login/create#recruiter') }}" class="btn red width232 margin45 no-shadow radius5">
                Create an account
            </a>
        </div>

    </div>
</section>
@include($theme.'.inc.scripts')
@if (session()->has('error'))
    <script>
        $(document).ready(function(){
            $('input[name="email"]').notify("{{ session('error') }}", "error");
            })
    </script>
@endif