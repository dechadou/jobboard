@include($theme.'.inc.header')
<section class="container forgot">
    <div class="alignVertical">
        <div class="row center-align header">
            <div class="logo"></div>
        </div>
        <div class="row">
            <div class="card">
                <h2>Change your Password</h2>
                <p>Please write your new password</p>
                <form method="post" action="{{ url('password/confirm') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="token" value="{{ $token }}">
                    <input type="password" name="password" placeholder="New password">
                    <input type="password" name="password_confirmation" placeholder="Confirm password">
                </form>
                <a href="#" class="btn blue width232 margin45 no-shadow radius5 center">Continue </a>
            </div>
        </div>
    </div>
</section>
@include($theme.'.inc.scripts')
<script>
    $(document).ready(function(){
        $('.row a').on('click',function(e){
            e.preventDefault();
            $('form').submit();
        })
        @foreach ($errors->all() as $error)
            $.notify("{{ $error }}",'error');
        @endforeach
})
</script>