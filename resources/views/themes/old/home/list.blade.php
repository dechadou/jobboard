@extends('themes.default.base')
@section('content')
    <section class="container home">
        @if(!$jobs->isEmpty())
            <div class="row cards">
                @foreach($jobs as $job)
                    @include($theme.'.modules.job_card',['job' => $job])
                @endforeach
            </div>
        @endif
        @if($total_jobs > 16)
            <div class="row">
                <div class="col s12 center-align">
                    <a href="#" class="waves-effect waves-light btn brown no-shadow loadMore" data-offset="{{ count($jobs) }}">Load More</a>
                </div>
            </div>
        @endif
    </section>
@endsection
@section('custom_js')
@endsection