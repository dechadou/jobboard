@extends($theme.'.company_profile.base',['menu'=>'billing_info'])
@section('profile_content')
    <div class="col s9 billing_info">
        <a class="btn red addNew">Add New</a>
        <div class="hiddenInfo hide">
            {!! Form::open(array('url' => 'company/addBillingInfo', 'request' => 'form')) !!}
            <div class="row">
                <div class="input-field col s6">
                    <input placeholder="John" name="firstName" id="first_name" type="text">
                    <label for="first_name">First Name</label>
                </div>
                <div class="input-field col s6">
                    <input id="last_name" placeholder="Smith" name="lastName" type="text">
                    <label for="last_name">Last Name</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input placeholder="My Company" value="{{ $logged_user->company->name }}" id="company" name="company" type="text">
                    <label for="company">Company</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input placeholder="My Street 5555" id="address" name="address" type="text">
                    <label for="address">Address</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input placeholder="California" id="city" name="city" type="text">
                    <label for="city">City</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <input placeholder="Placeholder" id="state" name="state" type="text">
                    <label for="state">State / Province</label>
                </div>
                <div class="input-field col s6">
                    <input placeholder="12345" id="zip" type="text" name="zip" >
                    <label for="zip">Zip / Postal Code</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input placeholder="United States" id="country" name="country" type="text" class="validate">
                    <label for="country">Country</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <input placeholder="+1 801 111 1111" id="phoneNumber" name="phoneNumber" type="text">
                    <label for="phoneNumber">Phone Number</label>
                </div>
                <div class="input-field col s6">
                    <input placeholder="+1 801 111 1111" id="faxNumber" name="faxNumber" type="text">
                    <label for="faxNumber">Fax Number</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input placeholder="Number without spaces or hypens" id="creditcard" name="cardNumber" type="text">
                    <label for="creditcard">Credit Card</label>
                </div>
            </div>
            <div class="row">
                <div class="col s3">
                    <label>Expiration Month</label>
                    <select class="browser-default" name="expirationMonth">
                        <option value="01">01</option>
                        <option value="02">02</option>
                        <option value="03">03</option>
                        <option value="04">04</option>
                        <option value="05">05</option>
                        <option value="06">06</option>
                        <option value="07">07</option>
                        <option value="08">08</option>
                        <option value="09">09</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                    </select>
                </div>
                <div class="col s3" style="margin-bottom:10px">
                    <label>Expiration Year</label>
                    <select class="browser-default" name="expirationYear">
                        <option value="16">2016</option>
                        <option value="17">2017</option>
                        <option value="18">2018</option>
                        <option value="19">2019</option>
                        <option value="20">2020</option>
                        <option value="21">2021</option>
                        <option value="22">2022</option>
                        <option value="23">2023</option>
                        <option value="24">2024</option>
                        <option value="25">2025</option>
                        <option value="26">2026</option>
                        <option value="27">2027</option>
                        <option value="28">2028</option>
                    </select>
                </div>

            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input placeholder="cardCode found on the back of the card" name="cardCode" id="cardcode" type="text">
                    <label for="cardcode">Card Code</label>
                </div>
            </div>
            <div class="row">
                {!! Form::submit('Save', array('class'=>'btn red margin45 valign draft no-shadow radius5')) !!}
            </div>
            {!! Form::close() !!}
        </div>
        <ul class="collection">
            @foreach($billing['paymentProfiles'] as $bill)
                <li class="collection-item">
                    <span class="title">Payment Profile: {{ $bill['customerPaymentProfileId'] }}</span>
                    <p>{{ $bill['billTo']['firstName']. ' '.$bill['billTo']['lastName'] }}<br>
                        {{ $bill['billTo']['address'] }}
                    </p>
                    <div>Credit Card ended in {{ $bill['payment']['creditCard']['cardNumber'] }}
                        <a href="{{ url('/company/destroyBillingInfo/'.$bill['customerPaymentProfileId']) }}" class="secondary-content delete"><i class="material-icon"> Delete</i></a>
                        <a href="#!" class="secondary-content edit"><i class="material-icon"> Edit</i></a>
                    </div>
                    <div class="hiddenInfo hide">
                        {!! Form::open(array('url' => 'company/updateBillingInfo/'.$bill['customerPaymentProfileId'], 'request' => 'form')) !!}
                        <div class="row">
                            <div class="input-field col s6">
                                <input placeholder="John" name="firstName" id="first_name" value="{{ $bill['billTo']['firstName'] }}" type="text">
                                <label for="first_name">First Name</label>
                            </div>
                            <div class="input-field col s6">
                                <input id="last_name" placeholder="Smith" name="lastName" value="{{ $bill['billTo']['lastName'] }}" type="text">
                                <label for="last_name">Last Name</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="My Company" value="{{ $logged_user->company->name }}" id="company" name="company" type="text">
                                <label for="company">Company</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="My Street 5555" id="address" name="address" value="{{ $bill['billTo']['address'] }}" type="text">
                                <label for="address">Address</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="California" id="city" name="city" type="text" value="{{ $bill['billTo']['city'] }}">
                                <label for="city">City</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <input placeholder="Placeholder" id="state" name="state" type="text" value="{{ $bill['billTo']['state'] }}">
                                <label for="state">State / Province</label>
                            </div>
                            <div class="input-field col s6">
                                <input placeholder="12345" id="zip" type="text" name="zip" value="{{ $bill['billTo']['zip'] }}">
                                <label for="zip">Zip / Postal Code</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="United States" id="country" name="country" type="text" class="validate" value="{{ $bill['billTo']['country'] }}">
                                <label for="country">Country</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <input placeholder="+1 801 111 1111" id="phoneNumber" name="phoneNumber" type="text" value="{{ $bill['billTo']['phoneNumber'] }}">
                                <label for="phoneNumber">Phone Number</label>
                            </div>
                            <div class="input-field col s6">
                                <input placeholder="+1 801 111 1111" id="faxNumber" name="faxNumber" type="text" value="{{ $bill['billTo']['faxNumber'] }}">
                                <label for="faxNumber">Fax Number</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="Number without spaces or hypens" id="creditcard" name="cardNumber" type="text" value="{{ $bill['payment']['creditCard']['cardNumber'] }}">
                                <label for="creditcard">Credit Card</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s3">
                                <label>Expiration Month</label>
                                <select class="browser-default" name="expirationMonth">
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                </select>
                            </div>
                            <div class="col s3" style="margin-bottom:10px">
                                <label>Expiration Year</label>
                                <select class="browser-default" name="expirationYear">
                                    <option value="16">2016</option>
                                    <option value="17">2017</option>
                                    <option value="18">2018</option>
                                    <option value="19">2019</option>
                                    <option value="20">2020</option>
                                    <option value="21">2021</option>
                                    <option value="22">2022</option>
                                    <option value="23">2023</option>
                                    <option value="24">2024</option>
                                    <option value="25">2025</option>
                                    <option value="26">2026</option>
                                    <option value="27">2027</option>
                                    <option value="28">2028</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="cardCode found on the back of the card" name="cardCode" id="cardcode" type="text">
                                <label for="cardcode">Card Code</label>
                            </div>
                        </div>
                        <div class="row">
                            {!! Form::submit('Save', array('class'=>'btn red margin45 valign draft no-shadow radius5')) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
@endsection