@extends($theme.'.company_profile.base',['menu'=>'job_applications'])
@section('profile_content')
    <div class="col s9">
        @include($theme.'.company_profile.searchBar')
        <div class="row jobs">
            @if(!$jobs->isEmpty())
                @foreach($jobs as $job)
                    <div class="card">
                        <h1>{{ $job->title }}</h1>
                        @if(!$job->applied->isEmpty())
                            <table>
                                <thead>
                                <th>Avatar</th>
                                <th>User</th>
                                <th>Email</th>
                                <th>User Comments</th>
                                <th>Files Attached</th>
                                </thead>
                                <tbody>
                                @foreach($job->applied as $applied)
                                    <td><img src="{{ userAvatar($applied) }}" width="100px"></td>
                                    <td>{{ $applied->name }}</td>
                                    <td><a href="mailto:{{ $applied->email }}">{{ $applied->email }}</a></td>
                                    <td>{{ $job->appliedDetails($job->id)[0]->user_comments }}</td>
                                    <td>{!! $job->appliedFiles($job->appliedDetails($job->id)[0]->selected_files) !!}</td>
                                @endforeach
                                </tbody>

                            </table>
                        @else
                            <p>No users had applied for this job yet.</p>
                        @endif
                    </div>
                @endforeach
            @else
                <p>You dont have any Job post yet. Want to make one?</p>
            @endif
        </div>

    </div>
@endsection
