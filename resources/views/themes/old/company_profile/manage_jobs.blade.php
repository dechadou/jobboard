@extends($theme.'.company_profile.base',['menu'=>'manage_jobs'])
@section('profile_content')
    <div class="col s9">
        @include($theme.'.company_profile.searchBar')
        <div class="row jobs">

            @if(!$jobs->isEmpty())
                @foreach($jobs as $job)
                    @if($job->status)
                        @include($theme.'.modules.manage_cards',['job' => $job])
                    @endif
                @endforeach
            @else
                You dont have any Jobs yet. <a href="{{ url('jobs/post') }}">Add one now!</a>
            @endif
        </div>

    </div>
@endsection
