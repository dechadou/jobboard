<div data-link="{{ getPermaLink($job) }}" class="col card medium @if(isRevised($job->id)) visited @endif jobCard" data-updated="{{ strtotime($job->updated_at) }}" data-id="{{ $job->id }}">
    <div class="top-content">
        <div class="location">
            <i class="mdi-communication-location-on"></i>
            @if($job->locations != null)
                {{$job->locations->state}}, {{ short_city($job->locations->city) }}
            @else
                <span class="zipLocation" data-zip="{{$job->zip_code}}"></span>
            @endif
        </div>
        @if($job->featured)
            <div class="featured">
                <i class="fa fa-star"></i>
            </div>
        @endif
        <div class="clear"></div>
        <div class="divider"></div>
        <h2>{{ str_limit(ucwords($job->title),40) }}</h2>
        <h3>{{$job->companies->name}}</h3>
    </div>
    <div class="company-logo">
        <img src="{{ CompanyLogo($job->companies->user)}}" class="circle" width="82px" height="82px">
    </div>
    <div class="bottom-content">
        <p>{!! str_limit(strip_tags($job->the_job),100) !!}</p>
        <div class="card-actions hide">
            <ul>
                <a href="{{ getPermaLink($job) }}"><li><i class="mdi-image-remove-red-eye"></i></li></a>
                @if($logged_user)
                    <a href="#" class="@if(isFav($job->id,$logged_user_favs)) selected removeFav @else addToFav @endif" data-id="{{ $job->id }}"><li><i class="mdi-action-favorite"></i></li></a>
                @else
                    <a href="{{ url('login?return_url='.Request::url().'&action=heart&job_id='.$job->id) }}"><li><i class="mdi-action-favorite"></i></li></a>
                @endif
                <a
                        href="#"
                        data-url="{{ getPermaLink($job) }}"
                        data-title="{{$job->title}}"
                        data-descr="{{ str_limit(strip_tags($job->the_job),60) }}"
                        data-image="{{ CompanyLogo($job->companies->user)}}"
                        class="share">
                    <li><i class="mdi-social-share"></i></li>
                </a>
            </ul>
        </div>
    </div>
</div>