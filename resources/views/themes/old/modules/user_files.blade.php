<div id="user_files" class="modal bottom-sheet">
    <div class="modal-content">
        <h4>Select your file</h4>
        <ul class="collection">
           @foreach($user_files as $file)
                <li class="collection-item avatar">
                    <span class="circle"><i class="file-icon {{ get_file_extension($file->file) }}"></i></span>
                    <span class="title">{{ $file->file_name }}</span>
                    <a href="#" data-fileId="{{ $file->id }}" data-fileName="{{ $file->file_name }}" data-filePath="{{ asset('uploads/user_files/'.$logged_user->id.'/'.$file->file) }}" class="addSelectedFile secondary-content"><i class="material-icons">Add</i></a>
                </li>
            @endforeach
        </ul>
    </div>
</div>
