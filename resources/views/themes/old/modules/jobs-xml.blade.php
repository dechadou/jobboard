<?php print '<?xml version="1.0" encoding="UTF-8" ?>'; ?>
<source xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<publisher>Hoojobs</publisher>
<publisherurl>{{ url() }}</publisherurl>

@foreach($jobs as $j)

    @foreach($j->categories as $category)
        <?php $cat[] = $category->name; ?>
    @endforeach

    @foreach([0=>'Full Time',1=>'Part Time',2=>'Freelance'] as $key => $value )
        @if($j->job_type == $key)
            <?php $jType = $value; ?>
        @endif
    @endforeach


    <job>
        <title><![CDATA[ {{$j->title}} ]]></title>
        <date><![CDATA[  {{$j->updated_at}} ]]></date>
        <referencenumber><![CDATA[ {{ $j->id }} ]]></referencenumber>
        <url><![CDATA[ {{ url('jobs/view/'.$j->id.'/'.str_slug($j->title)) }} ]]></url>
        <company><![CDATA[ {{ $j->companies->name }} ]]></company>
        <country><![CDATA[ {{ $j->locations->name }} ]]></country>
        <description><![CDATA[ {{ strip_tags($j->the_job) }} ]]></description>
        <candidate_description><![CDATA[ {{ strip_tags($j->the_candidate) }} ]]></candidate_description>
        <category><![CDATA[ {{ implode( ', ', $cat ) }} ]]></category>
        <job_type><![CDATA[ {{ $jType }} ]]></job_type>
    </job>

@endforeach

</source>