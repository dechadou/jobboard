@include($theme.'.inc.header')
@include($theme.'.inc.header_html')
<div class="body-menu">

    @yield('content')
    @include($theme.'.inc.footer')
    @include($theme.'.inc.menu')
</div>
@include($theme.'.inc.scripts')
@yield('custom_js')


@if(session()->has('message'))
    <script>
      $(document).ready(function(){
        $.notify("{{ session('message') }}", 'success');
      })
    </script>
@endif
</body>
</html>
