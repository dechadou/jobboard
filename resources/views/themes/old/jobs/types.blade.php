@extends($theme.'.base',['header' => 'mini'])
@section('content')
    <section class="container no-margin search">
        <div class="row">
            <div class="col s8 search-term">
                <h1>Positions for {{ strtoupper($type) }} jobs</h1>
                <span class="search-results">{{ $search_totals }}</span>
            </div>
            @if(!$jobs->isEmpty())
                <div class="col s4 search-actions">
                    <a href="#" class="btn-subscribe"><i class="mdi-editor-mode-edit"></i> Subscribe</a>
                    <a href="#" class="btn-share"><i class="mdi-social-share"></i> Share</a>
                </div>
            @endif
        </div>
        <div class="divider max-width"></div>
        <div class="row cards">
            @if(!$jobs->isEmpty())
                @foreach($jobs as $job)
                    @include($theme.'.modules.job_card',['job' => $job])
                @endforeach
            @else
                We could not find any job with these parameters. Please try with new ones. (Falta diseño)
            @endif
        </div>
        @if($search_totals > 10)
            <div class="row">
                <div class="col s12 center-align">
                    <a href="#" class="waves-effect waves-light btn brown">Load More</a>
                </div>
            </div>
        @endif

    </section>
@endsection
@section('custom_js')
    <script>
        @foreach($errors->all() as $error)
        $.notify("{{ $error }}",'error');
        @endforeach
    </script>
@endsection