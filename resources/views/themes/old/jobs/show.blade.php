@extends($theme.'.base',['header' => 'mini'])
@section('content')
    <section class="container postShow">
        @if($job->status == 0)
            <div class="row">
                <ul class="collection">
                    <li class="collection-item">This Job is no longer available.</li>
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col @if($job->status == 0) s12 @elseif ($logged_user) s8 @else s9 @endif remove-padding">
                <div class="card big @if($logged_user) logged @endif">
                    <div class="company-description">
                        <div class="card-top">
                            <div class="location">
                                @if($job->locations != null)
                                    <a href="{{ url('search/cities/'.$job->locations->id) }}">
                                        <i class="mdi-communication-location-on"></i>
                                        {{ $job->locations->state.', '.short_city($job->locations->city) }}
                                    </a>
                                @endif
                            </div>
                            <div class="category">
                                <a href="{{ url('jobs/types/'.str_slug($job->types->name)) }}">
                                    <i class="icon-{{ str_slug($job->types->name) }}"></i>
                                    {{ $job->types->name }}
                                </a>
                            </div>
                            <div class="industry">
                                @if($job->industry_id != '')
                                <a href="{{ url('search/industries/'.$job->industry->id) }}">
                                    <i class="material-icons">business</i>
                                    {{ $job->industry->name }}
                                </a>
                                @else
                                    <a href="{{ url('search/industries/'.$job->industry_other) }}">
                                        <i class="material-icons">business</i>
                                        {{ $job->industry_other }}
                                    </a>
                                @endif

                            </div>
                            <div class="clear"></div>
                            <h2>{{ $job->title }}</h2>
                            <h3>{{ $job->companies->name }}</h3>
                        </div>
                        <div class="company-logo">
                            <a href="{{ url('employer/'.$job->companies->name) }}">
                                <img src="{{ CompanyLogo($job->companies->user) }}" class="circle" width="110px" height="110px">
                            </a>
                        </div>
                        <div class="card-bottom">
                            @foreach($job->categories as $category)
                                <a href="{{ url('search/categories/'.$category->id) }}"><span>{{ $category->name }}</span></a>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="card big">
                    <div class="job-content">
                        <h2>The Job</h2>
                        <p>{!! $job->the_job !!}</p>
                    </div>
                </div>
                <div class="card big">
                    <div class="the-candidate">
                        <h2>The Candidate</h2>
                        <p>{!! $job->the_candidate !!}</p>
                    </div>
                </div>
                <div class="card big">
                    <div class="the-company">
                        <h2>The Company</h2>
                        <p>{!! $job->companies->description !!}</p>
                    </div>
                </div>
            </div>
            @include($theme.'.jobs.partials.'.$partials)

        </div>
        <!--<div id="result"></div>-->
        @include($theme.'.modules.similar_jobs',['similar_jobs' => $similar_jobs])
        @if($logged_user)
            @include($theme.'.modules.user_files',['user_files'=>$logged_user->files])
        @endif
    </section>
@endsection
@section('custom_js')
    @if(session()->has('error'))
        <script>
            $(document).ready(function(){
                $('input[name="email"]').notify("{{ session('error') }}", "error");
            })
        </script>
    @endif
    <script>
        $(document).ready(function(){
            /*$('#fromGDrive').on('click',function(e){
             e.preventDefault();
             app.GDrive();

             })*/

            $('a.fromDropbox').on('click',function(e){
                e.preventDefault();
                app.Dropbox();
            });

            $('a.fromHD').on('click',function(e){
                e.preventDefault();
                $('input[type="file"]').click();
            });

            $('input[type="file"]').on('change',function(e){
                var file = this.files[0];
                app.addFilesFromHD(file);
            });

            $('body').on('click','.uploaded-content ul.selected-files a.removeFile', function(e){
                e.preventDefault();
                $(this).parent().slideUp(function(){
                    $(this).remove();
                    if($('.uploaded-content ul.selected-files li.fileElement').length == 0){
                        $('.uploaded-content').slideUp();
                    }
                });
            });

            $('.modal-trigger').leanModal();

            $('.submit_application').on('click',function(e){
                e.preventDefault();
                var submited_files = [];
                $('.fileElement').each(function(i,v){
                    submited_files.push($('.fileElement').eq(i).attr('data-fileId'));
                });
                $('input[name="user_files"]').val(submited_files);

                // Remove job form Fav if exist;
                $.ajax({
                    type:'GET',
                    url:app.globalPath+'profile/fav_remove/{{$job->id}}',
                    dataType:'json'
                })

                $('form.application').submit();
            })

            @if($is_company)
            $('body').on('click', '.card-actions ul.top li a',function(e){
                e.preventDefault();
                var elem    = $(this);
                var action  = elem.data('action');
                var id      = elem.data('id');
                if (action == 'edit'){
                    window.location.href = elem.attr('href');
                }
                if (action == 'make_featured'){
                    window.location.href = elem.attr('href');
                }
                if (action == 'destroy'){
                    if (confirm('Are you sure you want to remove this Job?')){
                        app.cardAction(id,action,function(){
                            elem.parent().parent().parent().parent().fadeOut();
                        })
                    }
                } else {
                    app.cardAction(id,action,function(data){
                        if (action == 'pause'){
                            if (data['status']){
                                elem.text(data['action']);
                                if (data['action'] == 'Resume'){
                                    elem.parent().addClass('active');
                                } else {
                                    elem.parent().removeClass('active');
                                }
                            } else{
                                window.location.href = data['redirect'];
                            }
                        }
                        if (action == 'copy'){
                            window.location.href = '{{ url('company/saved_drafts') }}'
                        }
                        $.notify(data['message'], 'success');

                    })
                }
            })
            @endif

        })

    </script>
@endsection