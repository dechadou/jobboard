@extends($theme.'.base',['header' => 'mini'])
@section('content')


    <div class="container selectPlan">
        <div class="row">
            <div class="col s8 offset-s2">
                <h1>Pricing</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
            </div>
        </div>
        <div class="row">
            <div class="col s3 offset-s3">
                <div class="card">
                    <div class="title">
                        <h2>Save as you post</h2>
                    </div>
                    <p>Texto</p>
                    <a href="{{ url('jobs/post') }}" class="btn red">Single Post</a>
                </div>
            </div>
            <div class="col s3">
                <div class="card">
                    <div class="title">
                        <h2>Save now post later</h2>
                    </div>
                    <p>Texto</p>
                    <a href="@if($logged_user) {{ url('jobs/save') }} @else {{ url('login') }} @endif" class="btn red">Save now post later</a>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('custom_js')
@endsection