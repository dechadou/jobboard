@extends($theme.'.base',['header' => 'mini'])
@section('content')
    <section class="container">
        {!! Form::open(array('url' => 'jobs/checkout', 'request' => 'form','class'=>'postAJob')) !!}
        @if($company == null)
            <div class="row">
                <div class="col s12 l8 offset-sl2">
                    <p>As placed as (NEED TEXT!)</p>
                    <input type="text" placeholder="Your Name (John Doe)" name="new_user_name" class="big-input">
                    <span class="error" data-name="new_user_name" style="display:none">Please fill your name</span>
                    <input type="text" placeholder="Your Email (name@company.com)" name="new_user_email" class="big-input">
                    <span class="error" data-name="new_user_email" style="display:none">Please fill a valid email</span>
                    <input type="text" placeholder="Your Phone Number (1 555 555 555)" name="new_user_phone" class="big-input">
                    <span class="error" data-name="new_user_phone" style="display:none">Please fill a valid phone number</span>

                    <input type="hidden" name="new_user" value="1">
                </div>
            </div>
            <hr>
        @else
            <input type="hidden" name="new_user" value="0">
        @endif
        <div class="row">
            <div class="col s8 offset-s2">
                {!! Form::text('job_name', old('job_name'), array('class'=>'big-input countChars', 'placeholder'=>'Job Name','onkeyup'=>'app.countChars(this,74)','maxlength'=>'74')) !!}
                <span class="charCounter">73</span>
                <span class="error" data-name="job_name" style="display:none">Please fill the Job Name</span>
            </div>
        </div>
        <div class="row">
            <div class="col s8 offset-s2">
                <?php $company_name = ($company != null) ? $company->name : '' ?>
                <?php $value = (null !== old('company_name')) ? old('company_name') : $company_name ?>
                {!! Form::text('company_name', $value, array('class'=>'big-input', 'placeholder'=>'Company Name','maxlength'=>'58')) !!}
                    <span class="error" data-name="company_name" style="display:none">Please fill the Company Name</span>
            </div>
        </div>
        <div class="row">
            <div class="col s8 offset-s2 box">
                <div class="valign-wrapper">
                    <h1 class="valing">Job Location</h1>
                    <a href="#" class="valign btn radius5 btnSpecialHeight no-shadow noActive margin45">remote position</a>
                </div>

                <div class="valign-wrapper">
                    {!! Form::text('zip_code', old('zip_code'), array('class'=>'col s2 valign normal-input', 'placeholder'=>'Zip Code')) !!}
                    <span class="col s1 valign padding3rm type20 light">or</span>
                    {!! Form::text('location', old('zip_code'), array('id'=> 'autocomplete', 'class'=>'col s12 m8 valign normal-input arrows', 'placeholder'=>'Job Location')) !!}
                    <input type="hidden" name="location_id">
                </div>
                <span class="error" data-name="location" style="display:none">Please fill the Zip code OR location</span>
            </div>
        </div>
        <div class="row">
            <div class="col s8 offset-s2 box">
                <h1>The Job</h1>
                {!! Form::textarea('the_job', old('the_job'), array('id'=> 'the_job', 'placeholder'=>'Here you can tell the applicants what will be their main duties and<br>responsabilities. You can add details as who their supervisor will be and in which<br>area they will be working.')) !!}
                <span class="error" data-name="the_job" style="display:none">Please fill the Job Description</span>
            </div>
        </div>
        <div class="row">
            <div class="col s8 offset-s2 box">
                <h1>The Candidate <span class="small">(optional)</span></h1>
                {!! Form::textarea('the_candidate', old('the_candidate'), array('id'=> 'the_candidate', 'placeholder'=>' We recommend using bullet points<br>• What are the required skills for this position<br>• Required previous experience<br>• Educational background')) !!}
                <span class="error" data-name="the_candidate" style="display:none">Please fill the Candidate Description</span>
            </div>
        </div>

        <div class="row">
            <div class="col s8 offset-s2 box">
                <h1>The Company <span class="small">(optional)</span></h1>
                <?php $company_description = ($company != null) ? $company->description : '' ?>
                <?php $value = (null !== old('the_company')) ? old('the_company') : $company_description ?>
                {!! Form::textarea('the_company', $value, array('id'=> 'the_company', 'placeholder'=>'Talk about your company. What kind of industry is it, what are it’s main products and services and all of the information that the candidates might found interesting.')) !!}
                <span class="error" data-name="the_company" style="display:none">Please fill the company description</span>
                <div class="divider"></div>
                <h1>Company Logo <span class="small">(optional)</span></h1>
                <div class="fileInput">
                    <div id="dropzone" class="circle"></div>
                    @if ($company != null)
                        <img src="{{ companyLogo($company->user)}}" class="circle company_logo" width="200xp" height="200px">
                    @else
                        <img src="{{ asset('assets/frontend/images/no-logo.png') }}" class="circle company_logo" width="200xp" height="200px">
                    @endif
                    <p>Drag and drop from your desktop or
                        Upload it in format PNG, JPEG or TIFF</p>
                    <a href="#" class="btn blue radius5 btnSpecialHeight padding3rm no-shadow uploadFileCompany">Browse</a>
                    <input type="file" name="fileCompany" class="hide">
                </div>
            </div>
        </div>

        <div class="row typeOfJob">
            <div class="col s8 offset-s2 box">
                <h1>Type of Job</h1>
                @foreach($jobTypes as $type)
                    <a href="#" data-value="{{ $type->id }}" class="btn no-shadow">{{ $type->name }}</a>
                @endforeach
                <input type="hidden" name="job_type" value="">
                <span class="error" data-name="job_type" style="display:none">Please select a job type</span>
            </div>
        </div>

        <div class="row">
            <div class="col s8 offset-s2 box">
                <h1>Industry</h1>

                <select name="industry_id" class="browser-default">
                    @foreach($industries as $industry)
                        <option value="{{ $industry->id }}">{{ $industry->name }}</option>
                    @endforeach
                    <option value="other">Other</option>
                </select>
                <span class="error" data-name="industry" style="display:none">Please Select a Industry</span>
                <input type="text" name="industry_other" placeholder="Please input your Industry" style="display:none">
                <span class="error" data-name="industry_other" style="display:none">Please fill the industry name</span>
            </div>
        </div>

        <div class="row categories">
            <div class="col s8 offset-s2 box">
                <h1>Categories</h1>
                <h2>Choose up to three. These will help candidates quickly find your job</h2>
                @foreach($g_categories as $cat)
                    <a href="#" data-value="{{ $cat->id }}" class="btn no-shadow">{{ $cat->name }}</a>
                @endforeach
                <input type="hidden" name="categories[]" value="">
                <span class="error" data-name="categories" style="display:none">Please select at least 3 categories</span>
            </div>
        </div>

        <div class="row">
            <div class="col s8 offset-s2 box app_by">
                <h1>Applications by</h1>
                <div class="valign-wrapper">
                    <a href="#" data-value="0" class="valign btn no-shadow">email</a>
                    <a href="#" data-value="1" class="valign btn no-shadow">url</a>
                    <input type="hidden" name="app_by">
                    {!! Form::text('app_by_text', old('app_by_text'), array('class'=>'valign', 'placeholder'=>'Enter URL or Email')) !!}
                </div>
                <span class="error" data-name="app_by" style="display:none">Please fill the Email or URL</span>
            </div>
        </div>

        <div class="row">
            <div class="center-align margin-top-bottom-50">
                <a href="#" data-value="0" class="btn width232 margin45 valign draft no-shadow radius5 draft">Save as draft</a>
                <a href="#" data-value="1" class="btn red width232 margin45 valign no-shadow radius5 publish">Publish</a>
                <input type="hidden" name="job_status">
            </div>
        </div>
        {!! Form::close() !!}
    </section>
@endsection
@section('custom_js')
    <script>
        $(document).ready(function(){


            @foreach($errors->all() as $error)
            $.notify('{{ $error }}','error',
                    {
                        autoHideDelay: 10000
                    });
            @endforeach

            $('#autocomplete').autocomplete({
                 minChars: 2,
                 serviceUrl: app.globalPath + 'locations/list',
                 onSelect: function (suggestion) {
                   $('input[name="location_id"]').val(suggestion.data)
                 }
            });


            $('#dropzone').on('dragover', function(e) {
                event.preventDefault();
                event.stopPropagation();
                $(this).addClass('hover');
            });

            $('#dropzone').on('dragleave', function(e) {
                event.preventDefault();
                event.stopPropagation();
                $(this).removeClass('hover');
            });

            $('#dropzone').on('drop', function(e) {
                event.preventDefault();
                event.stopPropagation();
                app.dropUpload(e);
                $(this).removeClass('hover');
            });


            var categories = [];
            $('.categories a').on('click',function(e){
                e.preventDefault();
                var value = $(this).data('value')-1;
                if (categories.indexOf(value) == -1){
                    if (categories.length == 3){
                        $.notify('You can only select 3 categories','warning');
                        $(this).removeClass("active");
                        return false;
                    }
                    categories.push(value);

                } else {
                    index = categories.indexOf(value);
                    categories.splice(index, 1);

                }
                ($(this).hasClass('active')) ? $(this).removeClass('active') : $(this).addClass('active');
                $('input[name="categories[]"]').val(categories);
            });

            $('.typeOfJob a').on('click',function(e){
                e.preventDefault();
                $('.typeOfJob a').removeClass('active');
                $(this).addClass('active');
                $('input[name="job_type"]').val($(this).data('value'));
            });

            $('.app_by a').on('click',function(e){
                e.preventDefault();
                $('.app_by a').removeClass('active');
                $(this).addClass('active');
                $('input[name="app_by"]').val($(this).data('value'));
            });

            $('select[name="industry_id"]').on('change', function(e){
                if ($(this).val() == 'other'){
                    $('input[name="industry_other"]').fadeIn();
                } else {
                    $('input[name="industry_other"]').fadeOut();
                }
            });


            $('.publish, .draft').on('click',function(e){
                e.preventDefault();
                if(app.validateJobPost($('form.postAJob'))){
                    $('input[name="job_status"]').val($(this).data('value'));
                    $('form').submit();
                }



            });

            app.initTinyMce('the_job');
            app.initTinyMce('the_candidate');




        })
    </script>
@endsection

