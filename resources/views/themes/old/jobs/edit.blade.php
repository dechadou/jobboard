@extends($theme.'.base',['header' => 'mini'])
@section('content')
    <section class="container">
        {!! Form::open(array('url' => 'company/job/'.$job->id.'/store', 'request' => 'form','class'=>'postAJob')) !!}
        <div class="row">
            <div class="col s8 offset-s2">
                <input type="text" @if($job->status) disabled @endif placeholder="Job Name" class="big-input" name="job_name" value="{{ $job->title }}" maxlength="74">
            </div>
        </div>
        <div class="row">
            <div class="col s8 offset-s2">
                <input type="text" @if($job->status) disabled @endif value="{{ $logged_user->name }}" placeholder="Company Name" name="company_name" class="big-input" maxlength="58">
            </div>
        </div>
        <div class="row">
            <div class="col s8 offset-s2 box">
                <div class="valign-wrapper">
                    <h1 class="valing">Job Location</h1>
                </div>

                <div class="valign-wrapper">
                    <input type="text" @if($job->status) disabled @endif placeholder="Zip Code" name="zip_code" class="col s2 valign normal-input">
                    <span class="col s1 valign padding3rm type20 light">or</span>
                    <input type="text" @if($job->status) disabled @endif class="col s8 valign normal-input arrows" name="location" value="@if ($job->locations != null) $job->locations->state.', '.short_city($job->locations->city) @endif">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col s8 offset-s2 box">
                <h1>The Job</h1>
          <textarea name="the_job" id="the_job" placeholder="Here you can tell the applicants what will be their main duties and
responsabilities. You can add details as who their supervisor will be and in which
area they will be working.">{!! $job->the_job !!}</textarea>
            </div>
        </div>
        <div class="row">
            <div class="col s8 offset-s2 box">
                <h1>The Candidate <span class="small">(optional)</span></h1>
          <textarea name="the_candidate" id="the_candidate" placeholder="• We recommend using bullet points
• What are the required skills for this position
• Required previous experience
• Educational background">{!! $job->the_candidate !!}</textarea>
            </div>
        </div>

        <div class="row">
            <div class="col s8 offset-s2 box">
                <h1>The Company <span class="small">(optional)</span></h1>
          <textarea name="the_company" placeholder="Talk about your company. What kind of industry is it, what are it’s main products
and services and all of the information that the candidates might found interesting.">{{ $logged_user->company->description }}</textarea>
                <div class="divider"></div>
                <h1>Company Logo <span class="small">(optional)</span></h1>
                <div class="fileInput">
                    <img src="{{ companyLogo($logged_user) }}" class="circle" width="200px" height="200px">
                    <p>Drag and drop from your desktop or
                        Upload it in format PNG, JPEG or TIFF</p>
                    <a href="#" class="btn blue radius5 btnSpecialHeight padding3rm no-shadow">Browser</a>
                    <input type="file" name="logo" class="hide">
                </div>
            </div>
        </div>

        <div class="row typeOfJob">
            <div class="col s8 offset-s2 box">
                <h1>Type of Job</h1>
                @foreach($jobTypes as $type)
                    <a href="#" data-value="{{ $type->id }}" class="btn no-shadow @if($job->job_type == $type->id) active @endif">{{ $type->name }}</a>
                @endforeach
                <input type="hidden" name="job_type" value="{{ $job->job_type }}">
            </div>
        </div>

        <div class="row">
            <div class="col s8 offset-s2 box">
                <h1>Industry</h1>

                <select name="industry_id" class="browser-default">
                    @foreach($industries as $industry)
                        <option value="{{ $industry->id }}" @if($job->industry_id == $industry->id) selected @endif>{{ $industry->name }}</option>
                    @endforeach
                    <option value="other" @if($job->industry_other != '') selected @endif>Other</option>
                </select>
                <input type="text" name="industry_other" placeholder="Please input your Industry" @if($job->industry_other != '')value="{{ $job->industry_other }}"@else style="display:none"@endif>
            </div>
        </div>

        <div class="row categories">
            <div class="col s8 offset-s2 box">
                <h1>Categories</h1>
                <h2>Choose up to three. These will help candidates quickly find your job</h2>
                @foreach($g_categories as $cat)
                    <a href="#" data-value="{{ $cat->id }}" class="btn no-shadow @if(in_array($cat->id,$cId)) active @endif  ">{{ $cat->name }}</a>
                @endforeach
                <input type="hidden" name="categories[]" value="{{ implode(',',$cId) }}">
            </div>
        </div>

        <div class="row">
            <div class="col s8 offset-s2 box app_by">
                <h1>Applications by</h1>
                <div class="valign-wrapper">
                    <a href="#" data-value="0" class="valign btn no-shadow @if($job->app_by == 0) active @endif">email</a>
                    <a href="#" data-value="1" class="valign btn no-shadow @if($job->app_by == 1) active @endif">url</a>
                    <input type="hidden" name="app_by" value="{{ $job->app_by }}">
                    <input type="text" class="valign" name="app_by_text" value="{{ $job->app_by_text }}">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="center-align margin-top-bottom-50">
                <a href="#" data-value="0" class="btn width232 margin45 valign draft no-shadow radius5 draft">Save as draft</a>
                <a href="#" data-value="1" class="btn red width232 margin45 valign no-shadow radius5 publish">Publish</a>
                <input type="hidden" name="job_status">
            </div>
        </div>
        {!! Form::close() !!}
    </section>
@endsection
@section('custom_js')
    <script>
        $(document).ready(function(){

            @foreach($errors->all() as $error)
               $.notify('{{ $error }}','error');
            @endforeach



            $('#autocomplete').autocomplete({
                        minChars: 2,
                        serviceUrl: app.globalPath + 'locations/list',
                        onSelect: function (suggestion) {
                            $('input[name="location_id"]').val(suggestion.data)
                        }
                    });

            var categories = [{{ implode(',',$cId) }}];
            $('.categories a').on('click',function(e){
                e.preventDefault();
                var value = $(this).data('value');
                if (categories.indexOf(value) == -1){
                    if (categories.length == 3){
                        $.notify('You can only select 3 categories','warning');
                        $(this).removeClass("active");
                        return false;
                    }
                    categories.push(value);

                } else {
                    index = categories.indexOf(value);
                    categories.splice(index, 1);

                }
                ($(this).hasClass('active')) ? $(this).removeClass('active') : $(this).addClass('active');
                $('input[name="categories[]"]').val(categories);
            })
            $('.typeOfJob a').on('click',function(e){
                e.preventDefault();
                $('.typeOfJob a').removeClass('active');
                $(this).addClass('active');
                $('input[name="job_type"]').val($(this).data('value'));
            })
            $('.app_by a').on('click',function(e){
                e.preventDefault();
                $('.app_by a').removeClass('active');
                $(this).addClass('active');
                $('input[name="app_by"]').val($(this).data('value'));
            })

            $('.publish, .draft').on('click',function(e){
                e.preventDefault();
                $('input[name="job_status"]').val($(this).data('value'));
                $('form').submit();
            })

            app.initTinyMce('the_job');
            app.initTinyMce('the_candidate');

        })
    </script>

@endsection
