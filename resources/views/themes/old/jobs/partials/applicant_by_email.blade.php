<div class="col s4">

    <a href="#" class="btn-favourite @if(isFav($job->id,$logged_user_favs)) selected removeFav @else addToFav @endif" data-id="{{ $job->id }}">
        <i class="mdi-action-favorite"></i> Favourite
    </a>
    <a href="#" class="btn-share share"
       data-url="{{ getPermaLink($job) }}"
       data-title="{{$job->title}}"
       data-descr="{{ str_limit($job->the_job,60) }}"
       data-image="{{ CompanyLogo($job->companies->user)}}">
        <i class="mdi-social-share"></i> Share
    </a>
    <div class="card the-applicant">
        <form>
            <h3>Applicant</h3>
            <span class="user-name"><i class="fa fa-user"></i> {{ $logged_user->name }}</span>
            <span class="user-email"><i class="mdi-content-mail"></i>{{ $logged_user->email }}</span>

                {!! Form::open(array('class'=>"uploadFiles", 'url' => '/profile/addFiles', 'files'=>true, 'request' => 'form')) !!}
                {!! Form::file('file', array('required', 'class'=>'form-control hide')) !!}
                {!! Form::close() !!}
                <form class="application" method="post" action="{{ url('jobs/view/'.$job->id.'/apply') }}">
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                    <textarea placeholder="Tell us about you" name="comments"></textarea>
                    <div class="upload-content">
                        <span>Upload a file / add another file</span>
                        <ul>

                            <li><a href="#" class="fromHD tooltips align-left"><i class="fa fa-hdd-o"></i> <span>Upload from your HardDrive</span></a></li>
                            <li><a href="#" id="fromGDrive" class="tooltips align-middleleft"><i class="fa fa-drive"></i> <span>Select from your GDrive Account</span></a></li>
                            <li><a href="#" class="fromDropbox tooltips align-middleright"><i class="fa fa-dropbox"></i> <span>Select from your Dropbox Account</span></a></li>
                            <li><a href="#user_files" class="tooltips align-right modal-trigger"><i class="fa fa-user"></i> <span>Select from Uploaded files</span></a></li>
                        </ul>
                    </div>

                    <div class="uploaded-content" style="display:none">
                        <div class="divider"></div>
                        <span>Selected  files</span>
                        <div class="preloader"></div>
                        <ul class="selected-files">

                        </ul>
                    </div>
                    <div class="divider"></div>
                    <input type="hidden" name="user_files">

                    <input type="checkbox" id="receive_similar" name="receive_similar" />
                    <label for="receive_similar">I want to receive similars job</label>
                    <input type="hidden" name="user_files" value="">
                </form>
                <div class="divider"></div>
                <a href="#" class="btn no-shadow submit_application">Submit application</a>
            @if(in_array($job->id,$logged_user_jobs))
                <p style="text-align:center">You already applied for this job</p>
            @endif
        </form>

    </div>
</div>