<div class="col s4">
    <a href="#" class="btn-favourite @if(isFav($job->id,$logged_user_favs)) selected removeFav @else addToFav @endif" data-id="{{ $job->id }}">
        <i class="mdi-action-favorite"></i> Favourite
    </a>
    <a href="#" class="btn-share share"
       data-url="{{ getPermaLink($job) }}"
       data-title="{{$job->title}}"
       data-descr="{{ str_limit($job->the_job,60) }}"
       data-image="{{ CompanyLogo($job->companies->user)}}">
        <i class="mdi-social-share"></i> Share
    </a>
    <div class="card the-applicant">
        <form>
            <h3>Applicant</h3>
            <!--<span class="user-name"><i class="fa fa-user"></i> {{ $logged_user->name }}</span>
                                    <span class="user-email"><i class="mdi-content-mail"></i>{{ $logged_user->email }}</span>
                                    <div class="divider"></div>-->
            <a href='{{ $job->app_by_text }}?return_url="{{ Request::url().'?action=application' }}"' target="_blank" class="btn no-shadow red" style="line-height:13px;padding-top:9px">Submit application<br><small>on employer websitee</small></a>

        </form>
    </div>
</div>