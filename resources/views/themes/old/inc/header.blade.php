<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>{{ $site_title }}</title>
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic,700italic,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/main.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/style.css') }}" />
    <link rel="shortcut icon" href="{{ $site_favicon }}" />
    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <meta name="csrf-token" content="<?php echo csrf_token() ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Schema.org markup for Google+ -->
    <!--<meta itemprop="name" content="{{ $site_title }}">
    <meta itemprop="description" content="{{ $description }}">
    <meta itemprop="image" content="{{ $site_logo }}">-->

    <!-- Twitter Card data -->
    <!--<meta name="twitter:card" content="{{ $site_logo }}">
    <meta name="twitter:site" content="@publisher_handle">
    <meta name="twitter:title" content="{{ $site_title }}">
    <meta name="twitter:description" content="{{ $description }}">
    <meta name="twitter:creator" content="@author_handle">-->
    <!-- Twitter summary card with large image must be at least 280x150px -->
    <!--<meta name="twitter:image:src" content="{{ $site_logo }}">-->

    <!-- Open Graph data -->
    <!--<meta property="og:title" content="{{ $site_title }}" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="{{ url() }}" />
    <meta property="og:image" content="{{ $site_logo }}" />
    <meta property="og:description" content="{{ $description }}" />
    <meta property="og:site_name" content="{{ $site_title }}" />-->
</head>
<body>