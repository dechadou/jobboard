<div class="filter-box">
    <form class="searchBar">
        <input type="text" name="search_term" placeholder="Your Search">
        <select name="search_city">
            <option value="all">All Cities ({{ $g_locations_total }})</option>
            @foreach($g_locations as $location)
                <option value="{{ $location['id'] }}">
                    {{ $location['state'].', '.short_city($location['city']).' ('.$location['amount'].')' }}
                </option>
            @endforeach
        </select>
        <select name="search_job_type">
            <option value="all">All Job Types ({{ $g_job_types_total }})</option>
            @foreach($g_job_types as $types)
                <option value="{{ $types['id'] }}">
                    {{ $types['name'].' ('.$types['amount'].')' }}
                </option>
            @endforeach
        </select>
        <a href="#" class="btn"><i class="mdi-action-search"></i></a>
    </form>
</div>