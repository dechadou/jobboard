@extends($theme.'.base',['header' => 'mini'])
@section('content')
    <section class="container no-margin search">
        <div class="row">
            <div class="col s8 search-term">
                @if(!empty($search_terms['term']) && $search_terms['term'] != 'false')
                    <h1>Positions for {{ $search_terms['term'] }} in {{ $search_terms['city'] }}</h1>
                @elseif(!empty($search_terms['city']))
                    <h1>All Positions in {{ $search_terms['city'] }}</h1>
                @endif

                @if(!empty($search_terms['category']))
                    @if($search_terms['category'] == 'all categories')
                        <h1>All Positions for {{ $search_terms['category'] }}</h1>
                    @else
                        <h1>All Positions for Category {{ $search_terms['category'] }}</h1>
                    @endif
                @endif

                    <h1>Positions that matched your search criteria</h1>
                <span class="search-results">{{ $search_totals }}</span>
            </div>
            @if(!$search_results->isEmpty())
                <div class="col s4 search-actions">
                    <a href="#" class="btn-subscribe"><i class="mdi-editor-mode-edit"></i> Subscribe</a>
                    <a href="#" class="btn-share share"
                       data-url="{{ Request::url() }}"
                       data-title="Hoojobs"
                       data-image="{{ ($site_logo != '') ? $site_logo : asset('assets/frontend/images/logo.svg') }}"><i class="mdi-social-share"></i> Share</a>
                </div>
            @endif
        </div>
        <div class="divider max-width"></div>
        <div class="row cards">
            @if(!$search_results->isEmpty())
                @foreach($search_results as $job)
                    @include($theme.'.modules.job_card',['job' => $job])
                @endforeach
            @else
                We could not find any job with these parameters. Please try with new ones. (Falta diseño)
            @endif
        </div>
        @if($search_totals > 10)
            <div class="row">
                <div class="col s12 center-align">
                    <a href="#" class="waves-effect waves-light btn brown">Load More</a>
                </div>
            </div>
        @endif

    </section>
@endsection
@section('custom_js')
    <script>
        @foreach($errors->all() as $error)
        $.notify("{{ $error }}",'error');
        @endforeach
    </script>
@endsection