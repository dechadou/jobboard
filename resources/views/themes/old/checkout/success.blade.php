@extends($theme.'.checkout.base')
<meta http-equiv="refresh" content="10;URL={{ url('jobs/view/'.$job->id.'/'.str_slug($job->title)) }}">

@section('nav_checkout')
    <li><a href="#">1</a></li>
    <li class="active"><a href="#"><strong>2.</strong> Success</a></li>
@endsection
@section('content')
    <div class="container checkout_success">
        <div class="row">
            <div class="col s5 offset-s2 firstCol">
                <h1>Good!</h1>
                <h2>Lorem ipsum dolor sit amet,<br>consectetur adipiscing elit. </h2>
                <a href="{{ url('jobs/view/'.$job->id.'/'.str_slug($job->title)) }}" class="btn red">View Post</a>
                <p>You will be redirected to the job in <span class="counter" style="font-weight:bold">10</span> seconds. If not, click <a href="{{ url('jobs/view/'.$job->id.'/'.str_slug($job->title)) }}">here</a> to view it</p>
            </div>
            <div class="col s4">
                @include($theme.'.modules.job_card',['job' => $job])
            </div>
        </div>
    </div>
@endsection
@section('custom_js')
    <script>
        $(document).ready(function(e){
            var counter = 10;
            setInterval(function(e){
                counter = counter-1;
                if (counter <= 0) {
                    counter = 0;
                }
                $('span.counter').text(counter);
            },1000)
        })
    </script>
@endsection