@extends($theme.'.checkout.base')
@section('nav_checkout')
    <li><a href="#">1</a></li>
    <li class="active"><a href="#"><strong>2.</strong> Success</a></li>
@endsection
@section('content')
    <div class="container checkout_success">
        <div class="row">
            <div class="col s5 offset-s2 firstCol">
                <h1>Good!</h1>
                <h2>Thank you for purchasing with us.</h2>
                <p>You will be receiving an email with your Coupons for using on our site</p>
                <a href="#" class="btn red showhide">Show Vouchers</a>
                <ul style="display:none">
                    @foreach($vouchers as $v)
                        <li>{{ $v->token }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endsection
@section('custom_js')
    <script>
        $(document).ready(function(){
            $('a.showhide').on('click', function(e){
                e.preventDefault();
                $('ul').slideToggle();
            })
        })
    </script>
@endsection
