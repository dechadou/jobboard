@extends($theme.'.profile.base',['menu'=>'files'])
@section('profile_content')
    <div class="col s9">

        <div class="attached-files">
            <ul>
                @if(!$files->isEmpty())
                    @foreach($files as $file)
                        <li class="card">
                            <div class="file-type icons">
                                <i class="file-icon {{ get_file_extension($file->file) }}"></i>
                            </div>
                            <h1>{{ $file->file_name }}</h1>
                            <div class="actions">
                                <a href="{{ url('uploads/user_files/'.$logged_user->id.'/'.$file->file) }}" target="_blank"><i class="mdi-action-visibility"></i></a>
                                <a href="{{ url('/profile/removeFiles/'.$file->id) }}" class="removeFile"><i class="mdi-navigation-close"></i></a>
                            </div>
                        </li>
                    @endforeach
                @endif
                <li class="card addFile">
                    {!! Form::open(array('class'=>"uploadFiles", 'url' => '/profile/addFiles', 'files'=>true, 'request' => 'form')) !!}
                    {!! Form::file('file', array('required', 'class'=>'form-control hide')) !!}
                    {!! Form::close() !!}
                    <div class="file-type">
                        <i class="mdi-content-add" style="margin-left: 28px;"></i>
                    </div>
                    <h1><a href="#" class="uploadFile">Add File</a></h1>
                </li>
            </ul>
        </div>

    </div>
@endsection