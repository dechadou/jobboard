@extends($theme.'.base',['header' => 'mini'])
@section('content')
    <section class="container profile">
        <div class="row cards">
            <div class="col s3">
                <div class="user-name">
                    <img src="{{ userAvatar($logged_user) }}" class="circle" width="70px" height="70px">
                    <h1>{{ $logged_user->name }}</h1>
                </div>
                <div class="user-menu">
                    <ul>
                        <li data-value="applied"><a href="{{ url('profile/applied') }}">Applied Jobs</a> </li>
                        <li data-value="favourite"><a href="{{ url('profile/favourite') }}">Favourite Jobs</a> </li>
                        <li data-value="preferences"><a href="{{ url('profile/preferences') }}">Alert Preferences</a> </li>
                        <li data-value="files"><a href="{{ url('profile/files') }}">Attached Files</a> </li>
                        <li data-value="info"><a href="{{ url('profile/info') }}">My Info</a> </li>
                    </ul>
                </div>

            </div>
            @yield('profile_content')
        </div>
    </section>
@endsection
@section('custom_js')
    <script>
        $(document).ready(function(){

            $('#dropzone').on('dragover', function(e) {
                event.preventDefault();
                event.stopPropagation();
                $(this).addClass('hover');
            });

            $('#dropzone').on('dragleave', function(e) {
                event.preventDefault();
                event.stopPropagation();
                $(this).removeClass('hover');
            });

            $('#dropzone').on('drop', function(e) {
                event.preventDefault();
                event.stopPropagation();
                app.dropUpload(e,'user');
                $(this).removeClass('hover');
            })

            $('a.uploadFileAvatar').on('click',function(e){
                e.preventDefault();
                $('input[name="logo"]').click();
            })
            $('input[name="logo"]').on('change',function(e){
                app.createLoader('.box');
                var file = this.files[0];

                var fd = new FormData();
                fd.append("file", file);
                $.ajax({
                    url: app.globalPath+'profile/updateAvatar',
                    type: 'POST',
                    dataType:'json',
                    success: function(html){
                        $('.company_logo').attr('src',html['filePath']);
                        app.destroyLoader();
                    },
                    error: function(){
                        app.destroyLoader();
                        $.notify('Upps! Something went wrong. Please try again', 'error');
                    },
                    // Form data
                    data: fd,
                    mimeType:"multipart/form-data",
                    cache: false,
                    contentType: false,
                    processData: false
                });
            })
            $('#autocomplete').autocomplete({
                minChars: 3,
                serviceUrl: app.globalPath + 'countries/list',
                onSelect: function (suggestion) {
                    $('input[name="country_id"]').val(suggestion.data)
                }
            });
            <?php
            if (Session::has('message')) {  $message = Session::get('message'); ?>
            $.notify('<?php echo $message ?>','success');
            <?php } ?>

            $('a.uploadFile').on('click',function(e){
                e.preventDefault();
                e.stopPropagation();

                $('input[name="file"]').click();
            })
            $('input[name="file"]').on('change',function(e){
                e.preventDefault();
                e.stopPropagation();
                $('form.uploadFiles').submit();
            })
            $('a.removeFile').on('click',function(e){
                e.preventDefault();

                if (confirm('Are you sure you want to remove this file?')){
                    window.location.href = $(this).attr('href');
                }
            });
            $('.user-menu ul li[data-value="{{ $menu }}"] a').addClass('active');
        });

        @foreach($errors->all() as $error)
        $.notify("{{ $error }}",'error');
        @endforeach
    </script>
@endsection