<div data-link="{{ getPermaLink($job) }}" class="col xs12 s6 sm6 m4 l4 jobCard manage_job jobCard" data-id="{{ $job->id }}">
    <div class="card medium @if(isRevised($job->id)) visited @endif">
        <div class="top-content">
            <div class="location">
                <i class="mdi-communication-location-on"></i>
                @if($job->locations != null)
                        {{$job->locations->state}}, {{ short_city($job->locations->city) }}
                @else
                    <span class="zipLocation" data-zip="{{$job->zip_code}}"></span>
                @endif
            </div>
            @if($job->featured)
                <div class="featured">
                    <i class="fa fa-star"></i>
                </div>
            @endif
            <div class="clear"></div>
            <div class="divider"></div>
            <h2>{{$job->title}}</h2>
            <h3>{{$job->companies->name}}</h3>
        </div>
        <div class="company-logo"><img src="{{ CompanyLogo($job->companies->user)}}" class="circle" width="82px" height="82px"> </div>
        <div class="bottom-content">
            <p>{!! str_limit(strip_tags($job->the_job),100) !!}</p>

        </div>
        <div class="card-actions hide">
            <ul class="top">
                <li>
                    <i class="fa fa-pencil circle"></i>
                    <a href="{{ url('company/job/'.$job->id.'/edit') }}" data-id="{{$job->id}}" data-action="edit">Edit</a>
                </li>
                <li>
                    <i class="fa fa-files-o circle"></i>
                    <a href="#" data-id="{{$job->id}}" data-action="copy">Copy</a>
                </li>
                <!--<li @if(!compareDatesWithToday($job->expiration_date)) class="active" @endif>
                    <i class="fa fa-pause circle"></i>
                    <a href="#" data-id="{{$job->id}}" data-action="pause">@if(compareDatesWithToday($job->expiration_date)) Pause @else Resume @endif</a>
                </li>-->
                <li @if($job->featured) class="active" @endif>
                    <i class="fa fa-star circle"></i>
                    @if (!$job->featured)
                        <a href="{{ url('jobs/checkout/makeFeatured/'.$job->id) }}" data-action="make_featured">Make Featured</a>
                    @else
                        <a href="#">Featured Job</a>
                    @endif
                </li>
                <li>
                    <i class="fa fa-close circle"></i>
                    <a href="#" data-id="{{$job->id}}" data-action="destroy">Discontinue</a>
                </li>
            </ul>
            <ul class="bottom">
                <li>
                    <i class="fa fa-user circle"></i>
                    <a href="{{ url('company/job/'.$job->id.'/applicants') }}">Applicants</a>
                </li>
                <li>
                    <i class="fa fa-bar-chart circle"></i>
                    <a href="{{ url('company/job/'.$job->id.'/analytics') }}">Analytics</a>
                </li>
            </ul>
        </div>
    </div>
</div>