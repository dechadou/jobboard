<div data-link="{{ getPermaLink($job) }}" class="col xs12 s6 sm6 m4 @if(isset($inProfile)) l4 @else l3 @endif jobCard" data-updated="{{ strtotime($job->updated_at) }}" data-id="{{ $job->id }}">
    <div class="card medium @if(isRevised($job->id)) visited @endif @if(!$job->featured) non-featured @endif">
        <div class="top-content">
            <div class="location">
            <i class="mdi-communication-location-on"></i>
            @if($job->locations != null)
                {{$job->locations->state}}, {{ short_city($job->locations->city) }}
            @else
                <span class="zipLocation" data-zip="{{$job->zip_code}}"></span>
            @endif
        </div>
        @if($job->featured)
            <div class="featured">
                <i class="fa fa-star"></i>
            </div>
        @endif
            <div class="clear"></div>
            <div class="divider"></div>
            <h2>{{ str_limit(ucwords($job->title),60) }}</h2>
        </div>
        <div class="company-logo">
        <img src="{{ CompanyLogo($job->companies->user)}}" class="circle" width="82px" height="82px">
        </div>
        <div class="bottom-content">
            <h3>{{$job->companies->name}}</h3>
            <!--<p>{!! str_limit(strip_tags($job->the_job),100) !!}</p>-->
            <!--<div class="card-actions hide">
                <ul>
                    <li><a href="{{ getPermaLink($job) }}"><i class="mdi-image-remove-red-eye"></i></a></li>
                    @if($logged_user)
                        <li><a href="#" class="@if(isFav($job->id,$logged_user_favs)) selected removeFav @else addToFav @endif" data-id="{{ $job->id }}"><i class="mdi-action-favorite"></i></a></li>
                    @else
                        <li><a href="{{ url('login?return_url='.Request::url().'&action=heart&job_id='.$job->id) }}"><i class="mdi-action-favorite"></i></a></li>
                    @endif
                    <li><a
                            href="#"
                            data-url="{{ getPermaLink($job) }}"
                            data-title="{{$job->title}}"
                            data-descr="{{ str_limit(strip_tags($job->the_job),60) }}"
                            data-image="{{ CompanyLogo($job->companies->user)}}"
                            class="share">
                        <i class="mdi-social-share"></i></a>
                    </li>
                </ul>
            </div>-->
        </div>
    </div>
</div>