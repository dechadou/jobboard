@if(!$similar_jobs->isEmpty())
<div class="row cards similarJobs">
    <h1 class="col s12">Similar Jobs</h1>
    @if(!$similar_jobs->isEmpty())
        @foreach($similar_jobs as $job)
            @include($theme.'.modules.job_card',['job' => $job])
        @endforeach
    @endif
</div>
@endif