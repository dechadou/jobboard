@extends($theme.'.company_profile.base',['menu'=>'saved_jobs'])
@section('profile_content')
    <div class="col s9">
        @include($theme.'.company_profile.searchBar')
        <div class="row jobs">
            @if(!$jobs->isEmpty())
                @foreach($jobs as $job)
                    @if(!$job->featured && !$job->status)
                        @include($theme.'.modules.manage_cards',['job' => $job])
                    @endif
                @endforeach
            @else
                <p>You dont have any Jobs yet. <a href="{{ url('jobs/post') }}">Add one now!</a></p>
            @endif
        </div>

    </div>
@endsection
