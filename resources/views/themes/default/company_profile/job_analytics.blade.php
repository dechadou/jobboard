@extends($theme.'.company_profile.base',['menu'=>'job_analytics'])
@section('profile_content')
    <div class="col s9">
        <div class="row">
            <div class="analytic big">
                <div class="body">
                    <div class="canvas">
                        <h1>Views - Applications</h1>
                        <canvas id="totalChart" height="415" width="782"></canvas>
                    </div>
                    <div class="details">
                        <ul>
                            <li class="totals">
                                <span>Total</span>
                                <strong>{{ $totalsNumber }}</strong>
                                <div class="graph">
                                    <div class="sparkline" data-type="bar" data-width="80%" data-height="25px" data-bar-Width="5" data-bar-Spacing="5" data-bar-Color="#f39c12">
                                        {{ implode(',',$totalsGraph)  }}
                                    </div>
                                </div>
                            </li>
                            <li class="views">
                                <span>Views</span>
                                <strong>{{ $viewsNumber }}</strong>
                                <div class="graph">
                                    <div class="sparkline" data-type="bar" data-width="80%" data-height="25px" data-bar-Width="5" data-bar-Spacing="5" data-bar-Color="#f39c12">
                                        {{ implode(',',$viewsGraph) }}
                                    </div>
                                </div>
                            </li>
                            <li class="apps">
                                <span>Applications</span>
                                <strong>{{ $applicationsNumber }}</strong>
                                <div class="graph">
                                    <div class="sparkline" data-type="bar" data-width="80%" data-height="25px" data-bar-Width="5" data-bar-Spacing="5" data-bar-Color="#f39c12">
                                        {{ implode(',',$applicationsGraph)  }}
                                    </div>

                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row jobs">
            <div class="analytic">
                <div class="header">
                    <h1>Views x Month ({{ $currentMonth }})</h1>
                </div>
                <div class="body">
                    <canvas id="viewsChart" height="415" width="420"></canvas>
                </div>
            </div>
            <div class="analytic">
                <div class="header">
                    <h1>Application x Month ({{ $currentMonth }})</h1>
                </div>
                <div class="body">
                    <canvas id="appChart" height="415" width="420"></canvas>
                </div>
            </div>
        </div>
    </div>
@endsection
