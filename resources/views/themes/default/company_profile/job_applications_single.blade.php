@extends($theme.'.company_profile.base',['menu'=>'job_applications'])
@section('profile_content')
    <div class="col s9">
        <div class="row jobs applications">
            <div>
                <ul class="collapsible" data-collapsible="expandable">
                    <li>
                        <h1 class="collapsible-header active">{{ str_limit($job->title, 70) }}</h1>
                        <span class="new @if(count($job->applied) > 0) have_applications @endif">{{ count($job->applied) }}</span>
                        @if(!$job->applied->isEmpty())
                            <table class="collapsible-body">
                                <thead>
                                <th>Avatar</th>
                                <th>User</th>
                                <th>Email</th>
                                <th>User Comments</th>
                                <th>Files Attached</th>
                                </thead>
                                <tbody>
                                @foreach($job->applied as $applied)
                                    <td><img src="{{ userAvatar($applied) }}" width="100px"></td>
                                    <td>{{ $applied->name }}</td>
                                    <td><a href="mailto:{{ $applied->email }}">{{ $applied->email }}</a></td>
                                    <td>{{ $job->appliedDetails($job->id)[0]->user_comments }}</td>
                                    <td>{!! $job->appliedFiles($job->appliedDetails($job->id)[0]->selected_files) !!}</td>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <p class="collapsible-body">No users had applied for this job yet.</p>
                        @endif
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection
