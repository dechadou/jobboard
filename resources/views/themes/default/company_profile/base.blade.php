@extends($theme.'.base',['header' => 'mini'])
@section('content')
    <section class="company_profile">
        <div class="container">
            <div class="row cards">
                <div class="col s12 l3">
                    <div class="user-name">
                        <img src="{{ companyLogo($logged_user) }}" class="circle" width="70px" height="70px">
                        <h1>{{ $logged_user->name }}</h1>
                    </div>
                    <div class="user-menu">
                        <ul>
                            <li data-value="profile"><a href="{{ url('company') }}">My Profile</a> </li>
                            <li data-value="manage_jobs"><a href="{{ url('company/manage_jobs') }}">Manage Jobs</a> </li>
                            <li data-value="job_applications"><a href="{{ url('company/job_applications') }}">Job Applications</a> </li>
                        <!--<li data-value="featured_jobs"><a href="{{ url('company/featured_jobs') }}">Featured Jobs</a> </li>-->
                            <li data-value="saved_drafts"><a href="{{ url('company/saved_drafts') }}">Saved Drafts</a> </li>
                            <!--<li data-value="available_offers"><a href="{{ url('company/available_offers') }}">Available Offers</a> </li>-->
                            <li data-value="job_analytics"><a href="{{ url('company/job_analytics') }}">Job Analytics</a> </li>
                            <li data-value="billing_info"><a href="{{ url('company/billing') }}">Billing Information</a> </li>
                           <!-- <li data-value="payment_flow"><a href="{{ url('company/payment_flow') }}">Payment Flow</a> </li>-->
                            <!--<li data-value="disscount_codes"><a href="{{ url('company/disccount_codes') }}">Have you a discount code?</a> </li>-->
                        </ul>
                    </div>

                </div>
                @yield('profile_content')
            </div>
        </div>
    </section>
@endsection
@section('custom_js')
    <script>
        $(document).ready(function(){
            $('#dropzone').on('dragover', function(e) {
                event.preventDefault();
                event.stopPropagation();
                $(this).addClass('hover');
            });

            $('#dropzone').on('dragleave', function(e) {
                event.preventDefault();
                event.stopPropagation();
                $(this).removeClass('hover');
            });

            $('#dropzone').on('drop', function(e) {
                event.preventDefault();
                event.stopPropagation();
                app.dropUpload(e,'company');
                $(this).removeClass('hover');
            })

            $('#autocomplete').autocomplete({
                minChars: 3,
                serviceUrl: app.globalPath + 'countries/list',
                onSelect: function (suggestion) {
                    $('input[name="country_id"]').val(suggestion.data)
                }
            });
            <?php
            if (Session::has('message')) {  $message = Session::get('message'); ?>
            $.notify('<?php echo $message ?>','success');
            <?php } ?>

            $('body').on('click', '.card-actions ul.top li a',function(e){
                e.preventDefault();
                var elem    = $(this);
                var action  = elem.data('action');
                var id      = elem.data('id');
                if (action == 'edit'){
                    window.location.href = elem.attr('href');
                }
                if (action == 'make_featured'){
                    window.location.href = elem.attr('href');
                }
                if (action == 'destroy'){
                    if (confirm('Are you sure you want to remove this Job?')){
                        app.cardAction(id,action,function(){
                            elem.parent().parent().parent().parent().fadeOut();
                        })
                    }
                } else {
                    app.cardAction(id,action,function(data){
                        if (action == 'pause'){
                            if (data['status']){
                                elem.text(data['action']);
                                if (data['action'] == 'Resume'){
                                    elem.parent().addClass('active');
                                } else {
                                    elem.parent().removeClass('active');
                                }
                            } else{
                                window.location.href = data['redirect'];
                            }
                        }
                        if (action == 'copy'){
                            $('.row.jobs').append(app.duplicateCard(data['data']));
                        }
                        $.notify(data['message'], 'success');

                    })
                }
            })
            @foreach($errors->all() as $error)
            $.notify("{{ $error }}",'error');
            @endforeach
            $('.user-menu ul li[data-value="{{ $menu }}"] a').addClass('active');

            $('.billing_info .collection li a.delete').on('click',function(e){
                e.preventDefault();
                if (confirm('Are you sure you want to delete this Payment Method?')){
                    window.location.href = $(this).attr('href');
                }
            })

            $('.billing_info .collection li a.edit').on('click',function(e){
                e.preventDefault();
                $(this).parent().next().toggleClass('hide');
            })

            $('.billing_info a.addNew').on('click',function(e){
                e.preventDefault();
                $(this).next().toggleClass('hide');
            })

        });

    </script>
    <script>
        <?php if (isset($analyticsTotal)){ ?>
        views = [];
        apps = [];
        viewsT = [];
        appsT = [];
        <?php
        foreach($analyticsMonth as $data){ ?>
            views.push(<?php echo $data['views']?>);
            apps.push(<?php echo $data['applications']?>);

        <?php } ?>

        <?php
        foreach($analyticsTotal as $data){ ?>
        viewsT.push(<?php echo $data['views']?>);
        appsT.push(<?php echo $data['applications']?>);

        <?php } ?>

        var viewChartData = {
            labels: ["1-10","11-20","21-31"],
            datasets: [
                {
                    fillColor: "rgba(215, 85, 90, 1)",
                    pointColor: "rgba(210, 214, 222, 1)",
                    pointStrokeColor: "#c1c7d1",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: views
                }
            ]
        };

        var applicationsChartData = {
            labels: ["1-10","11-20","21-31"],
            datasets: [
                {
                    fillColor: "rgba(215, 85, 90, 1)",
                    pointColor: "rgba(210, 214, 222, 1)",
                    pointStrokeColor: "#c1c7d1",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: apps
                }
            ]
        };

        var totalData = {
            labels: ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],
            datasets: [
                {
                    fillColor: "rgba(215, 85, 90, 1)",
                    pointColor: "rgba(215, 85, 90, 1)",
                    pointStrokeColor: "#c1c7d1",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(215, 85, 90, 1)",
                    data: appsT
                },
                {
                    fillColor: "rgba(110, 166, 172, 1)",
                    pointColor: "rgba(110, 166, 172, 1)",
                    pointStrokeColor: "#c1c7d1",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(110, 166, 172, 1)",
                    data: viewsT
                }
            ]
        };
        var totalChartCanvas = $("#totalChart").get(0).getContext("2d");
        var totalChar = new Chart(totalChartCanvas);
        var totalChartData = totalData;

        var viewsChartCanvas = $("#viewsChart").get(0).getContext("2d");
        var viewsChar = new Chart(viewsChartCanvas);
        var viewsChartData = viewChartData;

        var appChartCanvas = $("#appChart").get(0).getContext("2d");
        var applicationsChart = new Chart(appChartCanvas);
        var appChartData = applicationsChartData;

        var barChartOptions = {
            //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
            scaleBeginAtZero: true,
            //Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: true,
            //String - Colour of the grid lines
            scaleGridLineColor: "rgba(0,0,0,.05)",
            //Number - Width of the grid lines
            scaleGridLineWidth: 1,
            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,
            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: false,
            //Boolean - If there is a stroke on each bar
            barShowStroke: true,
            //Number - Pixel width of the bar stroke
            barStrokeWidth: 2,
            //Number - Spacing between each of the X value sets
            barValueSpacing: 5,
            //Number - Spacing between data sets within X values
            barDatasetSpacing: 1,
            //String - A legend template
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
            //Boolean - whether to make the chart responsive
            responsive: false,
            maintainAspectRatio: true,
            tooltipYPadding: 12,
            // Number - pixel width of padding around tooltip text
            tooltipXPadding: 12
        };



        barChartOptions.datasetFill = false;
        viewsChar.Bar(viewsChartData, barChartOptions);
        applicationsChart.Bar(appChartData, barChartOptions);
        totalChar.Line(totalChartData, barChartOptions);

        $(".sparkline").each(function () {
            var $this = $(this);
            $this.sparkline('html', $this.data());
        });
        <?php } ?>



    </script>
@endsection