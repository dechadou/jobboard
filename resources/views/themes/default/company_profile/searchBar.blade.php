<div class="row">
    <form class="searchBar">
        <select name="search_city">
            <option value="all">All Cities</option>
            @foreach($g_locations as $location)
                <option value="{{ $location['id'] }}">{{ $location['state'].', '.$location['city'] }}</option>
            @endforeach
        </select>
        <select name="search_job_type">
            <option value="all">All Job Types</option>
            <option value="0">Full Time</option>
            <option value="1">Part Time</option>
            <option value="2">Freelance</option>
        </select>
        <input type="date" value="Date" />
        <a href="#" class=""><i class="mdi-action-search"></i></a>
    </form>
</div>