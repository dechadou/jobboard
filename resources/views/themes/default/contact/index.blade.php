@extends($theme.'.base',['header' => 'mini'])
@section('content')
    <section class="contact">
	    <div class="container">

	        <div class="row center-align header">
	            <div class="col s12">
	                <i class="fa fa-comment rounded-icon"></i>
	                <h1>Contact<br><span style="font-size:14px; color: red; font-weight: bold;">(rod, hay que agregar la clàse "contact" en el body para el fondo)</span></h1>
	                <h2>Please take a look at our FAQ.<br>
					Your question may have already been answered.</h2>
	            </div>
	        </div>

	        <div class="row forms">
	            <div class="col m10 offset-m1 l8 offset-l2">
	                <div class="row">
	                    <div class="col s12 m10 offset-m1 offset-l3 l6">
	                        <div>
	                            <form action="{{ url('/login/auth') }}" method="post">
	                            	<input type="text" placeholder="name" name="name" value="">
	                                <input type="text" placeholder="email" name="email" value="">
	                                <textarea rows="6" cols="50" placeholder="comments"></textarea>
	                                <button type="submit" class="btn blue no-shadow radius5 margin45">Send</button>
	                            </form>
	                        </div>
	                    </div>
	                </div>
	                <div class="row contact-numbers">
	                    <div class="col s5 offset-s1 m3 offset-m3 offset-l4 l2">
	                    	<a href="#">
	                    		<i class="fa fa-mobile rounded-icon"></i>
	                    		<span>917-464-3727</span>
	                    	</a>
	                    </div>
	                    <div class="col s5 m3 l2">
	                    	<a href="#">
	                    		<i class="fa fa-skype rounded-icon"></i>
	                    		<span>hoojobs</span>
	                    	</a>
	                    </div>
	                </div>
	            </div>
	        </div>

	    </div>
	</section>
@endsection
@section('custom_js')
@endsection