    <section class="search-results">
        <div class="container no-margin">

            <div class="row">
                <div class="col s12 m12 l8 search-term">
                <h1>
                    @if(!empty($search_terms['term']))
                        Positions for {{ $search_terms['term'] }} in {{ $search_terms['city'] }}
                    @elseif(!empty($search_terms['city']))
                        All Positions in {{ $search_terms['city'] }}
                    @endif

                    @if(!empty($search_terms['category']))
                        All Positions for Category {{ $search_terms['category'] }}
                        @endif
                    <span class="search-results">{{ $search_totals }}</span>
                </h1>
            </div>
            @if(!$search_results->isEmpty())
                <div class="col s12 m12 l4 search-actions">
                    <a href="#" class="btn-share"><i class="mdi-social-share"></i> Share</a>
                    <a href="#" class="btn-subscribe"><i class="mdi-editor-mode-edit"></i> Subscribe</a>
                </div>
            @endif
        </div>
        <div class="divider max-width"></div>
        <div class="row cards">
            @if(!$search_results->isEmpty())
                @foreach($search_results as $job)
                    @include($theme.'.modules.job_card',['job' => $job])
                @endforeach
            @else
                <div class="col s12">
                    <p>We could not find any job with these parameters. Please try with new ones.</p>
                </div>
            @endif
        </div>
        @if($search_totals > 10)
            <div class="row">
                <div class="col s12 center-align">
                    <a href="#" class="waves-effect waves-light btn brown">Load More</a>
                </div>
            </div>
        @endif
    </div>
</section>
