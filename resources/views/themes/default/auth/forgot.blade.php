@include($theme.'.inc.header')
<section class="forgot full-page">
    <div class="container">

        <div class="row center-align header">
            <div class="col s12">
                <div class="logo"></div>
            </div>
        </div>

        <div class="row forms">            
            <div class="col s12 m10 offset-m1 l8 offset-l2">
                <div class="row">
                    <div class="full-page-col-1 col s12 m10 offset-m1 l12">
                        <div>  
                            <h1>Forgot your Password?</h1>
                            <p>Enter your email address and<br>
                                we'll help you reset your password.</p>
                            <form action="{{ url('login/forgot/send') }}" method="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="text" placeholder="Email" name="email">
                            <a href="#" class="btn blue margin45 no-shadow radius5 center">Continue </a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include($theme.'.inc.scripts')
<script>
    $(document).ready(function(){
        $('.row a').on('click',function(e){
            e.preventDefault();
            $('form').submit();
        })
        @if (session()->has('error'))
            $.notify("{{ session('error') }}", "error");
        @endif
    })
</script>