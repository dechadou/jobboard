@include($theme.'.inc.header')
<section class="register full-page">
    <div class="container">

        <div class="row header">
            <div class="col s12 m10 offset-m1 l8 offset-l2">
                <h1 class="center-align">Create an Account</h1>
            </div>
        </div>
    
        <div class="row forms">            
            <div class="col m10 offset-m1 l8 offset-l2">
                <div class="row">
                    <div class="candidates full-page-col-1 full-page-col-white col s12 m10 offset-m1 l6">
                        <a class="back left-align" href="{{ url('login') }}"><i class="fa fa-chevron-left"></i> </a>
                        <div>
                            <h1> Candidates </h1>
                            <p>At vero eos et accusamus imos
                                us qui ditiis praesentium tatum deleniti atque corrupti.</p>

                            <ul>
                                <li>Quos dolores et quas moles.</li>
                                <li>Non provident, similique.</li>
                                <li>Culpa qui officia deserunt.</li>
                            </ul>

                            <a href="{{ url('login/create#candidate') }}" class="btn blue margin45 no-shadow radius5">
                                Create an account
                            </a>
                        </div>
                    </div>

                    <div class="recruiters full-page-col-2 full-page-col-white col s12 m10 offset-m1 l6">
                        <div>
                            <h1> Recruiters </h1>
                            <p>At vero eos et accusamus imos
                                us qui ditiis praesentium tatum deleniti atque corrupti.</p>

                            <ul>
                                <li>Quos dolores et quas moles.</li>
                                <li>Non provident, similique.</li>
                                <li>Culpa qui officia deserunt.</li>
                            </ul>

                            <a href="{{ url('login/create#recruiter') }}" class="btn red margin45 no-shadow radius5">
                                Create an account
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include($theme.'.inc.scripts')
@if (session()->has('error'))
    <script>
        $(document).ready(function(){
            $('input[name="email"]').notify("{{ session('error') }}", "error");
            })
    </script>
@endif