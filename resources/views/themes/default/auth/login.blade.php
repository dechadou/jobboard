@include($theme.'.inc.header')
<section class="login full-page">
    <div class="container">

        <div class="row center-align header">
            <div class="col s12">
                <div class="logo"></div>
                <h1>Lorem ipsum dolor sit amet consectetur.</h1>
                <h2>Adipiscing elit enean feugiat mollis imperdiet ellentesque ex eget sapien </h2>
            </div>
        </div>

        <div class="row forms">
            <div class="col m10 offset-m1 l8 offset-l2">
                <div class="row">
                    <div class="login-form full-page-col-1 col s12 m10 offset-m1 l6">
                        <div>
                            <h1> Login </h1>
                            <div class="social-netwroks">
                                <a href="{{ url('/login/service/Linkedin') }}" class="btn bglinked no-shadow radius5 margin45"><i class="fa fa-linkedin"></i></a>
                                <a href="{{ url('/login/service/Linkedin') }}" class="btn bggoogle no-shadow radius5 margin45"><i class="fa fa-google-plus"></i></a>
                                <a href="{{ url('/login/service/Linkedin') }}" class="btn bgfb no-shadow radius5 margin45"><i class="fa fa-facebook-f"></i></a>
                            </div>
                            <div class="separador"><hr> or <hr></div>
                            <form action="{{ url('/login/auth') }}" method="post">
                                <input type="hidden" name="_token" value="{{{ csrf_token() }}}">
                                <input type="text" placeholder="email" name="email" value="{{ old('email') }}">
                                <input type="password" placeholder="password" name="password">
                                <input type="hidden" name="return_url" value="{{ $return_url }}">
                                <input type="hidden" name="action" value="{{ $action }}">
                                <input type="hidden" name="job_id" value="{{ $job_id }}">
                                <a class="forgot" href="{{ url('/login/forgot') }}">Forgot your password?</a>
                                <button type="submit" class="btn blue no-shadow radius5 margin45">Log In</button>
                            </form>
                        </div>
                    </div>

                    <div class="register full-page-col-2 full-page-col-white col s12 m10 offset-m1 l6">
                        <div>
                            <h1> Not a Member? </h1>
                            <p>At vero eos et accusamus imos
                                us qui ditiis praesentium tatum deleniti atque corrupti.</p>

                            <ul>
                                <li>Quos dolores et quas moles.</li>
                                <li>Non provident, similique.</li>
                                <li>Culpa qui officia deserunt.</li>
                            </ul>

                            <a href="{{ url('login/register') }}" class="btn red margin45 no-shadow radius5">
                                Create an account
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include($theme.'.inc.scripts')
@if (session()->has('error'))
    <script>
        $(document).ready(function(){
            $('input[name="email"]').notify("{{ session('error') }}", "error");
            })
    </script>
@endif