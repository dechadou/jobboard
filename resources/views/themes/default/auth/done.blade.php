@include($theme.'.inc.header')
<section class="forgot full-page">
    <div class="container">

        <div class="row center-align header">
            <div class="col s12">
                <div class="logo"></div>
            </div>
        </div>

        <div class="row forms">            
            <div class="col s12 m10 offset-m1 l8 offset-l2">
                <div class="row">
                    <div class="full-page-col-1 col s12 m10 offset-m1 l12">
                        <div> 
                            <h1>Done</h1>
                            <p>Please check your email for<br>
                                password reset instructions</p>
                            <a href="{{ url('/') }}" class="btn blue margin45 no-shadow radius5 center">Close </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include($theme.'.inc.scripts')
