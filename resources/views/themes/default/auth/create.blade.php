@include($theme.'.inc.header')
<section class="register create full-page">
    <div class="container">
    
        <div class="row header">
            <div class="col s12 m10 offset-m1 l8 offset-l2">
                <h1>Create an Account</h1>
            </div>
        </div>

        <div class="row forms">            
            <div class="col m10 offset-m1 l8 offset-l2">
                <div class="row">
                    <div class="full-page-col-1 full-page-col-white col s12 m10 offset-m1 l12">
                        <a class="back left-align" href="{{ url('login/register') }}"><i class="fa fa-chevron-left"></i> </a>
                        <ul class="tab">
                            <li class="col s4 active"><a href="#" data-target="#candidates">Candidates</a></li>
                            <li class="col s4"><a href="#" data-target="#recruiters">Recruiters</a></li>
                        </ul>
                        <div>
                            <div id="candidates" class="active create-form">
                                <div class="social-netwroks">
                                    <a href="{{ url('/login/service/Linkedin') }}" class="btn bglinked no-shadow radius5 margin45"><i class="fa fa-linkedin"></i></a>
                                    <a href="{{ url('/login/service/Linkedin') }}" class="btn bggoogle no-shadow radius5 margin45"><i class="fa fa-google-plus"></i></a>
                                    <a href="{{ url('/login/service/Linkedin') }}" class="btn bgfb no-shadow radius5 margin45"><i class="fa fa-facebook-f"></i></a>
                                </div>
                                <div class="separador"><hr> or <hr></div>
                                <form action="{{ url('/login/create/candidate') }}" method="post">
                                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}">
                                    <input type="text" placeholder="Name" name="name">
                                    <input type="text" placeholder="Email" name="email">
                                    <input type="password" placeholder="Password" name="password">
                                    <button type="submit" class="btn blue no-shadow radius5 margin45">Create Account</button>
                                </form>

                            </div>
                            <div id="recruiters" class="create-form">
                                <div class="social-netwroks">
                                    <a href="{{ url('/login/service/Linkedin') }}" class="btn bglinked no-shadow radius5 margin45"><i class="fa fa-linkedin"></i></a>
                                    <a href="{{ url('/login/service/Linkedin') }}" class="btn bggoogle no-shadow radius5 margin45"><i class="fa fa-google-plus"></i></a>
                                    <a href="{{ url('/login/service/Linkedin') }}" class="btn bgfb no-shadow radius5 margin45"><i class="fa fa-facebook-f"></i></a>
                                </div>
                                <div class="separador"><hr> or <hr></div>
                                <form action="{{ url('/login/create/recruiter') }}" method="post">
                                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}">
                                    <input type="text" placeholder="Company name" name="name">
                                    <input type="text" placeholder="Email" name="email">
                                    <input type="password" placeholder="Password" name="password">
                                    <button type="submit" class="btn red no-shadow radius5 margin45">Create Account</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include($theme.'.inc.scripts')

<script>
    $(document).ready(function(){

            @foreach ($errors->all() as $error)
                $.notify("{{ $error }}",'error');
            @endforeach
            if (window.location.hash == '#recruiter'){
                $('ul.tab li a[data-target="#candidates"]').parent().removeClass('active');
                $('ul.tab li a[data-target="#recruiters"]').parent().addClass('active');
                $('#candidates').removeClass('active');
                $('#recruiters').addClass('active');
            }
                $('ul.tab li a').on('click',function(e){
                    e.preventDefault();
                    $('ul.tab li a').parent().removeClass('active');
                    if (!$(this).parent().hasClass('active')){
                        $(this).parent().addClass('active');
                    }
                    var elm = $($(this).data('target'));
                    $('#candidates, #recruiters').removeClass('active');
                    if (!elm.hasClass('active')){
                        elm.addClass('active')
                    }
                })
    })
</script>
