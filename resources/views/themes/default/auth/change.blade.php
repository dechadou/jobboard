@include($theme.'.inc.header')
<section class="forgot full-page">
    <div class="container">

        <div class="row center-align header">
            <div class="col s12">
                <div class="logo"></div>
            </div>
        </div>

        <div class="row forms">            
            <div class="col s12 m10 offset-m1 l8 offset-l2">
                <div class="row">
                    <div class="full-page-col-1 col s12 m10 offset-m1 l12">
                        <div> 
                            <h1>Change your Password</h1>
                            <p>Please write your new password</p>
                            <form method="post" action="{{ url('password/confirm') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="token" value="{{ $token }}">
                                <input type="password" name="password" placeholder="New password">
                                <input type="password" name="password_confirmation" placeholder="Confirm password">
                            </form>
                            <a href="#" class="btn blue margin45 no-shadow radius5 center">Continue </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include($theme.'.inc.scripts')
<script>
    $(document).ready(function(){
        $('.row a').on('click',function(e){
            e.preventDefault();
            $('form').submit();
        })
        @foreach ($errors->all() as $error)
            $.notify("{{ $error }}",'error');
        @endforeach
})
</script>