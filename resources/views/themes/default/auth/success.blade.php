@include($theme.'.inc.header')
<section class="register success full-page">
    <div class="container">

        <div class="row header">
            <div class="col s12 m10 offset-m1 l8 offset-l2">
                <h1>Bienvenido {{ $user->name }}!</h1>
            </div>
        </div>
        
        <div class="row forms">            
            <div class="col m10 offset-m1 l8 offset-l2">
                <div class="row">
                    <div class="full-page-col-1 full-page-col-white col s12 m10 offset-m1 l12">
                        <div>               
                            <img src="{{ userAvatar($logged_user) }}" class="circle">
                            <div>
                                <p>Remember complete your Profile and upload your CV </p>
                                @if ($is_company)
                                    <a href="{{ url('/company/') }}" class="btn blue margin45 no-shadow radius5">Complete my profile <i class="fa fa-chevron-right"></i> </a>
                                @else
                                    <a href="{{ url('/profile/') }}" class="btn blue margin45 no-shadow radius5">Complete my profile <i class="fa fa-chevron-right"></i> </a>
                                @endif
                                <a href="{{ url('/') }}" class="btn blue margin45 no-shadow radius5">Search Jobs <i class="fa fa-chevron-right"></i> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include($theme.'.inc.scripts')

<script>
    $(document).ready(function(){

            @foreach ($errors->all() as $error)
                $.notify("{{ $error }}",'error');
            @endforeach
    })
</script>
