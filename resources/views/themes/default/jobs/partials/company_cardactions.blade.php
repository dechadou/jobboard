<div class="col s3">
    <div class="card-actions job-view">
        <ul class="top">
            <li>
                <i class="fa fa-pencil circle"></i>
                <a href="{{ url('company/job/'.$job->id.'/edit') }}" data-id="{{$job->id}}" data-action="edit">Edit</a>
            </li>
            <li>
                <i class="fa fa-files-o circle"></i>
                <a href="#" data-id="{{$job->id}}" data-action="copy">Copy</a>
            </li>
            <li @if($job->featured) class="active" @endif>
                <i class="fa fa-star circle"></i>
                <a href="{{ url('jobs/checkout/makeFeatured/'.$job->id) }}" data-action="make_featured">Make Featured</a>
            </li>
            <li>
                <i class="fa fa-close circle"></i>
                <a href="#" data-id="{{$job->id}}" data-action="destroy">Discontinue</a>
            </li>
        </ul>
        <ul class="bottom">
            <li>
                <i class="fa fa-user circle"></i>
                <a href="{{ url('company/job/'.$job->id.'/applicants') }}">Applicants</a>
            </li>
            <li>
                <i class="fa fa-bar-chart circle"></i>
                <a href="{{ url('company/job/'.$job->id.'/analytics') }}">Analytics</a>
            </li>
        </ul>
    </div>
</div>