<div class="col l3 s12">

    <div class="module-actions">
        <a href="{{ url('login?return_url=?return_url='."//$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]".' ') }}" class="btn-favourite" data-id="{{ $job->id }}">
            <i class="mdi-action-favorite"></i> Favourite
        </a>
        <a href="#" class="btn-share share"
           data-url="{{ getPermaLink($job) }}"
           data-title="{{$job->title}}"
           data-descr="{{ str_limit($job->the_job,60) }}"
           data-image="{{ CompanyLogo($job->companies->user)}}">
            <i class="mdi-social-share"></i> Share
        </a>
    </div>
    <div class="clear"></div>
    <div class="card login-form">
        <h1>Login</h1>
        <form action="{{ url('/login/auth') }}" method="post">
            <input type="hidden" name="return_url" value="{{ "//$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" }}">
            <input type="hidden" name="_token" value="{{{ csrf_token() }}}">
            <input type="text" placeholder="email" name="email" value="{{ old('email') }}">
            <input type="password" placeholder="password" name="password">
            <div class="buttons">
            <a href="{{ url('/login/service/Linkedin?return_url='."//$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]".' ') }}" class="btn darkblue no-shadow radius5 linkedin">
                <i class="fa fa-linkedin"></i>
            </a>
            <button type="submit" class="btn blue no-shadow radius5 margin45">Log In</button>
            </div>
        </form>
    </div>

    <div class="card apply_unlogged">
        <h1>Send your resume</h1>
        {!! Form::open(array('url' => 'jobs/view/'.$job->id.'/apply', 'files'=>true, 'request' => 'form')) !!}
            <input type="hidden" name="return_url" value="{{ "//$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" }}">
            <input type="hidden" name="_token" value="{{{ csrf_token() }}}">
            <input type="text" placeholder="Your Name" name="unlogged_name" value="{{ old('unlogged_name') }}">
            <input type="text" placeholder="Your Email" name="unlogged_email" value="{{ old('unlogged_email') }}">
            <textarea placeholder="Tell us about you" name="unlogged_tell_us">{{ old('unlogged_tell_us') }}</textarea>
            <div class="file-field input-field">
                <div class="btn">
                    <span>File</span>
                    <input type="file" name="unlogged_file">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                </div>
            </div>
            <button type="submit" class="btn no-shadow radius5 red apply_unlogged">Apply for this job</button>
        {!! Form::close() !!}
    </div>



    <!--<div class="card">
        <div class="register">
            <h1> Not a Member? </h1>
            <p>At vero eos et accusamus imos
                us qui ditiis praesentium tatum deleniti atque corrupti.</p>

            <ul>
                <li>Quos dolores et quas moles.</li>
                <li>Non provident, similique.</li>
                <li>Culpa qui officia deserunt.</li>
            </ul>

            <a href="{{ url('login/create#candidate') }}" class="btn red width232 margin45 no-shadow radius5">
                Create an account
            </a>
        </div>
    </div>-->
</div>