<div class="col s12 l4">
    <div class="card module login-form">
        <h1>Login to Apply</h1>
        <div class="social-netwroks">
            <a href="{{ url('/login/service/Linkedin') }}" class="btn bglinked no-shadow radius5"><i class="fa fa-linkedin"></i></a>
            <a href="#" class="btn bggoogle no-shadow radius5 margin45"><i class="fa fa-google-plus"></i></a>
            <a href="#" class="btn bgfb no-shadow radius5 margin45"><i class="fa fa-facebook-f"></i></a>
        </div>

        <div class="separador">or</div>
        <form action="{{ url('/login/auth') }}" method="post">
            <input type="hidden" name="return_url" value="{{ "//$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" }}">
            <input type="hidden" name="_token" value="{{{ csrf_token() }}}">
            <input type="text" placeholder="email" name="email" value="{{ old('email') }}">
            <input type="password" placeholder="password" name="password">
            <button type="submit" class="btn blue no-shadow radius5">Log In</button>
        </form>

    </div>
    <div class="card module register">
        <div>
            <h1> Not a Member? </h1>
            <p>At vero eos et accusamus imos
                us qui ditiis praesentium tatum deleniti atque corrupti.</p>

            <ul>
                <li>Quos dolores et quas moles.</li>
                <li>Non provident, similique.</li>
                <li>Culpa qui officia deserunt.</li>
            </ul>

            <a href="{{ url('login/create#candidate') }}" class="btn red no-shadow radius5">
                Create an account
            </a>
        </div>
    </div>
</div>