@extends($theme.'.base',['header' => 'mini'])
@section('content')
    <section class="container">
        {!! Form::open(array('url' => 'company/job/'.$job->id.'/store', 'request' => 'form','class'=>'postAJob')) !!}
        <div class="row">
            <div class="col s12 l8 offset-l2">
                {!! Form::text('job_name', $job->title, array('class'=>'big-input countChars', 'placeholder'=>'Job Name','onkeyup'=>'app.countChars(this,74)','maxlength'=>'74', 'data-check'=>'text')) !!}
                <span class="charCounter">73</span>
                <span class="error" data-name="job_name" style="display:none">Please fill the Job Name</span>
            </div>
        </div>
        <div class="row">
            <div class="col s12 l8 offset-l2">
                {!! Form::text('company_name', $logged_user->name, array('class'=>'big-input', 'placeholder'=>'Company Name','maxlength'=>'58', 'data-check'=>'text')) !!}
                <span class="error" data-name="company_name" style="display:none">Please fill the Company Name</span>
            </div>
        </div>
        <div class="row">
            <div class="col s12 l8 offset-l2 box job-location">
                <div class="valign-wrapper">
                    <h1 class="valing">Job Location</h1>
                </div>

                <div class="valign-wrapper">
                    {!! Form::text('zip_code', $job->zip_code, array('class'=>'col s2 valign normal-input', 'placeholder'=>'Zip Code', 'data-check'=>'zip')) !!}
                    <span class="col s12 m1 valign padding3rm type20 light">or</span>
                    {!! Form::text('location', $job->location, array('id'=> 'autocomplete', 'class'=>'col s12 m8 valign normal-input arrows', 'placeholder'=>'Job Location','data-check'=>'text')) !!}
                    <input type="hidden" name="location_id" value="{{ $job->location_id }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col s12 l8 offset-l2 box">
                <h1>The Job</h1>
                {!! Form::textarea('the_job', $job->the_job, array('id'=> 'the_job', 'placeholder'=>'Here you can tell the applicants what will be their main duties and<br>responsabilities. You can add details as who their supervisor will be and in which<br>area they will be working.')) !!}
                <span class="error" data-name="the_job" style="display:none">Please fill the Job Description</span>
            </div>
        </div>
        <div class="row">
            <div class="col s12 l8 offset-l2 box">
                <h1>The Candidate <span class="small">(optional)</span></h1>
                {!! Form::textarea('the_candidate', $job->the_candidate, array('id'=> 'the_candidate', 'placeholder'=>' We recommend using bullet points<br>• What are the required skills for this position<br>• Required previous experience<br>• Educational background')) !!}
                <span class="error" data-name="the_candidate" style="display:none">Please fill the Candidate Description</span>
            </div>
        </div>

        <div class="row">
            <div class="col s12 l8 offset-l2 box">
                <h1>The Company <span class="small">(optional)</span></h1>
                {!! Form::textarea('the_company', $logged_user->company->description, array('id'=> 'the_company', 'placeholder'=>'Talk about your company. What kind of industry is it, what are it’s main products and services and all of the information that the candidates might found interesting.')) !!}
                <div class="divider"></div>
                <h1>Company Logo <span class="small">(optional)</span></h1>
                <div class="fileInput">
                    <img src="{{ companyLogo($logged_user) }}" class="circle" width="200px" height="200px">
                    <p>Drag and drop from your desktop or
                        Upload it in format PNG, JPEG or TIFF</p>
                    <a href="#" class="btn blue radius5 btnSpecialHeight padding3rm no-shadow">Browser</a>
                    <input type="file" name="logo" class="hide">
                </div>
            </div>
        </div>

        <div class="row typeOfJob">
            <div class="col s12 l8 offset-l2 box">
                <h1>Type of Job</h1>
                @foreach($jobTypes as $type)
                    <a href="#" data-value="{{ $type->id }}" class="btn no-shadow @if($job->job_type == $type->id) active @endif">{{ $type->name }}</a>
                @endforeach
                <input type="hidden" name="job_type" value="{{ $job->job_type }}">
                <span class="error" data-name="job_type" style="display:none">Please select a job type</span>
            </div>
        </div>

        <div class="row">
            <div class="col s12 l8 offset-l2 box">
                <h1>Industry</h1>

                <select name="industry_id" class="browser-default">
                    @foreach($industries as $industry)
                        <option value="{{ $industry->id }}" @if($job->industry_id == $industry->id) selected @endif>{{ $industry->name }}</option>
                    @endforeach
                    <option value="other" @if($job->industry_other != '') selected @endif>Other</option>
                </select>
                <span class="error" data-name="industry" style="display:none">Please Select a Industry</span>
                <input type="text" name="industry_other" placeholder="Please input your Industry" @if($job->industry_other != '')value="{{ $job->industry_other }}"@else style="display:none"@endif>
                <span class="error" data-name="industry_other" style="display:none">Please fill the industry name</span>

            </div>
        </div>

        <div class="row categories">
            <div class="col s12 l8 offset-l2 box">
                <h1>Categories</h1>
                <h2>Choose up to three. These will help candidates quickly find your job</h2>
                @foreach($g_categories as $cat)
                    <a href="#" data-value="{{ $cat->id }}" class="btn no-shadow @if(in_array($cat->id,$cId)) active @endif  ">{{ $cat->name }}</a>
                @endforeach
                <input type="hidden" name="categories[]" value="{{ implode(',',$cId) }}">
                <span class="error" data-name="categories" style="display:none">Please select at least 3 categories</span>
            </div>
        </div>

        <div class="row">
            <div class="col s12 l8 offset-l2 box app_by">
                <h1>Applications by</h1>
                <div class="valign-wrapper">
                    <a href="#" data-value="0" class="valign btn no-shadow @if($job->app_by == 0) active @endif">email</a>
                    <a href="#" data-value="1" class="valign btn no-shadow @if($job->app_by == 1) active @endif">url</a>
                    <input type="hidden" name="app_by" value="{{ $job->app_by }}">
                    <input type="text" class="valign" name="app_by_text" value="{{ $job->app_by_text }}">

                </div>
            </div>
        </div>

        <div class="row post-job-actions">
            <div class="center-align margin-top-bottom-50">
                <a href="#" data-value="0" class="btn margin45 valign draft no-shadow radius5 draft">Save as draft</a>
                <a href="#" data-value="1" class="btn red margin45 valign no-shadow radius5 publish">Publish</a>
                <input type="hidden" name="job_status">
            </div>
            <span class="error" data-name="app_by_text" style="display:none">Please fill the Email or URL</span>
        </div>
        {!! Form::close() !!}
    </section>
@endsection
@section('custom_js')
    <script>
        $(document).ready(function(){

            @foreach($errors->all() as $error)
               $.notify('{{ $error }}','error');
            @endforeach



            $('#autocomplete').autocomplete({
                        minChars: 2,
                        serviceUrl: app.globalPath + 'locations/list',
                        onSelect: function (suggestion) {
                            $('input[name="location_id"]').val(suggestion.data)
                        }
                    });


            $('input').on('keyup focusout',function(e){
                var check = $(this).data('check');
                var value = $(this).val();
                var name  = $(this).attr('name');
                $(this).removeClass('error');


                if ($(this).attr('name') == 'new_user_email'){
                    $('.email_taken').hide();
                    $.ajax({
                        type:'POST',
                        url:app.globalPath+'login/checkEmail',
                        data:{email:$(this).val()},
                        success:function(html){
                            if(html == 'true'){
                                $('.email_taken').show();
                            }
                        }

                    })
                }

                if ('text' == check){
                    if ($.trim(value) == ''){
                        $(this).addClass('error');
                        $('.error[data-name="'+name+'"]').show();
                    } else {
                        $(this).addClass('success');
                        $('.error[data-name="'+name+'"]').hide();
                    }
                }

                /*if ('zip' == check){
                 if ($.trim(value) == '' || !$.isNumeric(value) || $.trim(value).length < 5){
                 $(this).addClass('error');
                 $('.error[data-name="'+name+'"]').show();
                 } else {
                 $(this).addClass('success');
                 $('.error[data-name="'+name+'"]').hide();
                 }
                 }*/

                if ('number' == check){
                    if ($.trim(value) == '' || !$.isNumeric(value) || $.trim(value).length < 10){
                        $(this).addClass('error');
                        $('.error[data-name="'+name+'"]').show();
                    } else {
                        $(this).addClass('success');
                        $('.error[data-name="'+name+'"]').hide();
                    }
                }
                if ('email' == check){
                    if ($.trim(value) == '' || !app.isEmail(value)){
                        $(this).addClass('error');
                        $('.error[data-name="'+name+'"]').show();
                    } else {
                        $(this).addClass('success');
                        $('.error[data-name="'+name+'"]').hide();
                    }
                }
            })


            var categories = [{{ implode(',',$cId) }}];
            $('.categories a').on('click',function(e){
                e.preventDefault();
                var value = $(this).data('value');
                if (categories.indexOf(value) == -1){
                    if (categories.length == 3){
                        $.notify('You can only select 3 categories','warning');
                        $(this).removeClass("active");
                        return false;
                    }
                    categories.push(value);

                } else {
                    index = categories.indexOf(value);
                    categories.splice(index, 1);

                }
                ($(this).hasClass('active')) ? $(this).removeClass('active') : $(this).addClass('active');
                $('input[name="categories[]"]').val(categories);
            })
            $('.typeOfJob a').on('click',function(e){
                e.preventDefault();
                $('.typeOfJob a').removeClass('active');
                $(this).addClass('active');
                $('input[name="job_type"]').val($(this).data('value'));
            })
            $('.app_by a').on('click',function(e){
                e.preventDefault();
                $('.app_by a').removeClass('active');
                $(this).addClass('active');
                $('input[name="app_by"]').val($(this).data('value'));
            })

            $('.publish, .draft').on('click',function(e){
                e.preventDefault();
                $('input[name="job_status"]').val($(this).data('value'));
                $('form').submit();
            })

            app.initTinyMce('the_job');
            app.initTinyMce('the_candidate');

        })
    </script>

@endsection
