@extends($theme.'.profile.base',['menu'=>'info'])
@section('profile_content')
    <div class="col s9">
        <div class="row">

            {!! Form::open(array('url' => 'profile/updateProfile/', 'request' => 'form','class'=>'postAJob')) !!}
            <input type="text" placeholder="Company Name" class="big-input" name="company_name" value="{{ $logged_user->name }}">
            <div class="box">
                <h1>Avatar <span class="small">(optional)</span></h1>
                <div class="fileInput">
                    <div id="dropzone" class="circle"></div>
                    <img src="{{ userAvatar($logged_user) }}" class="circle company_logo" width="200px" height="200px">
                    <p>Drag and drop from your desktop or
                        Upload it in format PNG, JPEG or TIFF</p>
                    <a href="#" class="btn blue radius5 btnSpecialHeight padding3rm no-shadow uploadFileAvatar">Browser</a>
                    <input type="file" name="logo" class="hide">
                </div>
            </div>
            <input type="text" placeholder="Address" class="big-input half" name="address" value="{{ $logged_user->address }}">
            <input type="text" placeholder="Country" class="big-input half" id="autocomplete" value="{{ $logged_user->country->name }}">

            <input type="text" placeholder="Zip/Postal Code" class="big-input half" name="zip_code" value="{{ $logged_user->zip_code }}">
            <input type="text" placeholder="State / Province" class="big-input half" name="state" value="{{ $logged_user->state }}">
            <input type="tel" pattern="^(?:\(\d{3}\)|\d{3})[- . ]?\d{3}[- . ]?\d{4}$" placeholder="Bussiness Phone" class="big-input half" name="bussiness_phone" value="{{ $logged_user->bussiness_phone }}">
            <input type="tel" pattern="^(?:\(\d{3}\)|\d{3})[- . ]?\d{3}[- . ]?\d{4}$" placeholder="Mobile Phone" class="big-input half" name="mobile_phone" value="{{ $logged_user->mobile_phone }}">
            <input type="text" placeholder="Email" class="big-input half" name="email" value="{{ $logged_user->email }}" disabled>
            <input type="text" placeholder="http" class="big-input half" name="website" value="{{ $logged_user->website }}">
            <input type="hidden" name="country_id" value="{{ $logged_user->country_id }}">
        </div>
        <div class="row">
            <div class="right-align margin-top-bottom-50">
                {!! Form::submit('Save', array('class'=>'btn red width232 margin45 valign draft no-shadow radius5')) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection