@extends($theme.'.profile.base',['menu'=>'applied'])
@section('profile_content')
    <div class="col s9">
        <div class="row jobs">
        @if(!$jobs->isEmpty())
            @foreach($jobs as $job)
                @include($theme.'.modules.job_card',['job' => $job, 'inProfile' => true])
            @endforeach
        @else
            <p>You dont have any applied jobs yet.</p>
        @endif
            </div>
    </div>
@endsection