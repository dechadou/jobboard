@extends($theme.'.profile.base',['menu'=>'preferences'])
@section('profile_content')
    <div class="col s8 offset-s1">
        <div class="row">
            <h1>What kind of jobs are you looking for?</h1>
            <p>Tell us what types of jobs you're interested in and we'll alert you periodically when relevant jobs are posted.</p>
        </div>
        <div class="row">
            {!! Form::open(array('url' => 'profile/updateAlerts/', 'request' => 'form', 'class'=>'preferencesForm')) !!}
            {!! Form::label('job_type', 'Job Type', array('class' => 'head')) !!}


            @foreach($job_types as $jb)
                <p>
                    <input name="job_type[]" type="checkbox" class="filled-in" id="{{ $jb->name }}" value="{{ $jb->id }}" @if(in_array($jb->id, $userData['job_types'])) checked @endif />
                    <label for="{{ $jb->name }}">{{ $jb->name }}</label>
                </p>
            @endforeach

            {!! Form::label('category', 'Category', array('class' => 'head')) !!}
            @foreach($categories as $c)
                <p>
                    <input name="categories[]" type="checkbox" class="filled-in" id="{{ $c->name }}" value="{{ $c->id }}" @if(in_array($c->id, $userData['categories'])) checked @endif  />
                    <label for="{{ $c->name }}">{{ $c->name }}</label>
                </p>
            @endforeach
            <div class="row">
                <div class="right-align margin-top-bottom-50">
                    {!! Form::submit('Save Preferences', array('class'=>'btn red no-shadow radius5')) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
