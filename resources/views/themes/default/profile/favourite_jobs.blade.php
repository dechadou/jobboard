@extends($theme.'.profile.base',['menu'=>'favourite'])
@section('profile_content')
    <div class="col s9">
        @if(!$jobs->isEmpty())
            @foreach($jobs as $job)
                @include($theme.'.modules.job_card',['job' => $job, 'inProfile' => true])
            @endforeach
        @else
            You dont have any applied jobs yet.
        @endif
    </div>
@endsection