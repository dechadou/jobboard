@extends($theme.'.base',['header' => 'mini'])
@section('content')
    <div class="container pages">
    <div class="row">
        <div class="col s12">
            <h1><strong>{{ $page->title }}</strong></h1>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col s12">
            {!! $page->content !!}
        </div>
    </div>
    </div>
@endsection
@section('custom_js')
@endsection