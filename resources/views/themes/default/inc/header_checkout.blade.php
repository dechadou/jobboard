<header class="{{ $header or '' }}">
    <section class="top-menu">
        <nav class="header-nav">
            <div class="nav-wrapper">
                <a href="{{ url('/') }}" class="brand-logo"><img src="{{ asset('assets/frontend/images/logo.svg') }}"></a>
                <ul id="nav-mobile" class="right checkout_steps">
                    @yield('nav_checkout')
                </ul>
            </div>
        </nav>
    </section>
</header>