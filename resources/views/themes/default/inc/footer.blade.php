<footer>
    <div class="container">
        <div class="row">
            <div class="col m3 l2">
                <a href="#"><img src="{{ asset('assets/frontend/images/logo_black.svg') }}" width="96px"></a>
            </div>
            <div class="col m6 l8">
                @if(!$is_company)<a href="{{ url('profile/preferences') }}">Sign up for job alerts</a>@endif
                <a href="{{ url('search/categories/all') }}">Categories</a>
                <a href="{{ url('search/cities/all') }}">Cities</a>
                <a href="{{ url('jobs/post') }}">Post a Job</a>
                <a href="{{ url('pages/faq') }}">FAQ</a>
                <a href="{{ url('pages/privacy-statement') }}">Privacy Policy</a>
                <a href="{{ url('pages/about-us') }}">About us</a>
                <a href="{{ url('contact') }}">Contact Us</a>
            </div>
            <div class="col m3 l2 right-align">
                <img class="cards" src="{{ asset('assets/frontend/images/cards.png') }}">
            </div>
        </div>
    </div>
</footer>