<div id="main-nav">
    <ul>
        @if($logged_user)
            <li>
                <a href="{{ url('/profile') }}" class="user-avatar logged_user">
                    <img src="@if($is_company) {{ companyLogo($logged_user) }} @else {{ userAvatar($logged_user) }}@endif" class="circle">

                    <hgroup>
                        <h1>{{ $logged_user->name }}</h1>
                        <h2>{{ $logged_user->email }}</h2>
                    </hgroup>
                </a>
                <a href="{{ url('logout') }}" class="btn_logout"><i class="fa fa-sign-out"></i></a>
            </li>
            <div class="clear"></div>
        @endif
        <li><a><i class="mdi-maps-local-offer"></i> Categories</a>
            <ul>
                @foreach($g_categories as $category)
                    <li><a href="{{ url('search/categories/'.$category->id) }}">{{ $category->name }}</a></li>
                @endforeach
            </ul>
        </li>
        <li><a><i class="mdi-communication-location-on"></i> Popular Cities</a>
            <ul>
                @foreach($g_locations as $location)
                    <li><a href="{{ url('search/cities/'.$location['id']) }}">{{ $location['state'].', '.short_city($location['city'])}}</a></li>
                @endforeach
                <li><a href="{{ url('search/cities/all') }}">VIEW ALL</a></li>
            </ul>
        </li>
        @if(!$is_company)<li><a href="{{ url('profile/preferences') }}"><i class="mdi-editor-mode-edit"></i> Sign Up for job alerts</a></li>@endif
        <li><a href="{{ url('contact') }}"><i class="fa fa-comment"></i> Contact</a></li>
        <li><a href="{{ url('pages/faq') }}"><i class="fa fa-question-circle"></i> FAQ</a></li>
        <li><a href="{{ url('pages/popular_links') }}"><i class="mdi-content-link"></i> Popular Links</a></li>
        <li><a href="{{ $facebook }}"><i class="fa fa-facebook"></i> Facebook</a></li>
        <li><a href="{{ $twitter }}"><i class="fa fa-twitter"></i> Twitter</a></li>
    </ul>
</div>