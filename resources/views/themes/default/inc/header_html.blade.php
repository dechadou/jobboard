<header class="mm-page mm-slideout {{ $header or '' }} " style="background: url({{ ($home_background != '') ? $home_background : asset('assets/frontend/images/top-image.jpg') }}) no-repeat center center;background-size:cover">
    <section class="top-menu">
        <div class="container">
            <nav class="header-nav">
                <div class="nav-wrapper">
                    <a href="{{ url('/') }}" class="brand-logo"><img src="{{ ($site_logo != '') ? $site_logo : asset('assets/frontend/images/logo.svg') }}"></a>
                    <ul id="nav-mobile" class="right">
                        <li><a href="{{ url('jobs/plan') }}" class="waves-effect waves-light btn red circle nav-mini-buttons @if($logged_user && !$is_company) hide @endif" style="margin-right:10px;">Hire</a></li>
                        <li><a href="{{ url('login') }}" class="waves-effect waves-light btn transparent nav-mini-buttons @if($logged_user) hide @endif">Seek</a></li>
                        <li><a href="#main-nav" class="head-nav"><i class="mdi-navigation-menu"></i></a></li>
                    </ul>
                </div>
                 @if($logged_user)
                    <div class="user-nav @if($is_company) company @endif">
                        <ul>
                            <li><a href="{{ url('/profile') }}"><i class="fa fa-gear"></i> @if($is_company) {{ $logged_user->company->name }} @else {{ $logged_user->name }} @endif</a></li>
                            <li class="show-phone"><a href="{{ url('jobs/post') }}">Post a Job</a></li>
                            <li><a href="{{ url('/logout') }}"><i class="fa fa-arrow-circle-left"></i> Logout</a></li>
                        </ul>
                    </div>
                @endif
            </nav>
        </div>
    </section>

    <div class="container">
        <div class="row">
            <div class="col s12">
                <h1>{{ ($home_text != '') ? $home_text : 'Here is just some random text we made for you!' }}</h1>
            <ul class="header_buttons">
                <li><a href="{{ url('jobs/plan') }}" class="waves-effect waves-light btn red @if($logged_user && !$is_company) hide @endif">I want to hire</a></li>
                <li><a href="{{ url('login') }}" class="waves-effect waves-light btn @if($logged_user) hide @endif">I want to find work</a></li>
            </ul>
        </div>
    </div>

    @include($theme.'.inc.search')

</header>