@include($theme.'.inc.header')
<div>
    @include($theme.'.inc.header_checkout',['header' => 'mini'])
    @yield('content')
    @include($theme.'.inc.footer')
    @include($theme.'.inc.menu')
</div>
@include($theme.'.inc.scripts')
@yield('custom_js')
</body>
</html>
