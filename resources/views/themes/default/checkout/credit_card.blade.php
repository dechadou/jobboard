<div class="row">
    <div class="col s8 offset-s2">
        <div class="card">
            <div class="left">
                <h1>Payment details</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
            </div>
            <div class="clear"></div>
            @if(!empty($billing['paymentProfiles']))
                <select name="paymentProfiles" class="browser-default">
                    @foreach($billing['paymentProfiles'] as $profile)
                        <option value="{{ $profile['customerPaymentProfileId'] }}">Pay with Visa number {{ $profile['payment']['creditCard']['cardNumber'] }}</option>
                    @endforeach
                </select>
                <input type="checkbox" name="add_creditcard" id="use_another_creditcard" value="1">
                <label for="use_another_creditcard">Use another credit card</label>
            @endif

            @if(empty($billing['paymentProfiles']))
                <input type="hidden" name="add_creditcard" id="addCreditCard" value="1">
            @endif

            <div class="new_card" @if(!empty($billing['paymentProfiles'])) style="display: none;" @endif>
                <div class="row">
                    <div class="input-field col s6">
                        <input placeholder="First Name" name="firstName" id="first_name" type="text">
                    </div>
                    <div class="input-field col s6">
                        <input id="last_name" placeholder="Last Name" name="lastName" type="text">
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="Address" id="address" name="address" type="text">
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="City" id="city" name="city" type="text">
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <input placeholder="State" id="state" name="state" type="text">
                    </div>
                    <div class="input-field col s6">
                        <input placeholder="Zip Code" id="zip" type="text" name="zip" >
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="Country" id="country" name="country" type="text" class="validate">
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <input placeholder="Phone Number (+1 801 111 1111)" id="phoneNumber" name="phoneNumber" type="text">
                    </div>
                    <div class="input-field col s6">
                        <input placeholder="Fax Number (+1 801 111 1111)" id="faxNumber" name="faxNumber" type="text">
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input placeholder="Credit Card Number (Without Spaces or Hypens)" id="creditcard" name="cardNumber" type="text">
                    </div>
                </div>
                <div class="row">
                    <div class="col s3">
                        <label>Expiration Month</label>
                        <select class="browser-default" name="expirationMonth">
                            <option value="01">01</option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                            <option value="05">05</option>
                            <option value="06">06</option>
                            <option value="07">07</option>
                            <option value="08">08</option>
                            <option value="09">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                        </select>
                    </div>
                    <div class="col s3" style="margin-bottom:10px">
                        <label>Expiration Year</label>
                        <select class="browser-default" name="expirationYear">
                            <option value="16">2016</option>
                            <option value="17">2017</option>
                            <option value="18">2018</option>
                            <option value="19">2019</option>
                            <option value="20">2020</option>
                            <option value="21">2021</option>
                            <option value="22">2022</option>
                            <option value="23">2023</option>
                            <option value="24">2024</option>
                            <option value="25">2025</option>
                            <option value="26">2026</option>
                            <option value="27">2027</option>
                            <option value="28">2028</option>
                        </select>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input placeholder="cardCode found on the back of the card" name="cardCode" id="cardcode" type="text">
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>