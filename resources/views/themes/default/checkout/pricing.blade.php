@extends($theme.'.checkout.base')
@section('nav_checkout')
@section('nav_checkout')
    <li class="active"><a href="#"><strong>1.</strong> <span>Pricing</span></a></li>
    <li><a href="#">2</a></li>
@endsection
@endsection
@section('content')
    <div class="container checkout">
        <div class="row">
            <div class="col s8 offset-s2">
                <h1>Checkout</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
            </div>
        </div>
        {!! Form::open(array('url' => 'jobs/checkout/step2', 'request' => 'form')) !!}
        <div class="row">
            <div class="col s8 offset-s2">
                <div class="card">
                    <div class="left">
                        <h1>Post this job for</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                    </div>
                    <div class="right"><span class="price">${{ buildItemPrice($user->company->available_posts) }}</span></div>
                    <div class="clear"></div>
                    <hr>
                    <div class="right price_featured" style="display: none;">$89</div>
                    <p class="description">
                        <span class="red">Featured position:</span> appear on top of  HooJob’s home page,<br>
                        All category pages, Search results.<br>
                        <strong>Attract up to 70% more applications. <a href="#" class="helper"><i class="fa fa-question-circle"></i></a></strong>
                    </p>
                    <div class="featured_btns">
                        <a href="#" class="btn no-shadow" data-price="{{getFeaturedPrices(2)}}">1 Week ${{getFeaturedPrices(2)}}</a>
                        <a href="#" class="btn no-shadow" data-price="{{getFeaturedPrices(3)}}">2 Weeks ${{getFeaturedPrices(3)}}</a>
                        <a href="#" class="btn no-shadow" data-price="{{getFeaturedPrices(4)}}">30 Days - ${{getFeaturedPrices(4)}}</a>
                    </div>

                    <hr>
                    <div class="disccount_input">
                        <div class="input-field col s9">
                            <input placeholder="Enter your discount or Bulk code here" name="coupon" type="text">
                        </div>
                        <a href="#" class="btn no-shadow redeem unactive right">Redeem</a>
                    </div>
                    <p class="disccount_added" style="display: none">
                        Disccount code number: <span class="disccount_code"></span>
                        <span class="right disccount_value">-$110</span>
                    </p>

                    <div class="clear"></div>
                    <hr>
                    <div class="right total">
                        Total: <span class="price_final">${{ buildItemPrice($user->company->available_posts) }}</span>
                    </div>
                </div>
            </div>
        </div>
        @include($theme.'.checkout.credit_card',['billing' => $billing])
        <div class="row">
            <div class="col s8 offset-s2">
                <input type="hidden" name="coupon_code" value="">
                <input type="hidden" name="featured_selected" value="">
                <input type="hidden" name="job_data" value="{{ $job_data }}">
                <input type="hidden" name="paymentType" value="newJob">
                {!! Form::submit('Pay Now!',array('class'=>'btn red right')) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection
@section('custom_js')
    <script>
        var default_price = {{ buildItemPrice($user->company->available_posts) }};
        var total_price = 0;
        var disccount_value = 0;
        $(document).ready(function(){

            $('form').on('submit', function(e){
                e.preventDefault();
                var success = true;
                if ($('.new_card').is(':visible')){
                    $('.new_card input').each(function(i,v){
                        if($(v).val() == ''){
                            $(v).addClass('error');
                            success = false;
                        } else {
                            $(v).removeClass('error')
                        }
                    })
                }

                if (success){
                    $('form').submit();
                } else {
                    $('html, body').animate({
                        scrollTop:$('.new_card').offset().top
                    },500);
                }
            });

            $('input[name="add_creditcard"]').on('click',function(e){
                if ($(this).is(':checked')){
                    $('.new_card').slideDown();
                    $('select[name="payment_card"]').attr('disabled',true);
                } else {
                    $('.new_card').slideUp();
                    $('select[name="payment_card"]').attr('disabled',false);
                }
            })

            $('.featured_btns a').on('click',function(e){
                e.preventDefault();

                if($(this).hasClass('active')) {
                    $('.price_featured').hide();
                    $(this).removeClass('active');
                    total_price = default_price;
                }else {
                    $('.featured_btns a').removeClass('active');
                    $(this).addClass('active');
                    $('.price_featured').show();
                    $('.price_featured').text('$'+$(this).data('price'));
                    total_price = default_price + $(this).data('price');
                }
                if (disccount_value != 0) total_price = total_price - disccount_value;
                if (total_price < 0) total_price = 0 ;
                $('.price_final').text('$'+total_price);
                $('input[name="featured_selected"]').val($(this).data('price'));

            })

            $('.redeem').on('click',function(e){
                e.preventDefault();
                var code = $('input[name="coupon"]').val();
                $.ajax({
                    type:'POST',
                    url:'{{ url('jobs/checkout/redeem') }}',
                    data:{code:code},
                    dataType:'json',
                    success:function(html){
                        if (html['status']){
                            $('input[name="coupon_code"]').val(html['coupon']['token']);
                            disccount_value = html['value'];
                            $('.disccount_input').fadeOut();
                            $('.disccount_added .disccount_code').text(html['coupon']['token']);
                            $('.disccount_added .disccount_value').text('-$'+html['value']);
                            $('.disccount_added').fadeIn();
                            total_price = total_price - html['value'];
                            if (total_price < 0) total_price = 0;
                            $('.price_final').text('$'+total_price);
                        } else {
                            $('.disccount_input input').css('color','red');
                        }
                    }
                })
            })
        })
    </script>
@endsection