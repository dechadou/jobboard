@extends($theme.'.checkout.base')
@section('nav_checkout')
@section('nav_checkout')
    <li class="active"><a href="#"><strong>1.</strong> <span>Pricing</span></a></li>
    <li><a href="#">2</a></li>
@endsection
@endsection
@section('content')
    <div class="container checkout">
        <div class="row">
            <div class="col s8 offset-s2">
                <h1>Checkout</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
            </div>
        </div>
        {!! Form::open(array('url' => 'jobs/checkout/save', 'request' => 'form')) !!}
        <div class="row">
            <div class="col s8 offset-s2">
                <div class="card">
                    <div class="left">
                        <h1>Select amount of posts</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                    </div>
                    <div class="post_amount">
                        <a class="btn btn-minus"><i class="fa fa-minus"></i></a>
                        <input type="text" name="post_amount"  value="1">
                        <a class="btn btn-plus"><i class="fa fa-plus"></i></a>
                    </div>

                    <hr>
                    <div class="disccount_input">
                        <div class="input-field col s9">
                            <input placeholder="Enter your discount or Bulk code here" name="coupon" type="text">
                        </div>
                        <a href="#" class="btn no-shadow redeem unactive right">Redeem</a>
                    </div>
                    <p class="disccount_added" style="display: none">
                        Disccount code number: <span class="disccount_code"></span>
                        <span class="right disccount_value">-$110</span>
                    </p>

                    <div class="clear"></div>
                    <hr>
                    <div class="right total">
                        Total: <span class="price_final">${{ $pricing->first()->value }}</span>
                    </div>
                </div>
            </div>
        </div>
        @include($theme.'.checkout.credit_card',['billing' => $billing])
        <div class="row">
            <div class="col s8 offset-s2">
                {!! Form::submit('Pay Now!',array('class'=>'btn red right')) !!}
                </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection
@section('custom_js')
    <script>
        var default_price = 0;
        var total_price = 0;
        var disccount_value = 0;
        $(document).ready(function(){

            var pricing = [];
            @foreach($pricing as $price)
                pricing.push({{ $price->value }})
            @endforeach
            console.log(pricing);

            $('input[name="add_creditcard"]').on('click',function(e){
                if ($(this).is(':checked')){
                    $('.new_card').slideDown();
                    $('select[name="payment_card"]').attr('disabled',true);
                } else {
                    $('.new_card').slideUp();
                    $('select[name="payment_card"]').attr('disabled',false);
                }
            });

            $('input[name="post_amount"]').on('keyup', function(e){
                var currentValue = $('input[name="post_amount"]').val();
                if (currentValue == '' || currentValue < 1){
                    currentValue = 1;
                    $('input[name="post_amount"]').val(1);
                }
                if (currentValue >= 11){
                    var price = pricing.slice(-1)[0];
                } else {
                    var price = pricing[currentValue-1];
                }
                $('input[name="post_amount"]').val(currentValue);
                $('.price_final').text('$'+currentValue * price);
            })

            $('.btn-minus').on('click', function(e){
                e.preventDefault();
                var currentValue = $('input[name="post_amount"]').val();
                currentValue--;
                if (currentValue <= 1){
                    currentValue = 1;
                }
                if (currentValue >= 11){
                    var price = pricing.slice(-1)[0];
                } else {
                    var price = pricing[currentValue-1];
                }
                $('input[name="post_amount"]').val(currentValue);
                $('.price_final').text('$'+currentValue * price);

            });
            $('.btn-plus').on('click', function(e){
                e.preventDefault();
                var currentValue = $('input[name="post_amount"]').val();
                currentValue++;
                $('input[name="post_amount"]').val(currentValue);
                if (currentValue >= 11){
                    var price = pricing.slice(-1)[0];
                } else {
                    var price = pricing[currentValue-1];
                }
                $('.price_final').text('$'+currentValue * price);

            })

            $('.redeem').on('click',function(e){
                e.preventDefault();
                var code = $('input[name="coupon"]').val();
                $.ajax({
                    type:'POST',
                    url:'{{ url('jobs/checkout/redeem') }}',
                    data:{code:code},
                    dataType:'json',
                    success:function(html){
                        if (html['status']){
                            $('input[name="coupon_code"]').val(html['coupon']['token']);
                            disccount_value = html['value'];
                            $('.disccount_input').fadeOut();
                            $('.disccount_added .disccount_code').text(html['coupon']['token']);
                            $('.disccount_added .disccount_value').text('-$'+html['value']);
                            $('.disccount_added').fadeIn();
                            total_price = total_price - html['value'];
                            if (total_price < 0) total_price = 0;
                            $('.price_final').text('$'+total_price);
                        } else {
                            $('.disccount_input input').css('color','red');
                        }
                    }
                })
            })
        })
    </script>
@endsection