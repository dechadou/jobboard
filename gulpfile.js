var pkg  = require('./package.json'),
    gulp = require('gulp'),
    del  = require('del'),

    less         = require('gulp-less'),
    copy         = require('gulp-copy'),
    uglify       = require('gulp-uglify'),
    concat       = require('gulp-concat'),
    autoprefixer = require('gulp-autoprefixer'),
    minifyCss    = require('gulp-minify-css');


/**
 * Backend Resources
 */
gulp.task('backend:lessCompile', function(){
  var webPath = pkg.paths.web + '/assets/backend/css';
  del([webPath], function(){});

  gulp.src([
    pkg.paths.resources + 'less/app.less',
    pkg.paths.vendors + 'admin-lte/dist/css/skins/*.*',
    pkg.paths.vendors + 'admin-lte/dist/css/AdminLTE.css',
    pkg.paths.vendors + 'chosen/chosen.min.css',
    pkg.paths.vendors + 'admin-lte/plugins/datatables/*.css'
  ])
      .pipe(concat('app.css'))
      .pipe(less({
        paths: [
          pkg.paths.resources,
          pkg.paths.vendors
        ]
      }))
      .pipe(autoprefixer({
        browsers: ['last 3 versions'],
        cascade: false
      }))
      .pipe(gulp.dest(webPath));
});

gulp.task('backend:vendorsPlugins', function(){
  gulp.src([
    pkg.paths.vendors + 'Chart.js/Chart.js',
    pkg.paths.vendors + 'chosen/chosen.jquery.min.js',
    pkg.paths.vendors + '/admin-lte/plugins/**/*.js'

  ])
      .pipe(concat('plugins.js'))
      .pipe(uglify({
        output: {
          ascii_only: true
        }
      }))
      .pipe(gulp.dest(pkg.paths.web + 'assets/backend/js'));
});

gulp.task('backend:vendorsJsCompile', function(){
  gulp.src([
    pkg.paths.vendors + 'admin-lte/bootstrap/js/bootstrap.js',
    pkg.paths.vendors + 'admin-lte/dist/js/app.js'
  ])
      .pipe(concat('vendors.js'))
      .pipe(uglify({
        output: {
          ascii_only: true
        }
      }))
      .pipe(gulp.dest(pkg.paths.web + 'assets/backend/js'));

  gulp.src([
    pkg.paths.vendors + 'admin-lte/dist/js/pages/dashboard2.js'
  ])
      .pipe(concat('dashboard2.js'))
      .pipe(gulp.dest(pkg.paths.web + 'assets/backend/js'));


});

gulp.task('backend:moveImages', function(){
  gulp.src([
      pkg.paths.vendors + 'chosen/*.{png,jpg,gif,svg}',
    pkg.paths.resources + 'backend/images/*.{png,jpg,gif,svg}'
  ])
      .pipe(gulp.dest(pkg.paths.web + 'assets/backend/images'));
});

gulp.task('backend:all', ['backend:lessCompile','backend:vendorsPlugins','backend:vendorsJsCompile', 'backend:moveImages']);

/**
 * FrontEnd Resources
 */
gulp.task('frontend:compileCssVendors', function(){
  gulp.src([
    pkg.paths.vendors + 'materialize/dist/css/materialize.css',
    pkg.paths.vendors + 'components-font-awesome/css/font-awesome.css',
    pkg.paths.vendors + 'jQuery.mmenu/dist/css/jquery.mmenu.all.css',
    pkg.paths.vendors + 'wysiwyg.js/dist/wysiwyg-editor.min.css'
  ])

      .pipe(concat('main.css'))
      /*.pipe(autoprefixer({
        browsers: ['last 3 versions'],
        cascade: false
      }))*/
      //.pipe(minifyCss({compatibility: 'ie8'}))
      .pipe(gulp.dest(pkg.paths.web + 'assets/frontend/css'));
});

gulp.task('frontend:compileCssCustom', function(){
  gulp.src([
    pkg.paths.resources + 'frontend/css/style.css'
  ])
      .pipe(concat('style.css'))
      .pipe(gulp.dest(pkg.paths.web + 'assets/frontend/css'))
});



gulp.task('frontend:compileVendorsJs', function(){
  gulp.src([
    pkg.paths.vendors + 'jquery/dist/jquery.js',
    pkg.paths.vendors + 'materialize/dist/js/materialize.js',
    pkg.paths.vendors + 'jQuery.mmenu/dist/js/jquery.mmenu.min.all.js',
    pkg.paths.vendors + 'notifyjs/dist/notify-combined.js',
    pkg.paths.vendors + 'devbridge-autocomplete/dist/jquery.autocomplete.js',
    pkg.paths.vendors + 'Chart.js/Chart.js',
    pkg.paths.vendors + 'admin-lte/plugins/sparkline/jquery.sparkline.js',
    pkg.paths.resources + 'frontend/js/jquery-gdrive.js',
    pkg.paths.vendors + 'wysiwyg.js/dist/wysiwyg.min.js',
    pkg.paths.vendors + 'tinymce/tinymce.min.js',
    pkg.paths.vendors + 'tinymce/jquery.tinymce.min.js',
    pkg.paths.vendors + 'tinymce/plugins/**/*.js',
    pkg.paths.vendors + 'wysiwyg.js/dist/wysiwyg-editor.min.js'
  ])
      .pipe(concat('vendors.js'))
      .pipe(uglify({
        output: {
          ascii_only: true
        }
      }))
      .pipe(gulp.dest(pkg.paths.web + 'assets/frontend/js'));

});

gulp.task('frontend:compileMainJs', function(){
  gulp.src([
    pkg.paths.resources + 'frontend/js/main.js'
  ])
      .pipe(concat('main.js'))
      .pipe(gulp.dest(pkg.paths.web + 'assets/frontend/js'))
});

gulp.task('frontend:moveFonts', function(){
  /* font-awesome */
  gulp.src([
      pkg.paths.vendors + 'components-font-awesome/fonts/*.{otf,eot,svg,ttf,woff,woff2}'
  ])
      .pipe(gulp.dest(pkg.paths.web + 'assets/frontend/fonts'));
  /* materialize */
  gulp.src([
    pkg.paths.vendors + 'materialize/dist/font/material-design-icons/*.{otf,eot,svg,ttf,woff,woff2}'
  ])
      .pipe(gulp.dest(pkg.paths.web + 'assets/frontend/font/material-design-icons/'));

  gulp.src([
    pkg.paths.vendors + 'materialize/dist/font/roboto/*.{otf,eot,svg,ttf,woff,woff2}'
  ])
      .pipe(gulp.dest(pkg.paths.web + 'assets/frontend/font/roboto/'));

})

gulp.task('frontend:moveImages', function(){
  gulp.src([
      pkg.paths.resources + 'frontend/images/*.{png,jpg,gif,svg}'
  ])
      .pipe(gulp.dest(pkg.paths.web + 'assets/frontend/images'));
});

gulp.task('frontend:all', ['frontend:compileCss', 'frontend:compileJs', 'frontend:moveFonts','frontend:moveImages']);