<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 14/5/15
 * Time: 12:06
 */

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Pricing;

class PricingTableSeeder extends Seeder{

  public function run()
  {

    /*if (App::environment() === 'production') {
      exit('You are in Production!!');
    }*/

    DB::table('pricings')->truncate();


    Pricing::create([
        'description' => 'Renewal',
        'value'       => 99
    ]);

    Pricing::create([
        'description' => 'Featured 1 Week',
        'value'       => 29
    ]);

    Pricing::create([
        'description' => 'Featured 2 Weeks',
        'value'       => 49
    ]);

    Pricing::create([
        'description' => 'Featured 3 Weeks',
        'value'       => 89
    ]);

    Pricing::create([
        'description' => 'Stairs 1st Post',
        'value'       => 259
    ]);
    Pricing::create([
        'description' => 'Stairs 2nd Post',
        'value'       => 238
    ]);
    Pricing::create([
        'description' => 'Stairs 3th Post',
        'value'       => 219
    ]);
    Pricing::create([
        'description' => 'Stairs 4th Post',
        'value'       => 202
    ]);
    Pricing::create([
        'description' => 'Stairs 5th Post',
        'value'       => 186
    ]);
    Pricing::create([
        'description' => 'Stairs 6th Post',
        'value'       => 171
    ]);
    Pricing::create([
        'description' => 'Stairs 7th Post',
        'value'       => 157
    ]);
    Pricing::create([
        'description' => 'Stairs 8th Post',
        'value'       => 144
    ]);
    Pricing::create([
        'description' => 'Stairs 9th Post',
        'value'       => 133
    ]);
    Pricing::create([
        'description' => 'Stairs 10th Post',
        'value'       => 122
    ]);
    Pricing::create([
        'description' => 'Stairs 11th Post',
        'value'       => 113
    ]);
    Pricing::create([
        'description' => 'Stairs 12th Post',
        'value'       => 104
    ]);


  }

}