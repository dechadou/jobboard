<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 14/5/15
 * Time: 12:06
 */

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Jobs;

class JobTableSeeder extends Seeder{

  public function run()
  {

    /*if (App::environment() === 'production') {
      exit('I just stopped you getting fired. Love, Amo.');
    }*/

    DB::table('jobs')->truncate();

    Jobs::create([
        'title'          => 'PHP Senior Developer',
        'company_id'   => 1,
        'the_job'        => 'Test',
        'the_candidate' => 'Test test test',
        'location_id' => 1,
        'job_type' => 'full-time',
        'app_by' => 'adasd@asdasd.com',
        'status' => 1
    ]);

    Jobs::create([
        'title'          => 'PHP Senior asdasd',
        'company_id'   => 2,
        'the_job'        => 'Test',
        'the_candidate' => 'Test test test',
        'location_id' => 2,
        'job_type' => 'full-time',
        'app_by' => 'adasd@asdasd.com',
        'status' => 1
    ]);

    Jobs::create([
        'title'          => 'P123123or Developer',
        'company_id'   => 3,
        'the_job'        => 'Test',
        'the_candidate' => 'Test test test',
        'location_id' => 1,
        'job_type' => 'full-time',
        'app_by' => 'adasd@asdasd.com',
        'status' => 1
    ]);
  }

}