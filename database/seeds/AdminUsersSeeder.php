<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 14/5/15
 * Time: 12:06
 */

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class AdminUsersSeeder extends Seeder{

  public function run()
  {

    DB::table('users')->truncate();

    User::create([
        'id'            => 1,
        'name'          => 'admin',
        'email'         => 'admin@socialsnack.com',
        'password'      => Hash::make('admin'),
        'role_id'       => 1
    ]);


  }

}