<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 14/5/15
 * Time: 12:06
 */

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class FrontUsersSeeder extends Seeder{

  public function run()
  {

    //DB::table('users')->truncate();

    User::create([
        'id'            => 2,
        'name'          => 'Rodrigo Catalano',
        'email'         => 'rodrigo@socialsnack.com',
        'password'      => Hash::make('12345'),
        'role_id'       => 5
    ]);


  }

}