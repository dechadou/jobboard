<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 14/5/15
 * Time: 12:06
 */

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Module;

class ModuleTableSeeder extends Seeder{

  public function run()
  {

    /*if (App::environment() === 'production') {
      exit('I just stopped you getting fired. Love, Amo.');
    }*/

    DB::table('modules')->truncate();

    Module::create([
        'id'            => 1,
        'name'          => 'Authorize.net',
        'description'   => 'Payment Method bla bla',
        'type'          => 'Payment',
        'status'        => 1
    ]);

    Module::create([
        'id'            => 2,
        'name'          => 'PayPal',
        'description'   => 'Payment Method bla bla',
        'type'          => 'Payment',
        'status'        => 1
    ]);


  }

}