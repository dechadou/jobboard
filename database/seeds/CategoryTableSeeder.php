<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 14/5/15
 * Time: 12:06
 */

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Category;

class CategoryTableSeeder extends Seeder{

  public function run()
  {

    /*if (App::environment() === 'production') {
      exit('I just stopped you getting fired. Love, Amo.');
    }*/

    DB::table('categories')->truncate();

    Category::create([
        'name'          => 'Public Relations',
        'status'   => 1
    ]);

    Category::create([
        'name'          => 'Marketing',
        'status'   => 1
    ]);


    Category::create([
        'name'          => 'Analyst Relations',
        'status'   => 1
    ]);


    Category::create([
        'name'          => 'Other',
        'status'   => 1
    ]);




  }

}