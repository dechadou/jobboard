<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 14/5/15
 * Time: 12:06
 */

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Config;

class ConfigTableSeeder extends Seeder{

  public function run()
  {

    /*if (App::environment() === 'production') {
      exit('I just stopped you getting fired. Love, Amo.');
    }*/

    DB::table('configs')->truncate();

    Config::create([
        'name'          => 'site_title',
        'value'         => '',
        'description'         => 'Site Title',
    ]);

    Config::create([
        'name'          => 'keywords',
        'value'         => '',
        'description'   => 'Meta Keywords (Comma Separated)',
    ]);

    Config::create([
        'name'          => 'description',
        'value'         => '',
        'description'   => 'Meta Description',
    ]);

    Config::create([
        'name'          => 'copyright',
        'value'         => '',
        'description'   => 'Site Copyright',
    ]);

    Config::create([
        'name'          => 'analytics',
        'value'         => '',
        'description'   => 'Google Analytics Code',
    ]);

    Config::create([
        'name'          => 'facebook',
        'value'         => '',
        'description'         => 'Facebook FanPage',
    ]);

    Config::create([
        'name'          => 'twitter',
        'value'         => '',
        'description'         => 'Twitter FanPage',
    ]);

    Config::create([
        'name'          => 'google',
        'value'         => '',
        'description'         => 'Google FanPage',
    ]);

    Config::create([
        'name'          => 'site_favicon',
        'value'         => '',
        'description'         => 'Site Favicon',
    ]);

    Config::create([
        'name'          => 'site_logo',
        'value'         => '',
        'description'         => 'Site Logo',
    ]);

    Config::create([
        'name'          => 'home_background',
        'value'         => '',
        'description'         => 'Home Background Image',
    ]);

    Config::create([
        'name'          => 'home_text',
        'value'         => '',
        'description'         => 'Home Text Header',
    ]);




  }

}