<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobAnalyticsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('job_analytics', function(Blueprint $table)
		{
			$table->increments('id');
      $table->integer('job_id')->unsigned();
      $table->foreign('job_id')->references('id')->on('jobs');
      $table->integer('type');
      $table->string('ip');
      $table->string('location');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('job_analytics');
	}

}
