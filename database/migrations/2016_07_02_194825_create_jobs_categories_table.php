<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('jobs_categories', function(Blueprint $table)
		{
			$table->increments('id');
      $table->integer('job_id')->unsigned();
      $table->foreign('job_id')->references('id')->on('jobs');
      $table->integer('category_id')->unsigned();
      $table->foreign('category_id')->references('id')->on('categories');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('jobs_categories');
	}

}
