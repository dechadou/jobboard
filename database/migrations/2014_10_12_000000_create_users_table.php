<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('email')->unique();
			$table->string('password', 60);
      $table->integer('role_id')->unsigned();
      $table->foreign('role_id')->references('id')->on('role')->onDelete('cascade');
      $table->string('avatar');
      $table->integer('country_id')->unsigned();
      $table->foreign('country_id')->references('id')->on('countries');
      $table->text('address');
      $table->text('state');
      $table->string('zip_code');
      $table->string('bussiness_phone');
      $table->string('mobile_phone');
      $table->string('website');
			$table->rememberToken();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
