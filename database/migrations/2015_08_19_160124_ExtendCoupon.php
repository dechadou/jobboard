<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtendCoupon extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
    Schema::table('coupons', function(Blueprint $table)
    {
      $table->integer('discount_type');
    });
    DB::statement('ALTER TABLE coupons MODIFY COLUMN discount VARCHAR(200)');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
