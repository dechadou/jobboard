<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('jobs', function(Blueprint $table)
    {
      $table->increments('id');
      $table->text('title');
      $table->integer('company_id')->unsigned();
      $table->foreign('company_id')->references('id')->on('companies');
      $table->text('the_job');
      $table->text('the_candidate')->nullable();
      $table->integer('location_id')->unsigned()->nullable();
      $table->foreign('location_id')->references('id')->on('locations');
      $table->integer('industry_id')->unsigned()->nullable();
      $table->foreign('industry_id')->references('id')->on('industries');
      $table->string('industry_other');
      $table->string('zip_code');
      $table->integer('job_type')->unsigned();
      $table->foreign('job_type')->references('id')->on('job_types');
      $table->integer('app_by');
      $table->text('app_by_text');
      $table->integer('featured');
      $table->integer('sticky');
      $table->integer('status');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('jobs');
  }

}
