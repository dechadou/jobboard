<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */




	public function up()
	{
		Schema::create('locations', function(Blueprint $table)
		{
			$table->increments('id');
      $table->string('state');
      $table->string('city');
      $table->integer('lat');
      $table->integer('longitude');
      $table->string('zip');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('locations');
	}

}
