<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFavs extends Model {

  protected $table = 'user_favs';

  public function jobs()
  {
    return $this->belongsToMany('App\Jobs', 'job_id', 'id');
  }

}
