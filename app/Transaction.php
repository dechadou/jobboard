<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model {

  protected $table = 'transactions';

  public function transactions()
  {
    return $this->belongsTo('App\Company', 'company_id', 'id');
  }

  public function modules()
  {
    return $this->belongsTo('App\Module', 'module_id', 'id');
  }

}
