<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class JobsCategory extends Model {

  protected $table = 'jobs_categories';

  public function categories()
  {
    return $this->belongsToMany('App\Jobs', 'job_id', 'id');
  }

}
