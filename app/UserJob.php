<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UserJob extends Model {

  protected $table = 'user_jobs';

  public function users()
  {
    return $this->belongsToMany('App\User', 'user_id', 'id');
  }

  public function jobs(){
    return $this->belongsToMany('App\Jobs', 'job_id', 'id');
  }

}
