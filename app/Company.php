<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model {

  protected $table = 'companies';

  public function jobs()
  {
    return $this->hasMany('App\Jobs', 'company_id', 'id');
  }

  public function transactions()
  {
    return $this->hasMany('App\Transaction', 'company_id', 'id');
  }

  public function user()
  {
    return $this->hasOne('App\User', 'id', 'user_id');
  }

  public function billing()
  {
    return $this->hasMany('App\UserBilling', 'user_id', 'id');
  }





}
