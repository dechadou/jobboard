<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model {

  public function transactions()
  {
    return $this->belongStoMany('App\Transaction', 'module_id', 'id');
  }

}
