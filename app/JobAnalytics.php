<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class JobAnalytics extends Model {

	public function jobs(){
    return $this->hasOne('App\Jobs');
  }

}
