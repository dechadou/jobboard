<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Jobs extends Model {

  protected $table = 'jobs';

  public function companies()
  {
    return $this->hasOne('App\Company', 'id', 'company_id');
  }
  public function categories()
  {
    return $this->belongsToMany('App\Category', 'jobs_categories', 'job_id');
  }

  public function favs(){
    return $this->belongsToMany('App\User', 'user_favs');
  }

  public function locations(){
    return $this->hasOne('App\Location', 'id', 'location_id');
  }

  public function types(){
    return $this->hasOne('App\JobType', 'id', 'job_type');
  }

  public function industry(){
    return $this->hasOne('App\Industry', 'id', 'industry_id');
  }

  public static function getJobList($offset=''){
    $date = new \DateTime;

    $jobs = Jobs::query();

    $jobs->where('status',1);
    $jobs->where('expiration_date','>',$date->format('Y-m-d H:i:s'));
    $jobs->orderBy('sticky','desc');
    $jobs->orderBy('featured','desc');
    $jobs->orderBy('updated_at','desc');;
    if($offset != ''){
      $jobs->skip($offset+1);
    }
    $jobs->take(16);

    return $jobs->get();

  }

  public static function getTotalJobs(){
    $date = new \DateTime;
    return Jobs::where('status',1)
        ->where('expiration_date','>',$date->format('Y-m-d H:i:s'))
        ->get()->count();
  }

  public function applied()
  {
    return $this->belongsToMany('App\User', 'user_jobs');
  }

  public function appliedDetails($id){
    $appliedDetails =  UserJob::where('jobs_id','=',$id)->get();
    return $appliedDetails;
  }

  public function appliedFiles($files){
    if($files){
      $array = explode(',',$files);
      foreach($array as $f){
        $data[] =  UserFiles::where('id','=',$f)->get();
      }
      foreach($data as $d){
        if(!$d->isEmpty()){
          echo '<a href="'.asset('uploads/user_files/'.$d[0]->user_id.'/'.$d[0]->file).'" target="_blank">'.$d[0]->file_name.'</a><br>';
        } else {
          echo 'The User has deleted the provided file';
        }
      }
    }
  }

  public function getAnalytics(){
    return $this->hasMany('App\JobAnalytics', 'job_id');
  }

  public static function ajaxJobs(){
    // TODO: this.
  }

  public static function SearchByCategory($category){
    $search = Jobs::whereHas('categories', function($q) use ($category) {
      $q->whereIn('category_id',$category);
    });
    return $search;
  }

  public static function SimilarJobs($job){
    $date = new \DateTime;
    if($job->categories->isEmpty()) {
      return false;
    }
    foreach($job->categories as $cat){
      $cId[] = $cat->id;
    }
    $similar_jobs = Jobs::whereHas('categories', function($q) use ($cId,$job) {
      $q->whereIn('category_id',$cId);
      $q->orWhere('location_id','=',$job->location_id);
    })
        ->where('status',1)
        ->where('expiration_date','>',$date->format('Y-m-d H:i:s'))
        ->where('id','!=',$job->id)
        ->orderBy('sticky','desc')
        ->orderBy('featured','desc')
        ->orderBy('updated_at','desc')
        ->take(4)
        ->get();
    return $similar_jobs;

  }

}
