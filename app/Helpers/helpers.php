<?php
use App\Coupons;
use App\JobAnalytics;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 2/7/15
 * Time: 16:03
 */

/* Retrieve Default Logo if company doesnt have one */
function CompanyLogo($company){
  return ($company->avatar == '') ? asset('assets/frontend/images/no-logo.png') : asset('uploads/company_logo/'.$company->id.'/'.$company->avatar);
}

function userAvatar($logged_user){
  if (Auth::user()){
    if ($logged_user->auth_provider != 'hoojobs'){
      if (strpos($logged_user->avatar,'https://media.licdn.com/') !== false) {
        return $logged_user->avatar;
      } else {
        return ($logged_user->avatar == '') ? asset('assets/frontend/images/no-avatar.png') : asset('uploads/user_avatar/'.$logged_user->id.'/'.$logged_user->avatar);
      }
    }
    return ($logged_user->avatar == '') ? asset('assets/frontend/images/no-avatar.png') : asset('uploads/user_avatar/'.$logged_user->id.'/'.$logged_user->avatar);
  } else {
    return asset('assets/frontend/images/no-avatar.png');
  }
}

function getPermaLink($job){
  return url('jobs/view/'.$job->id.'/'.str_slug($job->title));
}

function isFav($id,$logged_user_favs){
  return (in_array($id,$logged_user_favs)) ? true : false;
}

function get_file_extension($file_name) {
  return substr(strrchr($file_name,'.'),1);
}

function is_multi($a) {
  $rv = array_filter($a,'is_array');
  if(count($rv)>0) return true;
  return false;
}


function parseBilling($billing){
  $billingData = array(
      'customerProfileId' => $billing['customerProfileId'],
      'paymentProfiles' => [],
      'shipToList' => []
  );

  // Populate paymentProfiles
  if (isset($billing['paymentProfiles'])){
    $isArrayOfArrays = array_filter($billing['paymentProfiles'], 'is_array') === $billing['paymentProfiles'];
    if ($isArrayOfArrays){
      foreach($billing['paymentProfiles'] as $paymentProfiles){
        $billingData['paymentProfiles'][] = serializePayment($paymentProfiles);
      }
    } else {
      $billingData['paymentProfiles'][] = serializePayment($billing['paymentProfiles']);
    }
  }


  // Populate shipToList
  if (isset($billing['shipToList'])){
    $isArrayOfArrays = array_filter($billing['shipToList'], 'is_array') === $billing['shipToList'];
    if ($isArrayOfArrays){
      foreach($billing['shipToList'] as $shipToList){
        $billingData['shipToList'][] = serializeShipping($shipToList);
      }
    } else {
      $billingData['shipToList'][] = serializeShipping($billing['shipToList']);
    }
  }
  return $billingData;
}


function serializeShipping($billing){
  return array(
      'firstName'         => isset($billing['firstName']) ? $billing['firstName'] : '',
      'lastName'          => isset($billing['lastName']) ? $billing['lastName'] : '',
      'company'           => isset($billing['company']) ? $billing['company'] : '',
      'address'           => isset($billing['address']) ? $billing['address'] : '',
      'city'              => isset($billing['city']) ? $billing['city'] : '',
      'state'             => isset($billing['state']) ? $billing['state'] : '',
      'zip'               => isset($billing['zip']) ? $billing['zip'] : '',
      'country'           => isset($billing['country']) ? $billing['country'] : '',
      'phoneNumber'       => isset($billing['phoneNumber']) ? $billing['phoneNumber'] : '',
      'faxNumber'         => isset($billing['faxNumber']) ? $billing['faxNumber'] : '',
      'customerAddressId' => isset($billing['customerAddressId']) ? $billing['customerAddressId'] : ''
  );
}

function serializePayment($billing){
  return array(
      'customerType'  => $billing['customerType'],
      'billTo'        => array(
          'firstName'     => isset($billing['billTo']['firstName']) ? $billing['billTo']['firstName'] : '',
          'lastName'      => isset($billing['billTo']['lastName']) ? $billing['billTo']['firstName'] : '',
          'company'       => isset($billing['billTo']['company']) ? $billing['billTo']['company'] : '',
          'address'       => isset($billing['billTo']['address']) ? $billing['billTo']['address'] : '',
          'city'          => isset($billing['billTo']['city']) ? $billing['billTo']['city'] : '',
          'state'         => isset($billing['billTo']['state']) ? $billing['billTo']['state'] : '',
          'zip'           => isset($billing['billTo']['zip']) ? $billing['billTo']['zip'] : '',
          'country'       => isset($billing['billTo']['country']) ? $billing['billTo']['country'] : '',
          'phoneNumber'   => isset($billing['billTo']['phoneNumber']) ? $billing['billTo']['phoneNumber'] : '',
          'faxNumber'     => isset($billing['billTo']['faxNumber']) ? $billing['billTo']['faxNumber'] : ''
      ),
      'customerPaymentProfileId' => $billing['customerPaymentProfileId'],
      'payment' => array(
          'creditCard' => array(
              'cardNumber' => $billing['payment']['creditCard']['cardNumber'],
              'expirationDate' => $billing['payment']['creditCard']['expirationDate']
          )
      )
  );
}

function getFeaturedPrices($id){
  // id 2 = featured 1 week, 3 = featured 2 week, 4 = featured 3 week
  return \App\Pricing::where('id',$id)->get()->first()->value;
}

function getRenewalPrice(){
  return \App\Pricing::where('id',1)->get()->first()->value;
}

function buildItemPrice($postCount){
      // ID 5 is the first on the stairway. UP to 16th
  if ($postCount >= 11){
    return \App\Pricing::where('id',16)->get()->first()->value;
  }
  return \App\Pricing::where('id',$postCount+5)->get()->first()->value;
}

// parseTransaction data
function parseFeaturedTransaction($coupon = false, $price){
  $dateTime = new \DateTime;
  $transactionData = array(
      'itemId' => Auth::user()->id.'-'.Auth::user()->company->merchant_id.'-'.$dateTime->getTimestamp(),
      'itemName' => 'Make job Featured',
      'itemDescription' => 'Make Job Featured',
      'itemQuantity' => 1,
      'itemPrice' => $price
  );
  $transactionData['amount'] = $transactionData['itemQuantity'] * $transactionData['itemPrice'];

  if ($coupon != false){
    $disccount = Coupons::where('token','=',$coupon)->where('status',1)->get()->first();
    switch($disccount->disccount_type){
      case 0:
        // Money Discount
        $transactionData['amount'] = $transactionData['amount'] - $disccount->discount;
        break;
      case 1:
        // Percentage Discount
        $transactionData['amount'] = $transactionData['amount'] - (($disccount->discount * $transactionData['amount']) / 100);
        break;
      case 2:
        // Free Publication
        $transactionData['amount'] = 0;
        $transactionData['itemPrice'] = 0;
        break;
      case 3:
        // Free featured
        $transactionData['amount'] = 0;
        break;
    }
    if ($transactionData['amount'] < 0) $transactionData['amount'] = 0;


    $disccount->user_id = Auth::user()->id;
    $disccount->status = 0;
    $disccount->save();

  }

  return $transactionData;
}

function parseSaveNowPostLater($price){
  $dateTime = new \DateTime;

  $transactionData = array(
      'itemId' => Auth::user()->id.'-'.Auth::user()->company->merchant_id.'-'.$dateTime->getTimestamp(),
      'itemName' => 'Save now post later',
      'itemDescription' => 'Save now post laster',
      'itemQuantity' => 1,
      'itemPrice' => $price
  );

  $transactionData['amount'] = $transactionData['itemQuantity'] * $transactionData['itemPrice'];
  return $transactionData;
}

function parseTransaction($quantity=1,$coupon = false, $featured, $postCount, $paymentType){
  $dateTime = new \DateTime;

  $transactionData = array(
      'itemId' => Auth::user()->id.'-'.Auth::user()->company->merchant_id.'-'.$dateTime->getTimestamp(),
      'itemName' => ($paymentType == 'newJob') ? 'Post a Single Job' : 'Renew Job for 30 days',
      'itemDescription' => ($paymentType == 'newJob') ? 'Single Post Job for Company Recruiters' : 'Renew Job',
      'itemQuantity' => $quantity,
      'itemPrice' => ($paymentType == 'newJob') ? buildItemPrice($postCount) : getRenewalPrice()
  );

  $transactionData['featured'] = ($featured != 0) ? 1 : 0 ;
  $transactionData['amount'] = $transactionData['itemQuantity'] * $transactionData['itemPrice'];
  $transactionData['amount'] = $transactionData['amount'] + $featured;

  if ($coupon != false){
    $disccount = Coupons::where('token','=',$coupon)->where('status',1)->get()->first();
    switch($disccount->disccount_type){
      case 0:
        // Money Discount
        $transactionData['amount'] = $transactionData['amount'] - $disccount->discount;
        break;
      case 1:
        // Percentage Discount
        $transactionData['amount'] = $transactionData['amount'] - (($disccount->discount * $transactionData['amount']) / 100);
        break;
      case 2:
        // Free Publication
        $transactionData['amount'] = 0;
        $transactionData['itemPrice'] = 0;
        break;
      case 3:
        // Free featured
        $transactionData['featured'] = 1;
        $transactionData['amount'] = $transactionData['amount'] - $featured;
        break;
    }
    if ($transactionData['amount'] < 0) $transactionData['amount'] = 0;


    $disccount->user_id = Auth::user()->id;
    $disccount->status = 0;
    $disccount->save();

  }

  return $transactionData;
}



function compareDatesWithToday($date){
  $today = new \DateTime;
  $today->format('Y-m-d H:i:s');
  if (strtotime($date) >= strtotime($today->format('Y-m-d H:i:s'))){
    return true;
  }
  return false;
}

function isRevised($id){
  $userIp = GeoIP::getLocation()['ip'];
  $analytics = JobAnalytics::where('job_id',$id)
      ->where('ip',$userIp)
      ->take(1)->get();
  if (!$analytics->isEmpty()){
    return true;
  }
  return false;
}

function short_city($words){

  if (str_word_count($words) == 1){
    return strtoupper(substr($words, 0, 2));
  } else {
    $rv = explode(" ", $words);
    $letters = "";
    foreach ($rv as $value) {
      $letters .= substr($value, 0, 1);
    }
    return $letters;
  }

}

