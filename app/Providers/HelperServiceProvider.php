<?php namespace App\Providers;
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 2/7/15
 * Time: 16:05
 */

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
class HelperServiceProvider extends ServiceProvider
{
  public function register()
  {
    foreach (glob(app_path() . '/Helpers/*.php') as $filename) {
      require_once($filename);
    }
  }
}