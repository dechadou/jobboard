<?php namespace App\Http\Controllers\FrontEnd;

use App\Company;
use App\Http\Controllers\FrontEnd\BaseController;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterFormRequest;
use App\TokenGenerator;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Http\Request;
use Illuminate\Auth\Guard;
use App\Http\Requests\LoginFormRequest;

class LoginController extends BaseController
{

  public function logOut()
  {
    Auth::logout();
    return redirect()->route('home');
  }

  public function auth(LoginFormRequest $request, Guard $auth)
  {
    $this->middleware('crsf');

    if ($auth->attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
      if ($request->input('action') == 'heart'){
        $job_id = $request->input('job_id');
        app('App\Http\Controllers\FrontEnd\ProfileController')->addFav($job_id);
      }

      if ($request->input('return_url')) {
        return Redirect::to($request->input('return_url'))->with('message', 'The Job has been added to your favourites');
      }
      $user = Auth::user();
      $company = $user->company()->get();
      if (!$company->isEmpty()){
        return redirect('company/manage_jobs');
      }

      return redirect('profile/favourite');
    }
    return redirect()->back()->with('error', 'These credentials do not match our records.');

  }

  public function index()
  {
    if ($this->logged_user != null) {
      return redirect('/profile');
    }

    $return_url = isset($_GET['return_url']) ? $_GET['return_url'] : '';
    $action     = isset($_GET['action']) ? $_GET['action'] : '';
    $job_id     = isset($_GET['job_id']) ? $_GET['job_id'] : '';

    return View::make($this->theme.'.auth.login')
        ->with('return_url', $return_url)
        ->with('action', $action)
        ->with('job_id', $job_id);
  }

  public function loginWith(Request $request, $service)
  {
    $code = $request->get('code');

    $servicio = \OAuth::consumer($service);
    $return_url = isset($_GET['return_url']) ? $_GET['return_url'] : false;


    if (!is_null($code)) {
      $token = $servicio->requestAccessToken($code);
      $result = json_decode($servicio->request('/people/~:(first-name,last-name,picture-url,email-address)?format=json'), true);
      $result['auth_provider'] = $service;

      $query = User::where('email', '=', $result['emailAddress'])
          ->where('auth_provider','=','Linkedin')
          ->get();
      if (!$query->isEmpty()) {
        if(Auth::attempt(array('email' => $query[0]->email, 'password' => $query[0]->email))){
          $user = Auth::user();
          $company = $user->company()->get();

          if ($return_url != false) {
            return Redirect::to($return_url);
          }

          if (!$company->isEmpty()){
            return redirect('company');
          }

          return redirect('profile/favourite');
        }
      }

      $user_data = array(
          'firstName' => $result['firstName'].' '.$result['lastName'],
          'emailAddress' => $result['emailAddress'],
          'password' => $result['emailAddress'],
          'country_id' => 840, // default US
          'auth_provider' => $service,
          'avatar' => $result['pictureUrl']
      );

      return $this->createUser($user_data,'candidate');

    } else {
      $url = $servicio->getAuthorizationUri(['state' => 'DCEEFWF45453sdffef424']); //Esto es un codigo aleatorio
      return redirect((string)$url);
    }
  }

  public function register()
  {
    return View::make($this->theme.'.auth.register');
  }

  public function create()
  {
    return View::make($this->theme.'.auth.create');
  }

  public function createAccount(Request $request, $type)
  {
    $v = Validator::make($request->all(), [
        'name' => 'required|max:100',
        'email' => 'required|email',
        'password' => 'required|min:8'
    ]);

    if ($v->fails()) {
      return redirect()->back()->withErrors($v->errors());
    }


    $query = User::where('email', '=', $request->input('email'))->get();
    if (!$query->isEmpty()) {
      return redirect()->back()->withErrors('That email is already registerd. Did you forget your password?');
    }

    $user_data = array(
        'firstName' => $request->input('name'),
        'emailAddress' => $request->input('email'),
        'password' => $request->input('password'),
        'country_id' => 840, // default US
        'auth_provider' => 'hoojobs',
        'avatar' => false
    );

    return $this->createUser($user_data, $type);
  }

  public function createUser($user_data, $type, $return = false)
  {
    $user = new User();
    $user->name = $user_data['firstName'];
    $user->email = $user_data['emailAddress'];
    $user->password = Hash::make($user_data['password']);
    $user->country_id = $user_data['country_id'];
    $user->auth_provider = $user_data['auth_provider'];
    if ($user_data['avatar']){
      $user->avatar = $user_data['avatar'];
    }

    $role = ($type == 'candidate') ? 5 : 4;
    $user->role_id = $role;
    $user->save();

    if ($type == 'recruiter') {
      // Create AuthNet Customer Profile;
      $AuthNetCustomer = app('App\Http\Controllers\FrontEnd\AuthNetController')->createCustomer($user->id,$user->email);
      $company = new Company();
      $company->user_id = $user->id;
      $company->merchant_id = $AuthNetCustomer;
      $company->save();

    }

    if(Auth::attempt(['email' => $user_data['emailAddress'], 'password' => $user_data['password']]))
    {
      if ($return){
        return Auth::user();
      }
      if ($type == 'recruiter'){
        return Redirect::to('login/success/company/' . $user->id);
      }
      return Redirect::to('login/success/candidate/' . $user->id);
    }
  }

  public function success($type,$id)
  {
    $user = User::find($id);
    return View::make($this->theme.'.auth.success')->with('user', $user);
  }

  public function forgotPassword(){
    return View::make($this->theme.'.auth.forgot');
  }

  public function createToken(Request $request){
    $email = $request->input('email');
    $user = User::where('email','=',$email)->get()->first(); // traigo el usuario con ese email

    if (!$user){
      return redirect()->back()->with('error','The email provided is not recognized.'); // si no existe, error
    }

    $token = new TokenGenerator(); // creo una token nueva
    $exist = DB::table('password_resets')->where('email','=',$email)->get(); // checkeo que no haya una token existente

    if(empty($exist)){
      // si no existe el token, lo genero
      DB::table('password_resets')->insert(
          ['email' => $email, 'token' => $token]
      );
    } else {
      // si existe, lo actualizo
      DB::table('password_resets')
          ->where('email', '=',$email)
          ->update(['token' => $token]);
    }

    $data['token'] = $token;
    $data['email'] = $email;
    $data['name'] = $user->name;
    Mail::send('emails.password', $data, function($message) use($data)
    {
      $message->to($data['email'], $data['name'])->subject('[HooJobs] - Password Reset');
    });
    return View::make($this->theme.'.auth.done');
  }

  public function resetPassword($token){
    return View::make($this->theme.'.auth.change')->with('token',$token);
  }

  public function passwordChange(){
    $data = Input::all();
    $rules = [
        'password' => 'required|min:8|confirmed',
        'password_confirmation' => 'required|min:8'
    ];
    $validator = Validator::make($data,$rules);

    if ($validator->passes()) {
      $exist = DB::table('password_resets')->where('token','=',Input::all()['token'])->get();

      $user = User::where('email','=',$exist[0]->email)->get()->first();
      $user->password = Hash::make(Input::all()['password']);
      $user->save();

      return View::make($this->theme.'.auth.done_success');

    }
    return redirect()->back()->withErrors($validator->errors());
  }

  public function checkEmail(Request $request){
    $email = $request->input('email');
    $users = User::where('email', $email)->get()->first();

    if ($users == null){
      return json_encode(false);
    } else {
      return json_encode(true);
    }
  }

}

