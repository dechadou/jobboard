<?php namespace App\Http\Controllers\FrontEnd;

use App\Company;
use App\Http\Requests;
use App\Http\Controllers\Controller;


use App\Industry;
use App\Jobs;
use App\JobType;
use App\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class CompanyCardController extends CompanyProfileController {


  public function doAction($id,$action){
    switch($action){
      case 'edit':
        return $this->edit($id);
        break;
      case 'copy':
        return $this->copy($id);
        break;
      case 'pause':
        return $this->pause($id);
        break;
      case 'destroy':
        return $this->destroy($id);
        break;
      case 'analytics':
        return $this->analytics($id);
        break;
      case 'applicants':
        return $this->applicants($id);
        break;
    }
  }

  public function edit($id){
    $job = Jobs::find($id);
    $categories = [];
    foreach($job->categories as $cat){
      $categories[] = $cat->id;
    }

    return View::make($this->theme.'.jobs.edit')
        ->with('job',$job)
        ->with('cId', $categories)
        ->with('industries', Industry::all())
        ->with('jobTypes', JobType::all());

  }
  public function copy($id){
    $job = Jobs::find($id);
    $copy = new Jobs();

    $copy->title             = $job->title.' '.$job->id;
    $copy->company_id        = $job->company_id;
    $copy->the_job           = $job->the_job;
    $copy->the_candidate     = $job->the_candidate;
    $copy->location_id       = $job->location_id;
    $copy->zip_code        = $job->zip_code;
    $copy->job_type          = $job->job_type;
    $copy->app_by            = $job->app_by;
    $copy->app_by_text       = $job->app_by_text;
    $copy->status            = 0;
    $copy->featured          = $job->featured;
    $copy->sticky            = $job->sticky;

    $copy->save();

    /* add new categories */
    $categories = $job->categories;
    foreach($categories as $c){
      $copy->categories()->attach($c);
    }

    $copy_card = array(
        'id' => $copy->id,
        'title' => $copy->title,
        'company_id' => $copy->company_id,
        'the_job' => str_limit($copy->the_job,120),
        'the_candidate' => $copy->the_candidate,
        'location' => $copy->locations->name,
        'job_type' => $copy->job_type,
        'app_by' => $copy->app_by,
        'app_by_text' => $copy->app_by_text,
        'status' => $copy->status,
        'featured' => $copy->featured,
        'sticky'   => $copy->sticky,
        'company_name' => $copy->companies->name,
        'company_logo' => companyLogo($copy->companies->user),
        'edit_url' => url("company/job/".$copy->id."/edit"),
        'applicants_url'=> url("company/job/".$copy->id."/applicants"),
        'analytics_url' => url("company/job/".$copy->id."/analytics")
    );

    return response(array('status'=>true,'message'=>'The Job '.$job->title.' has been duplicated','data' => $copy_card), 200)
        ->header('Content-Type', 'application/json');


  }
  public function pause($id){
    $date = new \DateTime;
    $job = Jobs::find($id);
    if ($job->status){
      $job->status = 0;
      $msg = 'The Job has been paused and is no longer visible for Users';
      $action = 'Resume';
    } else {
      if ($job->expiration_date >= $date->format('Y-m-d H:i:s')){
        $job->status = 1;
        $msg = 'The Job has been resumed and now is visible for Users';
        $action = 'Pause';
      } else {
       return response(array('status'=>false,'redirect'=>'../jobs/checkout/renewal/'.$job->id),200)
           ->header('Content-Type', 'application/json');
      }
    }

    $job->save();

    return response(array('status'=>true,'action'=>$action,'message' => $msg), 200)
        ->header('Content-Type', 'application/json');
  }
  public function destroy($id){
    $job = Jobs::find($id);
    $job->categories()->detach();
    $job->favs()->detach();
    $job->destroy($id);
    return response(array('status'=>true,'message' => 'The Job has been deleted'), 200)
        ->header('Content-Type', 'application/json');
  }
  public function analytics($id){
    $job = Jobs::find($id);

    $actualMonth =  date('M');

    $analyticsTotal = array(
        'Jan' => array('views'=>0,'applications'=>0),
        'Feb' => array('views'=>0,'applications'=>0),
        'Mar' => array('views'=>0,'applications'=>0),
        'Apr' => array('views'=>0,'applications'=>0),
        'May' => array('views'=>0,'applications'=>0),
        'Jun' => array('views'=>0,'applications'=>0),
        'Jul' => array('views'=>0,'applications'=>0),
        'Aug' => array('views'=>0,'applications'=>0),
        'Sep' => array('views'=>0,'applications'=>0),
        'Oct' => array('views'=>0,'applications'=>0),
        'Nov' => array('views'=>0,'applications'=>0),
        'Dec' => array('views'=>0,'applications'=>0)
    );

    $analyticsMonth = array(
        '1-10'=>array('views'=>0,'applications'=>0),
        '11-20'=>array('views'=>0,'applications'=>0),
        '21-31'=>array('views'=>0,'applications'=>0)
    );
    foreach($job->getAnalytics as $analytics) {
      if ($analytics->type == 0) {
        $analyticsTotal[date_format($analytics->created_at, 'M')]['views']++;
        if (date_format($analytics->created_at, 'M') == $actualMonth) {
          if (date_format($analytics->created_at, 'j') < 11) {
            $analyticsMonth['1-10']['views']++;
          }
          if (date_format($analytics->created_at, 'j') > 10 && date_format($analytics->created_at, 'j') < 21) {
            $analyticsMonth['11-20']['views']++;
          }
          if (date_format($analytics->created_at, 'j') > 20) {
            $analyticsMonth['21-31']['views']++;
          }

        }
      }
      if ($analytics->type == 1) {
        $analyticsTotal[date_format($analytics->created_at, 'M')]['applications']++;
        if (date_format($analytics->created_at, 'M') == $actualMonth) {

          if (date_format($analytics->created_at, 'j') < 11) {
            $analyticsMonth['1-10']['applications']++;
          }
          if (date_format($analytics->created_at, 'j') > 10 && date_format($analytics->created_at, 'j') < 21) {
            $analyticsMonth['11-20']['applications']++;
          }
          if (date_format($analytics->created_at, 'j') > 20) {
            $analyticsMonth['21-31']['applications']++;
          }
        }
      }
    }

    $totalsGraph = [];
    $viewsGraph = [];
    $applicationsGraph = [];
    $totalsNumber = 0;
    $viewsNumber = 0;
    $applicationsNumber = 0;
    foreach($analyticsTotal as $data => $v){
      $totalsGraph[$data] = $analyticsTotal[$data]['views']+$analyticsTotal[$data]['applications'];
      $viewsGraph[$data] = $analyticsTotal[$data]['views'];
      $applicationsGraph[$data] = $analyticsTotal[$data]['applications'];

      $totalsNumber = $totalsNumber + ($v['views']+$v['applications']);
      $viewsNumber = $viewsNumber + $v['views'];
      $applicationsNumber = $applicationsNumber + $v['applications'];
    }

    return View::make($this->theme.'.company_profile.job_analytics')
        ->with('analyticsTotal',$analyticsTotal)
        ->with('analyticsMonth',$analyticsMonth)
        ->with('totalsGraph', $totalsGraph)
        ->with('viewsGraph', $viewsGraph)
        ->with('applicationsGraph',$applicationsGraph)
        ->with('totalsNumber', $totalsNumber)
        ->with('viewsNumber',$viewsNumber)
        ->with('applicationsNumber',$applicationsNumber)
        ->with('currentMonth',$actualMonth);
  }

  public function applicants($id){
    $job = Jobs::find($id);
    return View::make($this->theme.'.company_profile.job_applications_single')->with('job',$job);
  }

}
