<?php namespace App\Http\Controllers\FrontEnd;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class ContactController extends BaseController {

  public function index(){
    return View::make($this->theme.'.contact.index');
  }
}
