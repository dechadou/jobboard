<?php namespace App\Http\Controllers\FrontEnd;

use App\Conocenos;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Page;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\View;

class PageController extends BaseController {

	public function index(Route $route, $slug)
	{
    $page = Page::where('slug','=',$slug)->get();
    if($page->isEmpty()){
      return redirect('404');
    }
		return View::make($this->theme.'.page.index')->with('page',$page[0]);
	}

}
