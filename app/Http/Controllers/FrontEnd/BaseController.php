<?php namespace App\Http\Controllers\FrontEnd;

use App\Category;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Jobs;
use App\Location;
use App\User;
use App\UserJob;
use App\UserFavs;
use App\Config;
use Illuminate\Routing\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Http\RedirectResponse;

class BaseController extends Controller {

  protected $logged_user;
  protected $is_company;
  protected $theme;

	public function __construct(User $logged_user, Request $request, Route $route){
    $generalConfig = $this->setGeneralConfig();
    View::share('site_title', $generalConfig['site_title']['value']);
    View::share('keywords', $generalConfig['keywords']['value']);
    View::share('description', $generalConfig['description']['value']);
    View::share('copyright', $generalConfig['copyright']['value']);
    View::share('analytics', $generalConfig['analytics']['value']);
    View::share('facebook', $generalConfig['facebook']['value']);
    View::share('twitter', $generalConfig['twitter']['value']);
    View::share('google', $generalConfig['google']['value']);
    View::share('site_favicon', asset('uploads/general/'.$generalConfig['site_favicon']['value']));
    View::share('site_logo', asset('uploads/general/'.$generalConfig['site_logo']['value']));
    View::share('home_background', asset('uploads/general/'.$generalConfig['home_background']['value']));
    View::share('home_text', $generalConfig['home_text']['value']);
    View::share('total_jobs', Jobs::all()->count());

    $this->theme       = 'themes/'.$generalConfig['theme_selected']['value'];
    View::share('theme',$this->theme);
    $this->logged_user = Auth::user();

    $favs = [];
    $applied_jobs = [];
    if ($this->logged_user){
      if(!$this->logged_user->favs->isEmpty()){
        foreach($this->logged_user->favs as $fv) {
          $favs[] = $fv->id;
        }
      }
      if(!$this->logged_user->jobs->isEmpty())  {
        foreach ($this->logged_user->jobs as $job) {
          $applied_jobs[] = $job->id;
        }
      }
      View::share('logged_user_favs', $favs);
      View::share('logged_user_jobs', $applied_jobs);
    }

    View::share('logged_user', $this->logged_user);
    View::share('g_categories',Category::all());

    View::share('g_job_types', $this->getJobTypes()['types']);
    View::share('g_job_types_total', $this->getJobTypes()['totalTypes']);

    View::share('g_locations_total',$this->getCities()['totalCities']);
    View::share('g_locations', $this->getCities()['location']);

    if($this->logged_user){
      if($this->logged_user->company != null){
        View::share('is_company', true);
        $this->is_company = true;
      } else{
        View::share('is_company', false);
        $this->is_company = false;
      }
    } else {
      View::share('is_company', false);
      $this->is_company = false;
    }

    // Test Company & User
    $currentRoute = $route->getPrefix();
    if ('/profile' == $currentRoute && $this->is_company){
      return redirect('company')->send();
    }
    if ('/company' == $currentRoute && !$this->is_company){
      return redirect('profile')->send();
    }


  }

  public function loader(){
    return View::make($this->theme.'.modules.loader');
  }

  public function setGeneralConfig(){
    $rv = [];
    $config = Config::all();

    foreach($config as $cfg){
      $rv[$cfg->name]['name']  = $cfg->name;
      $rv[$cfg->name]['value'] = $cfg->value;
    }
    return $rv;
  }

  public function getJobTypes(){
    $rv = [];
    $jobs = Jobs::where('status',1)->get();
    $lastId = [];
    $totalTypes = 0;
    foreach($jobs as $job) {
      if (!in_array($job->types->id,$lastId)) {

        $types[] = collect([
            'id' => $job->types->id,
            'name' => $job->types->name,
            'amount' => Jobs::where('job_type',$job->types->id)->get()->count()
        ]);
        $lastId[] = $job->types->id;
        $totalTypes = $totalTypes + Jobs::where('job_type',$job->types->id)->get()->count();
      }
    }
    $rv['types'] = $types;
    $rv['totalTypes'] = $totalTypes;
    return $rv;
  }

  public function getCities(){
    $rv = [];
    $jobs = Jobs::where('status',1)->get();
    $lastId = [];
    $totalCities = 0;
    foreach($jobs as $job) {
      if ($job->locations != null){
        if (!in_array($job->locations->id,$lastId)) {

          $location[] = collect([
              'id' => $job->locations->id,
              'state' => $job->locations->state,
              'city' => $job->locations->city,
              'amount' => Jobs::where('location_id',$job->locations->id)->get()->count()
          ]);
          $lastId[] = $job->locations->id;
          $totalCities = $totalCities + Jobs::where('location_id',$job->locations->id)->get()->count();
        }
      }
    }
    $rv['location'] = $location;
    $rv['totalCities'] = $totalCities;
    return $rv;
  }


}
