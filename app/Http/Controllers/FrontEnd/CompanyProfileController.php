<?php namespace App\Http\Controllers\FrontEnd;

use App\Company;
use App\Http\Requests;
use App\Http\Controllers\Controller;


use App\Jobs;
use App\User;
use App\UserBilling;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Inacho\CreditCard;

class CompanyProfileController extends BaseController {

  public function check(){
    $user     = Auth::user();
    $company  = $user->company;

    $jobs = Company::find($company->id)->jobs;
    return $jobs;
  }

  public function index(){
    return View::make($this->theme.'.company_profile.index');
  }

  public function manage_jobs()
  {
    return View::make($this->theme.'.company_profile.manage_jobs')->with('jobs',$this->check());
  }


  public function saved_drafts(){
    return View::make($this->theme.'.company_profile.drafts')->with('jobs',$this->check());
  }

  public function updateProfile(Request $request){

    $user     = User::find($this->logged_user->id);
    $company  = Company::find($user->company->id);


    $user->country_id       = $request->input('country_id');
    $user->address          = $request->input('address');
    $user->state            = $request->input('state');
    $user->zip_code         = $request->input('zip_code');
    $user->bussiness_phone  = $request->input('bussiness_phone');
    $user->mobile_phone     = $request->input('mobile_phone');
    $user->website          = $request->input('website');
    //$user->email            = $request->input('email');

    $user->save();

    $company->name        = $request->input('company_name');
    $company->description = $request->input('the_company');
    $company->save();


    $AuthNetAddShipping = app('App\Http\Controllers\FrontEnd\AuthNetController')
        ->checkCustomerAddress($company->merchant_id,$user->name,$company->name,$user->address,$user->state,$user->zip_code,$user->country_id,$user->bussiness_phone);

    if ($AuthNetAddShipping){
      return redirect('company')->with('message', 'Your profile has been updated!');
    } else {
      dump($AuthNetAddShipping);
    }
  }

  public function updateLogo(Request $request){
    $company = User::find($this->logged_user->id);
    $file = Input::all()['file'];
    if ($file != null){
      $fileName = md5($file->getClientOriginalName()) . '.' .$file->getClientOriginalExtension();
      $file->move(base_path() . '/public/uploads/company_logo/'.$company->id.'/', $fileName);
      $company->avatar = $fileName;
      $company->save();

      return json_encode(['filePath'=>asset('uploads/company_logo/'.$company->id.'/'.$fileName)]);
    }
  }

  public function job_applications(){
    return View::make($this->theme.'.company_profile.job_applications')->with('jobs',$this->check());
  }

  public function job_analytics(){
    $jobs = $this->check();
    $actualMonth =  date('M');

    $analyticsTotal = array(
        'Jan' => array('views'=>0,'applications'=>0),
        'Feb' => array('views'=>0,'applications'=>0),
        'Mar' => array('views'=>0,'applications'=>0),
        'Apr' => array('views'=>0,'applications'=>0),
        'May' => array('views'=>0,'applications'=>0),
        'Jun' => array('views'=>0,'applications'=>0),
        'Jul' => array('views'=>0,'applications'=>0),
        'Aug' => array('views'=>0,'applications'=>0),
        'Sep' => array('views'=>0,'applications'=>0),
        'Oct' => array('views'=>0,'applications'=>0),
        'Nov' => array('views'=>0,'applications'=>0),
        'Dec' => array('views'=>0,'applications'=>0)
    );

    $analyticsMonth = array(
        '1-10'=>array('views'=>0,'applications'=>0),
        '11-20'=>array('views'=>0,'applications'=>0),
        '21-31'=>array('views'=>0,'applications'=>0)
    );

    foreach($jobs as $j){
      foreach($j->getAnalytics as $analytics){
        if ($analytics->type == 0){
          $analyticsTotal[date_format($analytics->created_at, 'M')]['views']++;
          if (date_format($analytics->created_at, 'M') == $actualMonth){
            if (date_format($analytics->created_at, 'j') < 11){
              $analyticsMonth['1-10']['views']++;
            }
            if (date_format($analytics->created_at, 'j') > 10 && date_format($analytics->created_at, 'j') < 21){
              $analyticsMonth['11-20']['views']++;
            }
            if (date_format($analytics->created_at, 'j') > 20){
              $analyticsMonth['21-31']['views']++;
            }

          }
        }
        if ($analytics->type == 1){
          $analyticsTotal[date_format($analytics->created_at, 'M')]['applications']++;
          if (date_format($analytics->created_at, 'M') == $actualMonth){

            if (date_format($analytics->created_at, 'j') < 11){
              $analyticsMonth['1-10']['applications']++;
            }
            if (date_format($analytics->created_at, 'j') > 10 && date_format($analytics->created_at, 'j') < 21){
              $analyticsMonth['11-20']['applications']++;
            }
            if (date_format($analytics->created_at, 'j') > 20){
              $analyticsMonth['21-31']['applications']++;
            }
          }
        }
      }
    }

    $totalsGraph = [];
    $viewsGraph = [];
    $applicationsGraph = [];
    $totalsNumber = 0;
    $viewsNumber = 0;
    $applicationsNumber = 0;
    foreach($analyticsTotal as $data => $v){
      $totalsGraph[$data] = $analyticsTotal[$data]['views']+$analyticsTotal[$data]['applications'];
      $viewsGraph[$data] = $analyticsTotal[$data]['views'];
      $applicationsGraph[$data] = $analyticsTotal[$data]['applications'];

      $totalsNumber = $totalsNumber + ($v['views']+$v['applications']);
      $viewsNumber = $viewsNumber + $v['views'];
      $applicationsNumber = $applicationsNumber + $v['applications'];
    }

    return View::make($this->theme.'.company_profile.job_analytics')
        ->with('analyticsTotal',$analyticsTotal)
        ->with('analyticsMonth',$analyticsMonth)
        ->with('totalsGraph', $totalsGraph)
        ->with('viewsGraph', $viewsGraph)
        ->with('applicationsGraph',$applicationsGraph)
        ->with('totalsNumber', $totalsNumber)
        ->with('viewsNumber',$viewsNumber)
        ->with('applicationsNumber',$applicationsNumber)
        ->with('currentMonth',$actualMonth);

  }

  public function billingInformation(){
    $user     = Auth::user();
    $company  = $user->company;
    $billing = app('App\Http\Controllers\FrontEnd\AuthNetController')->getCustomerProfile($company->merchant_id);

    $billingData = parseBilling($billing);

    return View::make($this->theme.'.company_profile.billing')->with('billing', $billingData);
  }


  public function addBillingInfo(Request $request){

    $this->validateData($request);

    $billing = app('App\Http\Controllers\FrontEnd\AuthNetController')->createCustomerBillingAddress($request);
    if ($billing){
      return redirect()->back()->with('message','Your Credit Card information has been added to your billing information');
    }
  }

  public function updateBillingInfo(Request $request,$id){

    $this->validateData($request);

    $billing = app('App\Http\Controllers\FrontEnd\AuthNetController')->updateCustomerBillingAddress($request,$id);
    if ($billing){
      return redirect()->back()->with('message','Your Credit Card information has been updated');
    }
  }

  public function destroyBillingInfo($id){
    $billing = app('App\Http\Controllers\FrontEnd\AuthNetController')->deteleCustomerBillingProfile($id);
    if ($billing){
      return redirect()->back()->with('message','Your Credit Card information has been updated');
    }
  }

  public function validateData($request){
    $validator = Validator::make($request->all(), [
        'firstName' => 'required',
        'lastName' => 'required',
        'company' => 'required',
        'address' => 'required',
        'city' => 'required',
        'state' => 'required',
        'zip' => 'required|numeric',
        'country' => 'required',
        'phoneNumber' => 'required|numeric'
    ]);

    if ($validator->fails()) {
      redirect()->back()
          ->with('message',$validator)
          ->withInput();
    }

    // Validate Credit Card
    $card = CreditCard::validCreditCard($request->input('cardNumber'));

    $expirationYear = '20'.$request->input('expirationYear');
    $expirationMonth = $request->input('expirationMonth');
    $validDate = CreditCard::validDate($expirationYear, $expirationMonth);

    if ($card['valid']){
      $validCvc = CreditCard::validCvc($request->input('cardCode'), $card['type']);
      if ($validCvc && !getenv('AUTHORIZENET_SANDBOX')){
        return redirect()->back()
            ->with('message','The CVC Code enterd is not valid for this credit card.')
            ->withInput();
      }
    }

    if ($card['valid'] == false){
      return redirect()->back()
          ->with('message','Please enter a valid Credit Card.')
          ->withInput();

    }

    if (!$validDate){
      return redirect()->back()
          ->with('message','The Expiration Date is not valid.')
          ->withInput();
    }

  }


}
