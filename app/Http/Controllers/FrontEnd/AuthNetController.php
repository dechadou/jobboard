<?php namespace App\Http\Controllers\FrontEnd;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Queue\Jobs\Job;
use Illuminate\Support\Facades\View;

class AuthNetController extends BaseController {


  public function getCustomerProfile($merchantId){
    $request = new \AuthorizeNetCIM();
    $response = $request->getCustomerProfile($merchantId);
    if ($response->isOk()){
      return json_decode(json_encode($response->xml->profile),true);
    } else {
      return $response->getErrorMessage();
    }
  }


  public function createCustomer($merchantId,$email){
    $request = new \AuthorizeNetCIM();
    $customerProfile                     = new \AuthorizeNetCustomer();
    $customerProfile->merchantCustomerId = $merchantId;
    $customerProfile->email              = $email;
    $response = $request->createCustomerProfile($customerProfile);
    return ($response->isOk()) ? $response->getCustomerProfileId() : $response->getErrorMessage();

  }

  public function checkCustomerAddress($merchantId,$firstName,$company,$address,$state,$zip_code,$country,$phoneNumber){

    $customerProfile = $this->getCustomerProfile($merchantId);
    if (isset($customerProfile['shipToList'])){
      $shipAddressId = $customerProfile['shipToList']['customerAddressId'];
      return $this->updateCustomerAddress($merchantId,$shipAddressId,$firstName,$company,$address,$state,$zip_code,$country,$phoneNumber);
    } else {
      return $this->addCustomerAddress($merchantId,$firstName,$company,$address,$state,$zip_code,$country,$phoneNumber);
    }
  }

  public function addCustomerAddress($merchantId,$firstName,$company,$address,$state,$zip_code,$country,$phoneNumber){
    $request = new \AuthorizeNetCIM();

    $customerProfile                = new \AuthorizeNetAddress();
    $customerProfile->firstName     = $firstName;
    $customerProfile->company       = $company;
    $customerProfile->address       = $address;
    $customerProfile->state         = $state;
    $customerProfile->zip           = $zip_code;
    $customerProfile->country       = $country;
    $customerProfile->phoneNumber   = $phoneNumber;

    $response = $request->createCustomerShippingAddress($merchantId,$customerProfile);

    return ($response->isOk()) ? true : $response->getErrorMessage();
  }

  public function updateCustomerAddress($merchantId,$shipAddressId,$firstName,$company,$address,$state,$zip_code,$country,$phoneNumber){
    $request = new \AuthorizeNetCIM();

    $shippingAddress                = new \AuthorizeNetAddress();
    $shippingAddress->firstName     = $firstName;
    $shippingAddress->company       = $company;
    $shippingAddress->address       = $address;
    $shippingAddress->state         = $state;
    $shippingAddress->zip           = $zip_code;
    $shippingAddress->country       = $country;
    $shippingAddress->phoneNumber   = $phoneNumber;

    $response = $request->updateCustomerShippingAddress($merchantId, $shipAddressId, $shippingAddress);
    return ($response->isOk()) ? true : $response->getErrorMessage();
  }

  public function createCustomerBillingAddress($data){

    $getCustomerProfile = $this->getCustomerProfile($this->logged_user->company->merchant_id);

    $request = new \AuthorizeNetCIM();

    $paymentProfile = new \AuthorizeNetPaymentProfile();
    $paymentProfile->customerType                 = 'business';

    $paymentProfile->billTo->address              = $data->input('address');
    $paymentProfile->billTo->city                 = $data->input('city');
    $paymentProfile->billTo->company              = $data->input('company');
    $paymentProfile->billTo->country              = $data->input('country');
    $paymentProfile->billTo->faxNumber            = $data->input('faxNumber');
    $paymentProfile->billTo->firstName            = $data->input('firstName');
    $paymentProfile->billTo->lastName             = $data->input('lastName');
    $paymentProfile->billTo->phoneNumber          = $data->input('phoneNumber');
    $paymentProfile->billTo->state                = $data->input('state');
    $paymentProfile->billTo->zip                  = $data->input('zip');

    $paymentProfile->payment->creditCard->cardNumber        = $data->input('cardNumber');
    $paymentProfile->payment->creditCard->expirationDate    = $data->input('expirationMonth').$data->input('expirationYear');
    $paymentProfile->payment->creditCard->cardCode          = $data->input('cardCode');


    $response = $request->createCustomerPaymentProfile($getCustomerProfile['customerProfileId'], $paymentProfile, $validationMode = "testMode");
    return ($response->isOk()) ? true : $response->getErrorMessage();

  }

  public function updateCustomerBillingAddress($data, $billing_id){

    $getCustomerProfile = $this->getCustomerProfile($this->logged_user->company->merchant_id);

    $request = new \AuthorizeNetCIM();

    $paymentProfile = new \AuthorizeNetPaymentProfile();
    $paymentProfile->customerType                 = 'business';

    $paymentProfile->billTo->address              = $data->input('address');
    $paymentProfile->billTo->city                 = $data->input('city');
    $paymentProfile->billTo->company              = $data->input('company');
    $paymentProfile->billTo->country              = $data->input('country');
    $paymentProfile->billTo->faxNumber            = $data->input('faxNumber');
    $paymentProfile->billTo->firstName            = $data->input('firstName');
    $paymentProfile->billTo->lastName             = $data->input('lastName');
    $paymentProfile->billTo->phoneNumber          = $data->input('phoneNumber');
    $paymentProfile->billTo->state                = $data->input('state');
    $paymentProfile->billTo->zip                  = $data->input('zip');

    $paymentProfile->payment->creditCard->cardNumber        = $data->input('cardNumber');
    $paymentProfile->payment->creditCard->expirationDate    = $data->input('expirationMonth').$data->input('expirationYear');
    $paymentProfile->payment->creditCard->cardCode          = $data->input('cardCode');

    $paymentProfile->customerPaymentProfileId     = $billing_id;


    $response = $request->updateCustomerPaymentProfile($getCustomerProfile['customerProfileId'], $paymentProfile->customerPaymentProfileId, $paymentProfile, $validationMode = "testMode");
    return ($response->isOk()) ? true : $response->getErrorMessage();
  }

  public function deteleCustomerBillingProfile($id){
    $getCustomerProfile = $this->getCustomerProfile($this->logged_user->company->merchant_id);

    $request = new \AuthorizeNetCIM();
    $response = $request->deleteCustomerPaymentProfile($getCustomerProfile['customerProfileId'], $id);

    return ($response->isOk()) ? true : $response->getErrorMessage();
  }

  public function makeTransaction($paymentProfileId,/*$customerAddressId,*/ $transactionData){
    $getCustomerProfile = $this->getCustomerProfile($this->logged_user->company->merchant_id);

    $request = new \AuthorizeNetCIM();

    $transaction = new \AuthorizeNetTransaction;
    $transaction->amount = $transactionData['amount']; //Todo: CANT BE 0! do something if the redeem code makes it 0!
    $transaction->customerProfileId = $getCustomerProfile['customerProfileId'];
    $transaction->customerPaymentProfileId = (string)$paymentProfileId;
    //$transaction->customerShippingAddressId = "";


    $lineItem              = new \AuthorizeNetLineItem;
    $lineItem->itemId      = $transactionData['itemId'];
    $lineItem->name        = $transactionData['itemName'];
    $lineItem->description = $transactionData['itemDescription'];
    $lineItem->quantity    = $transactionData['itemQuantity'];
    $lineItem->unitPrice   = $transactionData['itemPrice'];
    $lineItem->taxable     = "false";


    $transaction->lineItems[] = $lineItem;


    $response = $request->createCustomerProfileTransaction("AuthCapture", $transaction);
    $transactionResponse = $response->getTransactionResponse();
    $transactionId = $transactionResponse->transaction_id;
    return $transactionResponse;
    //return $transactionId; // if return 0 it means its from SandBox
  }

}