<?php namespace App\Http\Controllers\FrontEnd;

use App\Country;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class CountryController extends BaseController {

	public function find(Request $request){
    $rq = $request->get('query');
    $countries = Country::where('name','like','%'.$rq.'%')->get();
    $in = array(
        "suggestions" => []
    );
    foreach($countries as $c){
      $in['suggestions'][] = array('value' =>$c->name, 'data' => $c->id);

    }
    return Response::json($in);
  }

  public function findLocation(Request $request){
    $rq = $request->get('query');
    $locations = Location::where('state','like','%'.$rq.'%')->orWhere('city','like','%'.$rq.'%')->get();
    $in = array(
        "suggestions" => []
    );
    foreach($locations as $c){
      $in['suggestions'][] = array('value' =>$c->state.', '.$c->city, 'data' => $c->id);

    }
    return Response::json($in);
  }
}
