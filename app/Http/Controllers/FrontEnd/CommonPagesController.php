<?php namespace App\Http\Controllers\FrontEnd;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Jobs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class CommonPagesController extends BaseController {

  public function employer($employer_name){
    $rv = [];
    $jobs = Jobs::where('status',1)->get();
    foreach($jobs as $job){
      if (strtolower($job->companies->name) == strtolower($employer_name)){
        $rv[] = $job;
      }
    }

    return View::make($this->theme.'.employer.index')
        ->with('jobs',collect($rv))
        ->with('employer_name',$employer_name)
        ->with('search_totals', count($rv));
  }

  public function jobTypes($type){
    $jobTypes = ['full-time'=>0,'part-time'=>1,'freelance'=>2];
    $rv = [];
    $jobs = Jobs::where('status',1)->get();
    foreach ($jobs as $job){
      if ($jobTypes[$type] == $job->job_type){
       $rv[] = $job;
      }
    }

    return View::make($this->theme.'.jobs.types')
        ->with('jobs', collect($rv))
        ->with('type', $type)
        ->with('search_totals', collect($rv)->count());

  }

}