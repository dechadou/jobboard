<?php namespace App\Http\Controllers\FrontEnd;

use App\Category;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\JobAnalytics;
use App\Jobs;
use App\JobType;
use App\TokenGenerator;
use App\User;
use App\UserAlerts;
use App\UserFavs;
use App\UserFiles;
use App\UserJob;
use App\Company;
use App\Http\Requests\ProfileFormRequest;
use App\Http\Requests\FilesFormRequest;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use GeoIP;


class ProfileController extends BaseController {

  public function index()
  {
    $userJobs = $this->logged_user->jobs()->get();
    return View::make($this->theme.'.profile.applied_jobs')->with('jobs',$userJobs);
  }

  public function favourite(){
    $userFavorite = $this->logged_user->favs()->get();
    return View::make($this->theme.'.profile.favourite_jobs')->with('jobs',$userFavorite);
  }

  public function preferences(){
    $jobTypes = JobType::all();
    $categories = Category::all();
    $userData = [];
    $userAlerts = $this->logged_user->alerts;
    $userData['job_types'] = [];
    $userData['categories'] = [];
    foreach($userAlerts as $alerts){
      if ($alerts->job_type_id != null){
        $userData['job_types'][] = $alerts->job_type_id;
      }
      if ($alerts->category_id != null){
        $userData['categories'][] = $alerts->category_id;
      }
    }
    return View::make($this->theme.'.profile.preferences')
        ->with('job_types', $jobTypes)
        ->with('categories', $categories)
        ->with('userData', $userData);
  }

  public function updateAlerts(Request $request){
    $jobType = $request->input('job_type');
    $categories = $request->input('categories');

    //detach all

    $currentAlerts = UserAlerts::where('user_id', $this->logged_user->id)->delete();


    foreach($jobType as $key => $value){
      $userAlerts = new UserAlerts();
      $userAlerts->user_id = $this->logged_user->id;
      $userAlerts->job_type_id = $value;
      $userAlerts->category_id = null;
      $userAlerts->save();
    }

    foreach($categories as $key => $value){
      $userAlerts = new UserAlerts();
      $userAlerts->user_id = $this->logged_user->id;
      $userAlerts->job_type_id = null;
      $userAlerts->category_id = $value;
      $userAlerts->save();
    }

    $jobTypes = JobType::all();
    $categories = Category::all();
    $userData['job_types'] = [];
    $userData['categories'] = [];
    $userAlerts = $this->logged_user->alerts;
    foreach($userAlerts as $alerts){
      if ($alerts->job_type_id != null){
        $userData['job_types'][] = $alerts->job_type_id;
      }
      if ($alerts->category_id != null){
        $userData['categories'][] = $alerts->category_id;
      }
    }
    return View::make($this->theme.'.profile.preferences')
        ->with('job_types', $jobTypes)
        ->with('categories', $categories)
        ->with('userData', $userData);
  }

  public function files(){
    $userFiles = $this->logged_user->files()->get();
    return View::make($this->theme.'.profile.files')->with('files',$userFiles);
  }

  public function info(){
    return View::make($this->theme.'.profile.info');
  }

  public function update(ProfileFormRequest $request)
  {
    $id = $this->logged_user->id;
    $user = User::find($id);

    $file = $request->file('avatar');
    if ($file != null){
      $imageName = new TokenGenerator(). '.' .$file->getClientOriginalExtension();
      $file->move(base_path() . '/public/uploads/user_avatar/'.$id.'/', $imageName);
      $user->avatar = $imageName;
    }
    $user->save();
    return View::make($this->theme.'.base');
  }

  public function addFiles(FilesFormRequest $request){
    $id = $this->logged_user->id;
    $user_files = new UserFiles;

    $file = $request->file('file');

    if ($file != null){
      $fileName = new TokenGenerator(). '.' .$file->getClientOriginalExtension();
      // Check if dir is writeable
      //chmod(base_path() . '/public/uploads/user_files/'.$id.'/', 0777);
      $file->move(base_path() . '/public/uploads/user_files/'.$id.'/', $fileName);
      // Change back to 755
      //chmod(base_path() . '/public/uploads/user_files/'.$id.'/', 0755);
      $user_files->user_id = $id;
      $user_files->file_name = $file->getClientOriginalName();
      $user_files->file = $fileName;
    }

    $user_files->save();

    return redirect('/profile/files')->with('message','Your file has been uploaded');
  }

  public function removeFiles($file_id){
    unlink('/'.public_path().'/uploads/user_files/'.$this->logged_user->id.'/'.UserFiles::find($file_id)->file);
    UserFiles::find($file_id)->delete();
    return redirect('/profile/files')->with('message','Your file has been deleted');
  }

  public function addJob($job_id, Request $request){
    $this->saveAnalytics($job_id,1);

    if ($this->logged_user){
      $user_files = $request->input('user_files');
      $user_comments = $request->input('comments');

      $this->logged_user->jobs()->attach($job_id, array('selected_files'=>$user_files, 'user_comments'=>$user_comments));
      return redirect()->back()->with('message','You have applied for the job!');
    } else {
      $name   = $request->input('unlogged_name');
      $email  = $request->input('unlogged_email');
      $tellus = $request->input('unlogged_tell_us');
      $file   = $request->file('unlogged_file');

      $v = Validator::make($request->all(), [
          'unlogged_name' => 'required|max:100',
          'unlogged_email' => 'required|email'
      ]);

      if ($v->fails()) {
        return redirect()->back()->withErrors($v->errors())->withInput();
      }

      $query = User::where('email', $email)->get();
      if (!$query->isEmpty()) {
        return redirect()->back()->withErrors('That email is already registerd. Did you forget your password?')->withInput();
      }

      $user_data = [
          'firstName' => $name,
          'emailAddress' => $email,
          'password' => Hash::make($email),
          'country_id' => 840,
          'auth_provider' => 'hoojobs',
          'avatar' => false
      ];
      $user = app('App\Http\Controllers\FrontEnd\LoginController')->createUser($user_data, 'candidate', true);

      $user_files = new UserFiles();
      if ($file != null){
        $fileName = new TokenGenerator(). '.' .$file->getClientOriginalExtension();
        $file->move(base_path() . '/public/uploads/user_files/'.$user->id.'/', $fileName);
        $user_files->user_id = $user->id;
        $user_files->file_name = $file->getClientOriginalName();
        $user_files->file = $fileName;
      }
      $user_files->save();

      $user->jobs()->attach($job_id, array('selected_files'=>$user_files->id, 'user_comments'=>$tellus));


      $data['password'] = $user_data['password'];
      $data['email'] = $email;
      $data['name'] = $name;
      Mail::send('emails.welcome', $data, function($message) use($data)
      {
        $message->to($data['email'], $data['name'])->subject('[HooJobs] - Welcome to Hoojobs!');
      });

      return redirect()->back()->with('message','You have applied for the job!');
    }
  }

  public function addFav($job_id){
    $exist = $this->logged_user->favs()->find($job_id);
    if ($exist == null){
      Auth::user()->favs()->attach($job_id);
      return response(array('message' => 'The Job has been added to your favourites'), 200)->header('Content-Type', 'application/json');
    } else {
      return response(array('message' => 'You already added this job as a favourite'), 401)->header('Content-Type', 'application/json');
    }
  }

  public function removeFav($id){
    $exist = $this->logged_user->favs()->find($id);

    if ($exist == null){
      return response(array('message' => 'You cannot remove a job from favourite if it was not added first!'), 401)->header('Content-Type', 'application/json');
    } else {
      Auth::user()->favs()->detach($id);
      return response(array('message' => 'The Job has been removed to your favourites'), 200)->header('Content-Type', 'application/json');
   }
  }

  public function addFilesFromDropbox(Request $request){
    $id = $this->logged_user->id;
    $user = User::find($id);
    $user_files = new UserFiles;
    $file = file_get_contents($request->input('file'));
    if ($file != null){

      $extension = pathinfo($request->input('file'), PATHINFO_EXTENSION);
      $originalName = pathinfo($request->input('file'), PATHINFO_FILENAME).'.'.$extension;
      $fileName = new TokenGenerator().'.'.$extension;


      // Creo el directorio si no existe
      if (!is_dir(base_path() . '/public/uploads/user_files/'.$id)) {
        mkdir(base_path() . '/public/uploads/user_files/'.$id);
      }
      $save = file_put_contents(base_path() . '/public/uploads/user_files/'.$id.'/'. $fileName,$file);
      if ($save){
        $user_files->user_id = $id;
        $user_files->file_name = $originalName;
        $user_files->file = $fileName;
        $user->files()->save($user_files);
      }
    }

    if ($request->input('return_path')){
      return json_encode(['id'=>$user_files->id, 'filePath'=>asset('uploads/user_files/'.$id.'/'.$fileName), 'fileName'=>$originalName]);
    }
    return redirect('/profile/files')->with('message','Your file has been uploaded');
  }

  public function addFilesFromHD(Request $request){
    $id = $this->logged_user->id;
    $user = User::find($id);
    $user_files = new UserFiles;

    $file = Input::all()['file'];
    if ($file != null){
      $fileName = new TokenGenerator() . '.' .$file->getClientOriginalExtension();
      // Check if dir is writeable
      //chmod(base_path() . '/public/uploads/user_files/'.$id.'/', 0777);
      $file->move(base_path() . '/public/uploads/user_files/'.$id.'/', $fileName);
      // Change back to 755
      //chmod(base_path() . '/public/uploads/user_files/'.$id.'/', 0755);
      $user_files->user_id = $id;
      $user_files->file_name = $file->getClientOriginalName();
      $user_files->file = $fileName;
      $user->files()->save($user_files);
      return json_encode(['id'=>$user_files->id, 'filePath'=>asset('uploads/user_files/'.$id.'/'.$fileName), 'fileName'=>$file->getClientOriginalName()]);
    }
  return false;
  }

  public function updateProfile(Request $request){

    $user     = User::find($this->logged_user->id);


    $user->country_id       = $request->input('country_id');
    $user->address          = $request->input('address');
    $user->state            = $request->input('state');
    $user->zip_code         = $request->input('zip_code');
    $user->bussiness_phone  = $request->input('bussiness_phone');
    $user->mobile_phone     = $request->input('mobile_phone');
    $user->website          = $request->input('website');
    //$user->email            = $request->input('email');

    $user->save();


    return redirect('profile/info')->with('message', 'Your profile has been updated!');


  }

  public function updateAvatar(Request $request){
    $user = User::find($this->logged_user->id);

    $file = Input::all()['file'];
    if ($file != null){
      $fileName = new TokenGenerator(). '.' .$file->getClientOriginalExtension();
      $file->move(base_path() . '/public/uploads/user_avatar/'.$user->id.'/', $fileName);
      $user->avatar = $fileName;
      $user->save();

      return json_encode(['filePath'=>asset('uploads/user_avatar/'.$user->id.'/'.$fileName)]);
    }
  }

  public function saveAnalytics($id,$type){
    $analytics = new JobAnalytics();
    $analytics->job_id = $id;
    $analytics->type = $type; // 0 = view; 1 = submit
    $analytics->ip = GeoIP::getLocation()['ip'];
    $analytics->location = GeoIP::getLocation()['country'];
    $analytics->save();
  }

}
