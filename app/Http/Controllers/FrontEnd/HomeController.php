<?php namespace App\Http\Controllers\FrontEnd;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Jobs;
use App\User;
use App\UserJob;
use App\UserFavs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class HomeController extends BaseController {

  public function index(){
    $jobs = Jobs::getJobList();
    $total_jobs = 0;
    if (!$jobs->isEmpty()){
      $total_jobs = Jobs::getTotalJobs();
    }
    return View::make($this->theme.'.home.list')
        ->with('jobs',$jobs)
        ->with('total_jobs',$total_jobs);
  }
}
