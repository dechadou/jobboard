<?php namespace App\Http\Controllers\FrontEnd;

use App\Coupons;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Industry;
use App\JobAnalytics;
use App\Jobs;
use App\Company;
use App\Category;
use App\JobType;
use App\Location;
use App\TokenGenerator;
use App\Transaction;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Queue\Jobs\Job;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use GeoIP;

class JobsController extends BaseController {

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $favs = [];
    $applied_jobs = [];

    if(!$this->logged_user->favs->isEmpty()){
      foreach($this->logged_user->favs as $fv) {
        $favs[] = $fv->id;
      }
    }

    if(!$this->logged_user->jobs->isEmpty()) {
      foreach ($this->logged_user->jobs as $job) {
        $applied_jobs[] = $job->id;
      }
    }
    return View::make($this->theme.'.jobs.list')
        ->with('jobs', Jobs::all())
        ->with('favorite_jobs', $favs)
        ->with('applied_jobs', $applied_jobs);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    /*if ($this->is_company == null){
      return redirect('/login');
    }*/
    $company = null;
    if ($this->logged_user != null){
      if ($this->is_company) {
        $company = $this->logged_user->company;
      }
    }

    $geo = [];
    $locations = Location::all();
    foreach($locations as $location){
      $geo[$location->state][$location->id] = $location->city;
    }



    return View::make($this->theme.'.jobs.post')
        ->with('company', $company)
        ->with('jobTypes', JobType::all())
        ->with('industries', Industry::all())
        ->with('locations', $geo);
  }

  public function selectPlan(){
    return View::make($this->theme.'.jobs.plan');
  }

  public function redeemCoupon(){
    $coupon = Input::get('code');
    $valid = Coupons::where('token','=',$coupon)->where('status',1)->get()->first();
    $status =  ($valid == null) ? false : true;
    $value = 0;
    if ($status){
      switch($valid->disccount_type){
        case 0:
          // Money Discount
          $value = $valid->discount;
          break;
        case 1:
          // Percentage Discount
          $value = 299 - (($valid->discount * 299) / 100);
          break;
      }
    }

    return json_encode(array('status'=>$status,'coupon'=>$valid,'value'=>$value));
  }

  public function renewal($job_id){
    $user = Auth::user();
    if ($user == null || $job_id == ''){
      return redirect('/login');
    }
    $company  = $user->company;
    $billing = app('App\Http\Controllers\FrontEnd\AuthNetController')->getCustomerProfile($company->merchant_id);
    $billingData = parseBilling($billing);


    $job = Jobs::find($job_id);
    $isCopy = ($job->expiration_date == '0000-00-00 00:00:00') ? true : false;

    return View::make($this->theme.'.checkout.renewal')
        ->with('user',$user)
        ->with('billing',$billingData)
        ->with('job_id',$job_id)
        ->with('isCopy',$isCopy);

  }

  public function saveNowPostLater(){
    $pricing = \App\Pricing::where('id','>',5)->where('id','<',17)->get();

    $company  = $this->logged_user->company;
    $billing = app('App\Http\Controllers\FrontEnd\AuthNetController')->getCustomerProfile($company->merchant_id);
    $billingData = parseBilling($billing);

    return View::make($this->theme.'.checkout.save_now_post_later')
        ->with('pricing', $pricing)
        ->with('billing',$billingData);
  }

  public function saveNowPostLaterCheckout(Request $request){
    $post_amount = $request->input('post_amount');
    $price_final = null;
    $useNewCreditCard = ($request->input('add_creditcard')) ? $request->input('add_creditcard') : false;
    $paymentType      = $request->input('paymentType');

    if ($post_amount >= 11){
      $price_final =  \App\Pricing::where('id',16)->get()->first()->value * $post_amount;
    }
    $price_final = \App\Pricing::where('id',$post_amount+5)->get()->first()->value * $post_amount;

    if ($useNewCreditCard){
      $billing = app('App\Http\Controllers\FrontEnd\AuthNetController')->createCustomerBillingAddress($request);
      $paymentProfileId = app('App\Http\Controllers\FrontEnd\AuthNetController')->getCustomerProfile($this->logged_user->company->merchant_id)['paymentProfiles']['customerPaymentProfileId'];
    } else {
      $paymentProfileId = $request->input('paymentProfiles');
    }

    $transactionData = parseSaveNowPostLater($price_final);

    $billing = app('App\Http\Controllers\FrontEnd\AuthNetController')->makeTransaction($paymentProfileId, $transactionData);
    $error = false;
    if($billing->approved){
      $transactionData['transaction_id']      = $billing->transaction_id;
      $transactionData['paymentProfileId']    = $paymentProfileId;

      // generar cupones y enviarlos al usuario

      $transaction = new Transaction();
      $transaction->company_id = Auth::user()->company->id;
      $transaction->module_id = 1; // Authorize.Net
      $transaction->data = json_encode($transactionData);
      $transaction->save();


      $vouchers = [];
      for($i = 0; $i < $post_amount; $i++){
        $token = new TokenGenerator();
        $coupon = new Coupons();
        $coupon->description    = 'Auto Generated Save now post later for '.$this->logged_user->company->name;
        $coupon->token          = str_slug($this->logged_user->company->name).'_'.$token;
        $coupon->discount_type  = 2;
        $coupon->save();
        $vouchers[] = $coupon;
      }

      $data['vouchers'] = $vouchers;
      $data['email'] = $this->logged_user->email;
      $data['name'] = $this->logged_user->name;
      Mail::send('emails.savenowpostlater', $data, function($message) use($data)
      {
        $message->to($data['email'], $data['name'])->subject('[HooJobs] - Thank you for purchasing! Here are your coupons');
      });

      $status = true;
    }

    if ($billing->declined || $billing->error){
      $status = false;
      $error = $billing->response_reason_text;
    }

    if ($status){
      return View::make($this->theme.'.checkout.save_now_post_later_success')
          ->with('vouchers', $vouchers)
          ->with('post_amount', $post_amount);
    }

    return View::make($this->theme.'.checkout.error')->with('status',$status)->with('error',$error);
  }

  public function checkoutFeaturedStep2(Request $request){
    $useNewCreditCard = ($request->input('add_creditcard')) ? $request->input('add_creditcard') : false;
    $paymentType      = $request->input('paymentType');


    if ($useNewCreditCard){
      $billing = app('App\Http\Controllers\FrontEnd\AuthNetController')->createCustomerBillingAddress($request);
      $paymentProfileId = app('App\Http\Controllers\FrontEnd\AuthNetController')->getCustomerProfile($this->logged_user->company->merchant_id)['paymentProfiles']['customerPaymentProfileId'];
    } else {
      $paymentProfileId = $request->input('paymentProfiles');
    }


    $coupon           = (trim($request->input('coupon')) == '') ? false : $request->input('coupon_code');
    $price            = (trim($request->input('featured_selected')) == '') ? 0 : $request->input('featured_selected');


    $transactionData  = parseFeaturedTransaction($coupon,$price);


    $billing = app('App\Http\Controllers\FrontEnd\AuthNetController')->makeTransaction($paymentProfileId, $transactionData);

    $error = false;
    if($billing->approved){
      $transactionData['transaction_id']      = $billing->transaction_id;
      $transactionData['paymentProfileId']    = $paymentProfileId;

      $job = Jobs::find(json_decode($request->input('job_data'))->id);
      $job->featured = 1;
      $job->save();

      $transactionData['jobId'] = $job->id;
      $transaction = new Transaction();
      $transaction->company_id = Auth::user()->company->id;
      $transaction->module_id = 1; // Authorize.Net
      $transaction->data = json_encode($transactionData);
      $transaction->save();
      $status = true;
    }

    if ($billing->declined || $billing->error){
      $status = false;
      $error = $billing->response_reason_text;
    }

    if ($status){
      $user     = Auth::user();
      $company  = $user->company;
      $company->available_posts = $company->available_posts+1;
      $company->save();
      return $this->success($job);
    }

    return View::make($this->theme.'.checkout.error')->with('status',$status)->with('error',$error);
  }

  public function checkoutFeatured($job_id){
    $job = Jobs::find($job_id);
    $company  = $this->logged_user->company;
    $billing = app('App\Http\Controllers\FrontEnd\AuthNetController')->getCustomerProfile($company->merchant_id);
    $billingData = parseBilling($billing);

    return View::make($this->theme.'.checkout.featured_only')->with('billing',$billingData)->with('job', $job);
  }

  public function checkout(Request $request){

    // Save to Draft
    if ($request->input('job_status') == 0){
      return $this->store($request);
    }

    // User does not exist, create an account.
    if ($request->input('new_user') == 1){

      $v = Validator::make($request->all(), [
          'new_user_email' => 'required|email',
          'new_user_name' => 'required',
          'new_user_phone' => 'required'
      ]);

      if ($v->fails()) {
        return redirect()->back()->withErrors($v->errors())->withInput();;
      }
      $query = User::where('email', $request->input('new_user_email'))->get();
      if (!$query->isEmpty()) {
        return redirect()->back()->withErrors('That email is already registerd. Did you forget your password?')->withInput();;
      }

      $user_data = [
          'firstName' => $request->input('new_user_name'),
          'emailAddress' => $request->input('new_user_email'),
          'password' => Hash::make($request->input('new_user_email')),
          'country_id' => 840,
          'auth_provider' => 'hoojobs',
          'avatar' => false
      ];
      $user = app('App\Http\Controllers\FrontEnd\LoginController')->createUser($user_data, 'recruiter', true);

      $data['password'] = $user_data['password'];
      $data['email'] = $user_data['emailAddress'];
      $data['name'] = $user_data['firstName'];
      Mail::send('emails.welcome', $data, function($message) use($data)
      {
        $message->to($data['email'], $data['name'])->subject('[HooJobs] - Welcome to Hoojobs!');
      });

      $company = Company::where('user_id', $user->id)->get()->first();
      $company->name = $request->input('company_name');
      $company->description = $request->input('the_company');
      $company->save();

    }

    $v = Validator::make($request->all(), [
        'job_name'  => 'required',
        'the_job' => 'required',
        'job_type' => 'required',
        'categories' => 'required',
        'app_by_text' => 'required',
        'app_by' => 'required',
        'company_name' => 'required'
    ]);

    if ($v->fails()) {
      return redirect()->back()->withErrors($v->errors())->withInput();
    }

    $user = Auth::user();
    $company  = Auth::user()->company;
    $billing = app('App\Http\Controllers\FrontEnd\AuthNetController')->getCustomerProfile($company->merchant_id);
    $billingData = parseBilling($billing);


    return View::make($this->theme.'.checkout.pricing')->with('user',$user)->with('billing',$billingData)->with('job_data',json_encode($request->all()));
  }

  public function checkoutStep2(Request $request){
    $useNewCreditCard = ($request->input('add_creditcard')) ? $request->input('add_creditcard') : false;
    $paymentType      = $request->input('paymentType');


    if ($useNewCreditCard){
      $billing = app('App\Http\Controllers\FrontEnd\AuthNetController')->createCustomerBillingAddress($request);
      $paymentProfileId = app('App\Http\Controllers\FrontEnd\AuthNetController')->getCustomerProfile($this->logged_user->company->merchant_id)['paymentProfiles']['customerPaymentProfileId'];
    } else {
      $paymentProfileId = $request->input('paymentProfiles');
    }

    $coupon           = (trim($request->input('coupon')) == '') ? false : $request->input('coupon_code');
    $featured         = (trim($request->input('featured_selected')) == '') ? 0 : $request->input('featured_selected');
    $postCount        = $this->logged_user->company->available_posts;

    $transactionData  = parseTransaction(1, $coupon,$featured,$postCount,$paymentType);
    $billing = app('App\Http\Controllers\FrontEnd\AuthNetController')->makeTransaction($paymentProfileId, $transactionData);

    $error = false;
    if($billing->approved){
      $transactionData['transaction_id']      = $billing->transaction_id;
      $transactionData['paymentProfileId']    = $paymentProfileId;

      if ($paymentType == 'newJob'){
        $job = $this->storeParsed(json_decode($request->input('job_data')), $transactionData['featured']);
      } else if ($paymentType == 'renewal') {
        $job = Jobs::find($request->input('job_id'));
        $job->status = 1;
        $dateTime = new \DateTime;
        $dateTime->add(new \DateInterval("P1M"));
        $job->expiration_date = $dateTime->format('Y-m-d H:i:s');
        $job->save();
      }

      $transactionData['jobId'] = $job->id;
      $transaction = new Transaction();
      $transaction->company_id = Auth::user()->company->id;
      $transaction->module_id = 1; // Authorize.Net
      $transaction->data = json_encode($transactionData);
      $transaction->save();
      $status = true;
    }

    if ($billing->declined || $billing->error){
      $status = false;
      $error = $billing->response_reason_text;
    }

    if ($status){
      $user     = Auth::user();
      $company  = $user->company;
      $company->available_posts = $company->available_posts+1;
      $company->save();
      return $this->success($job);
    }

    return View::make($this->theme.'.checkout.error')->with('status',$status)->with('error',$error);
  }

  public function success($job){
    return View::make($this->theme.'.checkout.success')->with('job',$job);
  }

  public function bulkPosts(Request $request){
    $paymentProfileId = $request->input('paymentProfiles');
    $coupon = (trim($request->input('coupon')) == '') ? false : $request->input('coupon');
    $transactionData = parseTransaction($request->input('quantity'), $coupon);
    $billing = app('App\Http\Controllers\FrontEnd\AuthNetController')->makeTransaction($paymentProfileId, $transactionData);


    $status = true;
    if($billing->approved){
      $transactionData['transaction_id']      = $billing->transaction_id;
      $transactionData['paymentProfileId']    = $paymentProfileId;
      $transaction = new Transaction();
      $transaction->company_id = Auth::user()->company->id;
      $transaction->module_id = 1; // Authorize.Net
      $transaction->data = json_encode($transactionData);
      $transaction->save();
    }else{
      if ($billing->declined){
        $status = false;
      }
    }
    return View::make($this->theme.'.checkout.success')->with('status',$status);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function storeParsed($request, $featured=0)
  {


    $company = $this->logged_user->company;

    $company_data = Company::find($company->id);
    $company_data->name = $request->company_name;
    $company_data->save();


    $job = new Jobs();
    $job->title             = $request->job_name;
    $job->company_id        = $company->id;
    $job->the_job           = $request->the_job;
    $job->the_candidate     = $request->the_candidate;
    $job->location_id       = ($request->location_id != '') ? $request->location_id : null;
    $job->zip_code          = $request->zip_code;
    $job->job_type          = $request->job_type;
    $job->app_by            = $request->app_by;
    $job->app_by_text       = $request->app_by_text;
    if($request->industry_id == 'other'){
      $job->industry_other = $request->industry_other;
      $job->industry_id    = NULL;
    } else {
      $job->industry_id       = $request->industry_id;
    }

    $job->status            = ($request->job_status) ? 1 : 0;
    $job->featured          = $featured;

    // Set expiration date to 1 month plus
    $dateTime = new \DateTime;
    $dateTime->add(new \DateInterval("P1M"));
    $job->expiration_date = $dateTime->format('Y-m-d H:i:s');


    $job->save();

    /* add new categories */
    $categories = explode(',',implode(',',$request->categories));
    foreach($categories as $c){
      $job->categories()->attach($c+1);
    }

    return $job;

  }

  public function store($request, $featured=0)
  {

    $company = $this->logged_user->company;

    $job = new Jobs();
    $job->title             = $request->input('job_name');
    $job->company_id        = $company->id;
    $job->the_job           = $request->input('the_job');
    $job->the_candidate     = $request->input('the_candidate');
    $job->location_id       = ($request->input('location_id') != '') ? $request->input('location_id') : null;
    $job->zip_code          = $request->input('zip_code');
    $job->job_type          = $request->input('job_type');
    if($request->industry_id == 'other'){
      $job->industry_other = $request->input('industry_other');
      $job->industry_id    = NULL;
    } else {
      $job->industry_id       = $request->input('industry_id');
    }
    $job->app_by            = $request->input('app_by');
    $job->app_by_text       = $request->input('app_by_text');
    $job->status            = ($request->input('job_status')) ? 1 : 0;
    $job->featured          = $featured;


    $job->save();

    /* add new categories */
    $categories = explode(',',implode(',',$request->input('categories')));
    foreach($categories as $c){
      $job->categories()->attach($c+1);
    }

    if (!$job->status){
      return redirect('company/saved_drafts');
    }
    return redirect('company');

  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    $this->saveAnalytics($id,0);
    $job = Jobs::find($id);


    $partials = null;

    if (!$this->is_company && $job->status){
      if ($this->logged_user){
        $partials = ($job->app_by == 1) ? 'applicant_by_url' : 'applicant_by_email';
      } else {
        $partials = 'login_apply';
      }
    } else if ($this->logged_user->company->id == $job->company_id) {
      $partials = 'company_cardactions';
    } else {
      $partials = 'no-sidebar';
    }



    return View::make($this->theme.'.jobs.show')
        ->with('job', $job)
        ->with('partials', $partials)
        ->with('similar_jobs',$this->getSimilarJobs($id));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param Requests $request
   * @param  int $id
   * @return Response
   */
  public function update(Request $request, $id)
  {

    $v = Validator::make($request->all(), [
        'the_job' => 'required',
        'job_type' => 'required',
        'categories' => 'required',
        'app_by_text' => 'required',
        'app_by' => 'required',
    ]);

    if ($v->fails()) {
      return redirect()->back()->withErrors($v->errors())->withInput();
    }

    $job = Jobs::find($id);
    //$job->title             = $request->input('job_name');
    $job->the_job           = $request->input('the_job');
    $job->the_candidate     = $request->input('the_candidate');
    //$job->location_id       = $request->input('location');
    //$job->zip_code        = $request->input('zip_code');
    $job->job_type          = $request->input('job_type');
    if($request->industry_id == 'other'){
      $job->industry_other = $request->input('industry_other');
      $job->industry_id    = NULL;
    } else {
      $job->industry_id       = $request->input('industry_id');
    }
    $job->app_by            = $request->input('app_by');
    $job->app_by_text       = $request->input('app_by_text');
    $job->status            = ($request->input('job_status')) ? 1 : 0;
    $job->featured          = ($request->input('featured')) ? 1 : 0;


    $job->save();

    /* Delete all related categories */
    foreach($job->categories as $c){
      $detach_ids[] = $c->id;
    }
    $job->categories()->detach($detach_ids);
    /* add new categories */
    $categories = explode(',',implode(',',$request->input('categories')));
    foreach($categories as $c){
      $job->categories()->attach($c);
    }

    return redirect('company');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    $job = Jobs::find($id);
    $job->destroy($id);
    return redirect('company');
  }

  public function getSimilarJobs($id){
    return Jobs::SimilarJobs(Jobs::find($id));
  }

  public function saveAnalytics($id,$type){
    $analytics = new JobAnalytics();
    $analytics->job_id = $id;
    $analytics->type = $type; // 0 = view; 1 = submit
    $analytics->ip = GeoIP::getLocation()['ip'];
    $analytics->location = GeoIP::getLocation()['country'];
    $analytics->save();
  }

  public function loadMore($offset){
    $jobs =  Jobs::getJobList($offset);
    if(!$jobs->isEmpty()){
      foreach($jobs as $job){
        echo View::make($this->theme.'.modules.job_card')->with('job',$job);
      }
    } else {
      echo false;
    }
  }


}
