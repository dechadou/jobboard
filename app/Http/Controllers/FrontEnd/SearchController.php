<?php namespace App\Http\Controllers\FrontEnd;

use App\Category;
use App\Company;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Industry;
use App\Jobs;
use App\Location;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class SearchController extends BaseController {

  public function searchByIndustry($industry){
    if (!is_numeric($industry)){
      $search_results = Jobs::where('status', 1)->where('industry_other', $industry);
    } else {
      $search_results = Jobs::where('status',1)->where('industry_id', $industry);
    }
    $result = $search_results->take(10)->get();
    $search_totals = $search_results->count();
    $search_terms = array(
        'term' => '',
        'industry' => (is_numeric($industry)) ? Industry::find($industry)->name : $industry,
        'job_type' => ''
    );

    $results = array(
        'search_totals' => $search_totals,
        'search_results' => $result,
        'search_terms' => $search_terms
    );
    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      return response()->json(['status' => true, 'data' => $results], 200);
    } else {
      return View::make($this->theme.'.search.list_normal')
          ->with('search_totals', $search_totals)
          ->with('search_results', $result)
          ->with('search_terms',$search_terms);
    }
  }

  public function searchByCity($city){
    if ($city == 'all'){
      $search_results = Jobs::where('status', 1);
    } else {
      $search_results = Jobs::where('location_id', (int)$city);
      $location = Location::where('id', (int)$city)->get()->first();
      $allCitiesInState = Location::where('state',$location->state)->get();
      foreach($allCitiesInState as $states){
        $search_results->orWhere('location_id', $states->id);
      }
      $search_results->where('status', 1);
    }


    $result = $search_results->get();

    $search_totals = $search_results->count();

    $search_terms = array(
        'term' => '',
        'city' => ($city != 'all') ? Location::find($city)->city : 'all cities',
        'job_type' => ''
    );

    $results = array(
        'search_totals' => $search_totals,
        'search_results' => $search_results,
        'search_terms' => $search_terms
    );
    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      return response()->json(['status' => true, 'data' => $results], 200);
    } else {
      return View::make($this->theme.'.search.list_normal')
          ->with('search_totals', $search_totals)
          ->with('search_results', $result)
          ->with('search_terms',$search_terms);
    }
  }

  public function searchByCategory($category){
    if ($category == 'all'){
      $category_name = Category::where('status',1);
      $search_results = Jobs::where('status',1);
    } else {
      $category_name = Category::find($category);
      $search_results = Jobs::SearchByCategory(array($category));
      $search_results->where('status','=',1);
    }

    $result = $search_results->take(10)->get();
    $search_totals = $search_results->count();
    $search_terms = array(
        'term' => '',
        'city' => '',
        'job_type' => '',
        'category' => ($category != 'all') ? $category_name->name : 'all categories'
    );


    $results = array(
        'search_totals' => $search_totals,
        'search_results' => $result,
        'search_terms' => $search_terms
    );
    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      return response()->json(['status' => true, 'data' => $results], 200);
    } else {

      return View::make($this->theme.'.search.list_normal')
          ->with('search_totals', $search_totals)
          ->with('search_results', $result)
          ->with('search_terms', $search_terms);
    }
  }

  public function search($term,$city,$job_type){


    if ($term != 'false'){

      $company = Company::where('name', 'LIKE', '%'.$term.'%')->get();
      $cIds = array();
      foreach($company as $c){
        $cIds[] = $c->id;
      }


      if(preg_match('/^(["\']).*\1$/m', $term)) {
        $search_results = Jobs::
        where(function ($query) use ($term, $cIds) {
          $query->orWhere('title', $term);
          foreach($cIds as $company_id){
            $query->orWhere('company_id', $company_id);
          }
          $query->orWhere('the_job', $term);
        });

      } else {

        $search_results = Jobs::
        where(function ($query) use ($term, $cIds) {
          $query->orWhere('title', 'LIKE', '%' . $term . '%');
          foreach($cIds as $company_id) {
            $query->orWhere('company_id', $company_id);
          }
          $query->orWhere('the_job', 'LIKE', '%' . $term . '%');
          //$query->orWhere('the_candidate', 'LIKE', '%' . $term . '%');
        });

      }

    } else {
      $search_results = Jobs::where('status',1);
    }

    /* Check for city */
    if ($city != 'all'){
      $location = Location::where('id',(int)$city)->get()->first();
      $search_results->where('location_id',$city);
      $allCitiesInState = Location::where('state',$location->state)->get();
      foreach($allCitiesInState as $states){
        $search_results->orWhere('location_id', $states->id);
      }
    }

    /* Check for job type */
    if ($job_type != 'all'){
      $search_results->Where('job_type', $job_type);
    }

    $date = new \DateTime;
    $search_results->where('status',1)->where('expiration_date','>',$date->format('Y-m-d H:i:s'));


    $result = $search_results->take(15)
        ->orderBy('featured', 'desc')
        ->orderBy('title','desc')
        ->orderBy('the_job', 'desc')
        ->orderBy('the_candidate', 'desc')
        ->get();

    $search_totals = $search_results->count();


    $search_terms = array(
        'term' => $term,
        'city' => ($city != 'all') ? Location::find($city)->name : 'all cities',
        'job_type' => $job_type
    );


    $returnHTML = view($this->theme.'.search.list')
        ->with('search_totals', $search_totals)
        ->with('search_results', $result)
        ->with('search_terms',$search_terms)
        ->render();

    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

      return response()->json(['status' => true, 'data' => $returnHTML], 200);

    } else {
      return View::make($this->theme.'.search.list_normal')
          ->with('search_totals', $search_totals)
          ->with('search_results', $result)
          ->with('search_terms',$search_terms);
    }
  }

}
