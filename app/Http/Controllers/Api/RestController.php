<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Jobs;
use App\User;
use App\Config;
use App\Rss;
use App\Page;
use App\Company;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use SoapBox\Formatter\Formatter;

class RestController extends Controller {


  public function __construct(){

  }

  private function toJson($data){
    if (!$data)
      return response()->json(['satus' => 'false', 'error' => 'No data found or invalid method'],404);
    return response()->json(['status' => 'true', 'data' => $data],200);
  }

  private function toXml($data){
    //header( 'Content-Type: text/xml; charset=utf-8' );
    //echo $data;
    return Response::view($data, '200')->header('Content-Type', 'text/xml');
  }


  /*jobs xml*/
  public function jobsXml(){
    $jobs = Jobs::where('status',1)->get();

    return Response::view('frontend.modules.jobs-xml', compact('jobs'))
        ->header('Content-Type', 'application/xml');
  }

  /* Users */
  public function Users(){
    return $this->toJson(User::all());
  }

  public function showUser($user_id){
    return $this->toJson(User::find($user_id));
  }

  /* General Config */
  public function Config(){
    return $this->toJson(Config::all());
  }

  /* RSS Feeds */
  public function Rss(){
    return $this->toJson(Rss::all());
  }
  public function showRss($rss_id){
    return $this->toJson(Rss::find($rss_id));
  }

  /* Static Pages */
  public function Page(){
    return $this->toJson(Page::all());
  }
  public function showPage($page_id){
    return $this->toJson(Page::find($page_id));
  }

  /* Jobs */
  public function unifyJobs($job_id = null){
    $rv = [];
    $jobs = ($job_id == null) ? Jobs::all() : Jobs::find($job_id);

    if ($jobs == null){
      return false;
    }

    if ($job_id == null){
      foreach($jobs as $_job){
        $rv[] = array(
            'id'          => $_job->id,
            'title'       => $_job->title,
            'Company'     => $_job->companies->name,
            'Description' => $_job->the_job,
            'Candidate'   => $_job->the_candidate,
            'Location'    => $_job->location,
            'JobType'     => $_job->job_type,
            'Category'    => $_job->categories->name,
            'app_by'      => $_job->app_by
        );
      }
    } else {
      $rv[] = array(
          'id'            => $jobs->id,
          'title'         => $jobs->title,
          'Company'       => $jobs->companies->name,
          'Description'   => $jobs->the_job,
          'Candidate'     => $jobs->the_candidate,
          'Location'      => $jobs->location,
          'JobType'       => $jobs->job_type,
          'Category'      => $jobs->categories->name,
          'app_by'        => $jobs->app_by
      );
    }
    return $rv;

  }
  public function Jobs(){
    return $this->toJson($this->unifyJobs());
  }
  public function showJob($job_id){
    return $this->toJson($this->unifyJobs($job_id));
  }

}
