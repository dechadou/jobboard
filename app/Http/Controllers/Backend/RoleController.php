<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use App\Http\Requests\RoleFormRequest;
use Illuminate\Support\Facades\Auth;

class RoleController extends AdminController {


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('admin.role.list')->with('roles', Role::all());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
    $this->middleware('auth');
		return View::make('admin.role.add');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(RoleFormRequest $request)
	{
    $role              = new Role();
    $role->name        = $request->input('name');
    $role->description = $request->input('description');

    $role->save();

    return Redirect::to('admin/roles/edit/'.$role->id)
        ->with('message', 'Your role has been created!');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return View::make('admin.role.edit')->with('roles',Role::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(RoleFormRequest $request, $id)
	{
    $role              = Role::find($id);
    $role->name        = $request->input('name');
    $role->description = $request->input('description');

    $role->save();

    return Redirect::to('admin/roles/edit/'.$role->id)
        ->with('message', 'Your role has been updated!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
    $blocked_roles = [1,2,5];
    if (!in_array($id,$blocked_roles)){
      $role = Role::find($id);
      $role->destroy($id);

      return Redirect::to('admin/roles')
          ->with('message', 'Your Role has been deleted!');
    } else {
      return Redirect::to('admin/roles')
          ->with('message', 'Your Cannot delete this Role!');
    }
	}

}
