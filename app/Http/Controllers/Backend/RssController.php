<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Rss;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use App\Http\Requests\RssFormRequest;

class RssController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
    return View::make('admin.rss.list')->with('rss', Rss::all());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
    return View::make('admin.rss.add');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(RssFormRequest $request)
	{
    $rss              = new Rss();
    $rss->name        = $request->input('name');
    $rss->url         = $request->input('url');
    $rss->description = $request->input('description');
    $rss->active      = $request->input('active');

    $rss->save();

    return Redirect::to('admin/rss/edit/'.$rss->id)
        ->with('message', 'Your rss feed has been created!');

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
    return View::make('admin.rss.show')->with('rss', Rss::find($id));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
    $feed = Rss::find($id);
    return View::make('admin.rss.edit')->with('rss', $feed);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(RssFormRequest $request, $id)
	{
    $rss              = Rss::find($id);
    $rss->name        = $request->input('name');
    $rss->url         = $request->input('url');
    $rss->description = $request->input('description');
    $rss->active      = $request->input('active');

    $rss->save();

    return Redirect::to('admin/rss/edit/'.$rss->id)
        ->with('message', 'Your RSS feed has been updated!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$rss = Rss::find($id);
    $rss->destroy($id);

    return Redirect::to('admin/rss')
        ->with('message', 'Your RSS feed has been created!');
	}

}
