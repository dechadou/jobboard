<?php namespace App\Http\Controllers\Backend;

use App\Company;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Jobs;
use App\Transaction;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class AdminController extends Controller {


  public function __construct(){
    $this->middleware('auth');

    $user = Auth::user();
    View::share('logged_user', $user);
  }


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
    $recruiters     = Company::all()->count();
    $transactions  = Transaction::all();
    $jobs       = Jobs::all()->count();
    $members    = User::where('role_id',5)->get()->count();
        $sales = 0;
        if (!$transactions->isEmpty()){
            foreach($transactions as $sale){
                $data = json_decode($sale->data);
                $sales += $data->amount;
            }
        }

    return View::make('admin.home')
        ->with('recruiters',$recruiters)
        ->with('sales', $sales)
        ->with('jobs', $jobs)
        ->with('members', $members);

	}

}
