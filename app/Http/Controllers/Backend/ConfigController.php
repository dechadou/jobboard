<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\ConfigFormRequest;
use App\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;


class ConfigController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
    return View::make('admin.config.list')->with('config', Config::all());
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(ConfigFormRequest $request)
	{
    $fields = $request->except('_token');
    foreach($fields as $k => $v) {
      // check for images
      if ($k == 'site_logo' || $k == 'site_favicon' || $k == 'home_background')
      {
        $file = $request->file($k);
        if ($file != null){
          $imageName = md5($file->getClientOriginalName()) . '.' .$file->getClientOriginalExtension();
          $file->move(base_path() . '/public/uploads/general/', $imageName);
          DB::table('configs')->where('name', $k)->update(array('value' => $imageName));
        }

      } else if ($k == 'theme') {
        $all = Config::all();
        foreach($all as $t){
          if ($t->name == 'theme' || $t->name == 'theme_selected'){
            $t->name = 'theme';
            $t->save();
          }
        }

        $theme = Config::find($v);
        $theme->name = 'theme_selected';
        $theme->save();
      } else {
        DB::table('configs')
            ->where('id', $k)
            ->update(array('value' => $v));
      }

    }

    return Redirect::to('admin/config')
        ->with('message', 'The Config has been updated');
	}



}
