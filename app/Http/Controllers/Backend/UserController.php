<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserFormRequest;
use App\Role;
use App\User;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class UserController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('admin.users.list')->with('users',User::all());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
    $roles = Role::all();
    foreach($roles as $role) {
      $r[$role->id] = $role->name;
    }
		return View::make('admin.users.add')->with(array('roles' => $r));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(UserFormRequest $request)
	{
    $role = Role::find($request->input('role'));
    $file = $request->file('avatar');
    if ($file != null) {
      $imageName = md5($file->getClientOriginalName()) . '.' . $file->getClientOriginalExtension();
      $file->move(base_path() . '/public/uploads/user_avatar/', $imageName);
    }

    $user = new User();

    $user->name = $request->input('name');
    $user->email = $request->input('email');
    $user->role_id = $role->id;
    if ($file != null) {
      $user->avatar = $imageName;
    }
    $user->password = Hash::make($request->input('password'));

    $user->save();



    return View::make('admin.users.show')->with('users',$user);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return View::make('admin.users.show')->with('users',User::find($id));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = User::find($id);
		$roles = Role::all();
		foreach($roles as $role) {
			$r[$role->id] = $role->name;
		}
		return View::make('admin.users.edit')->with(array('user' => $user, 'roles' => $r));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(UserFormRequest $request, $id)
	{

		$role = Role::find($request->input('role'));
		$user = User::find($id);

    $file = $request->file('avatar');
    if ($file != null){
      $imageName = md5($file->getClientOriginalName()) . '.' .$file->getClientOriginalExtension();
      $file->move(base_path() . '/public/uploads/user_avatar/'.$id, $imageName);
    }

		$user->name = $request->input('name');
		$user->email = $request->input('email');
		$user->role_id = $role->id;
    if ($file != null){
      $user->avatar  = $imageName;
    }

		if($request->input('password') != null) {
			$user->password = Hash::make($request->input('password'));
		}
		$user->save();


		return View::make('admin.users.show')->with('users',$user);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
