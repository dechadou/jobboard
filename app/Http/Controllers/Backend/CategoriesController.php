<?php namespace App\Http\Controllers\Backend;

use App\Category;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class CategoriesController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('admin.categories.list')->with('categories', Category::all());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.categories.add');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
    $v = Validator::make($request->all(), [
        'name'        => 'required'
    ]);

    if ($v->fails()) {
      return redirect()
          ->back()
          ->withErrors($v->errors());
    }

    $category = new Category();
    $category->name = $request->input('name');
    $category->save();

    return Redirect::to('admin/categories/show/'.$category->id)
        ->with('category',$category)
        ->with('message', 'Your category has been created!');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
    $category = Category::findOrFail($id);

    return Redirect::to('admin/categories/edit/'.$id);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
    return View::make('admin.categories.edit')
        ->with('category', Category::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
  public function update(Request $request, $id)
  {
    $v = Validator::make($request->all(), [
        'name'        => 'required'
    ]);

    if ($v->fails()) {
      return redirect()
          ->back()
          ->withErrors($v->errors());
    }

    $category = Category::find($id);
    $category->name = $request->input('name');
    $category->save();

    return Redirect::to('admin/categories/show/'.$category->id)
        ->with('category',$category)
        ->with('message', 'Your category has been updated!');
  }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
    $category = Category::findOrFail($id);

    $category->delete();

    return Redirect::to('admin/categories');
	}

}
