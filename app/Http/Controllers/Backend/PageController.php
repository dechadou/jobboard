<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Page;
use App\Http\Requests\PageFormRequest;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class PageController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('admin.page.list')->with('page', Page::all());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
    return View::make('admin.page.add');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(PageFormRequest $request)
	{
    $page            = new Page();
    $page->title     = $request->input('title');
    $page->content   = $request->input('content');
        $pageSlug = str_replace(' ','-',$request->input('title'));
        $page->slug      = strtolower($pageSlug);
    $page->status    = $request->input('active');

    $page->save();

    return Redirect::to('admin/pages/edit/'.$page->id)
        ->with('message', 'Your page has been created!');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return View::make('admin.page.show')->with('page',Page::find($id));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
    return View::make('admin.page.edit')->with('page',Page::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(PageFormRequest $request, $id)
	{
    $page            = Page::find($id);
    $page->title     = $request->input('title');
    $page->content   = $request->input('content');
    $page->status    = $request->input('active');
        $pageSlug = str_replace(' ','-',$request->input('title'));
        $page->slug      = strtolower($pageSlug);
    $page->save();

    return Redirect::to('admin/pages/edit/'.$page->id)
        ->with('message', 'Your page has been updated!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
    $page = Page::find($id);
    $page->destroy($id);
        return View::make('admin.page.list')->with('page', Page::all());
	}

}
