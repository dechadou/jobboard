<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyFormRequest;
use App\Http\Requests\JobFormRequest;
use App\Company;
use App\Jobs;
use App\Location;
use App\Category;
use App\JobsCategory;
use App\Transaction;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class CompaniesController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('admin.companies.list')->with('companies',Company::all());
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return View::make('admin.companies.show')->with('companies',Company::find($id));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return View::make('admin.companies.edit')->with('companies' ,Company::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(CompanyFormRequest $request, $id)
	{
		$company = Company::find($id);

    $file = $request->file('logo');
    if ($file != null){
      $imageName = md5($file->getClientOriginalName()) . '.' .$file->getClientOriginalExtension();
      $file->move(base_path() . '/public/uploads/company_logo/', $imageName);
    }

    $company->name = $request->input('name');
    $company->description = $request->input('description');

    if ($file != null){
      $company->logo  = $imageName;
    }

    $company->save();


		return View::make('admin.companies.show')->with('companies',$company);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function editJob($company_id,$job_id)
  {

    /*

    'locations' => array(
      'alabama' => array('citi','city2','city3'),
      'state2'  => array()
    )

     */
    $geo = [];
    $locations = Location::all();
    foreach($locations as $location){
      $geo[$location->state][$location->id] = $location->city;
    }


    return View::make('admin.companies.editJob')
        ->with('company',Company::find($company_id))
        ->with('job' ,Jobs::find($job_id))
        ->with('locations',$geo)
        ->with('categories', Category::lists('name','id'));

  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function updateJob(JobFormRequest $request, $company_id, $job_id)
  {

    $job = Jobs::find($job_id);


    $job->title           = $request->input('title');
    $job->the_job         = $request->input('the_job');
    $job->the_candidate   = $request->input('the_candidate');
    $job->location_id     = $request->input('location_id');
    $job->job_type        = $request->input('job_type');
    $job->app_by          = $request->input('app_by');
    $job->app_by_text     = $request->input('app_by_text');
    $job->featured        = $request->input('featured');
    $job->sticky          = $request->input('sticky');
    $job->status          = $request->input('status');

    $job->save();

    /* Delete all related categories */
    foreach($job->categories as $c){
      $detach_ids[] = $c->id;
    }
    $job->categories()->detach($detach_ids);

    /* add new categories */
    $categories = $request->input('category_id');
    $job->categories()->attach($categories);



    return redirect('admin/companies/show/'.$company_id)->with('companies',Company::find($company_id));
  }



}
