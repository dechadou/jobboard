<?php namespace App\Http\Controllers\Backend;

use App\Industry;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class IndustriesController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('admin.industries.list')->with('industries', Industry::all());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.industries.add');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
    $v = Validator::make($request->all(), [
        'name'        => 'required'
    ]);

    if ($v->fails()) {
      return redirect()
          ->back()
          ->withErrors($v->errors());
    }

    $industry = new Industry();
    $industry->name = $request->input('name');
    $industry->save();

    return Redirect::to('admin/industries/show/'.$industry->id)
        ->with('industry',$industry)
        ->with('message', 'Your industry has been created!');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
    $industry = Industry::findOrFail($id);

    return Redirect::to('admin/industries/edit/'.$id);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
    return View::make('admin.industries.edit')
        ->with('industry', Industry::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
  public function update(Request $request, $id)
  {
    $v = Validator::make($request->all(), [
        'name'        => 'required'
    ]);

    if ($v->fails()) {
      return redirect()
          ->back()
          ->withErrors($v->errors());
    }

    $industry = Industry::find($id);
    $industry->name = $request->input('name');
    $industry->save();

    return Redirect::to('admin/industries/show/'.$industry->id)
        ->with('industry',$industry)
        ->with('message', 'Your industry has been updated!');
  }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
    $industry = Industry::findOrFail($id);

    $industry->delete();

    return Redirect::to('admin/industries');
	}

}
