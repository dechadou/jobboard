<?php namespace App\Http\Controllers\Backend;

use App\Coupons;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use App\TokenGenerator;
use App\Http\Requests\CouponFormRequest;

class CouponsController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
    $types = array(
        0=>'Money',1=>'Percentage',2=>'Free Publication',4=>'Free Featured Publication'
    );
		return View::make('admin.coupons.list')->with('coupons', Coupons::all())->with('discount_types',$types);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
    $token = new TokenGenerator('coupon_');
		return View::make('admin.coupons.add')->with('token',$token);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CouponFormRequest $request)
	{
    $coupon                 = new Coupons();
    $coupon->description    = $request->input('description');
    $coupon->token          = $request->input('token');
    $coupon->discount_type  = $request->input('discount_type');
    $coupon->discount       = $request->input('discount');
    $coupon->status         = $request->input('status');
    $coupon->user_id        = null;

    $coupon->save();

    return Redirect::to('admin/coupons');

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return View::make('admin.coupons.edit')->with('coupon', Coupons::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(CouponFormRequest $request, $id)
	{
    $coupon                 = Coupons::find($id);
    $coupon->description    = $request->input('description');
    $coupon->discount_type  = $request->input('discount_type');
    $coupon->token          = $request->input('token');
    $coupon->discount       = $request->input('discount');
    $coupon->status         = $request->input('status');
    $coupon->user_id        = null;

    $coupon->save();

    return Redirect::to('admin/coupons');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
    $coupon = Coupons::find($id);
    $coupon->destroy($id);
    return Redirect::to('admin/coupons');
	}

}
