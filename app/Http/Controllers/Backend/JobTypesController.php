<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\JobType;
use Illuminate\Http\Request;
use Illuminate\Queue\Jobs\Job;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class JobTypesController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('admin.jobtypes.list')->with('types',JobType::all());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.jobtypes.add');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
    $v = Validator::make($request->all(), [
        'name'        => 'required'
    ]);

    if ($v->fails()) {
      return redirect()
          ->back()
          ->withErrors($v->errors());
    }

    $jobType = new JobType();
    $jobType->name = $request->input('name');
    $jobType->save();

    return Redirect::to('admin/jobtypes/show/'.$jobType->id)
        ->with('jobType',$jobType)
        ->with('message', 'Your job type has been created!');

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
    $jobType = JobType::findOrFail($id);

    return Redirect::to('admin/jobtypes/edit/'.$id);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
    return View::make('admin.jobtypes.edit')
        ->with('jobtype', JobType::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
    $v = Validator::make($request->all(), [
        'name'        => 'required'
    ]);

    if ($v->fails()) {
      return redirect()
          ->back()
          ->withErrors($v->errors());
    }

    $jobType = JobType::find($id);
    $jobType->name = $request->input('name');
    $jobType->save();

    return Redirect::to('admin/jobtypes/show/'.$jobType->id)
        ->with('jobType',$jobType)
        ->with('message', 'Your job type has been updated!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
    $jobTypes = JobTYpe::findOrFail($id);

    $jobTypes->delete();

    return Redirect::to('admin/jobtypes');
	}

}
