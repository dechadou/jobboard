<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Pricing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class PricingController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('admin.pricing.list')->with('pricing',Pricing::all());
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return View::make('admin.pricing.edit')->with('pricing',Pricing::find($id));
	}

  /**
   * Update the specified resource in storage.
   *
   * @param Request $request
   * @param  int $id
   * @return Response
   */
	public function update(Request $request, $id)
	{
		$pricing = Pricing::find($id);

    $pricing->value = $request->input('value');
    $pricing->save();

    return Redirect::to('admin/pricing');
	}


}
