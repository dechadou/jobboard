<?php namespace App\Http\Controllers\Backend;

use ZipArchive;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Chumper\Zipper\Facades\Zipper;
use Illuminate\Http\Request;
use App\Http\Requests\ConfigFormRequest;
use App\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;


class ThemesController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
    $themes = Config::where('name','theme')->orWhere('name','theme_selected')->get();
    return View::make('admin.themes.list')->with('themes', $themes);
	}

  public function create(){
    return View::make('admin.themes.add');
  }

  public function destroy($id){
    $theme = Config::find($id);
    $theme->destroy($id);
    return Redirect::to('admin/themes');
  }

  public function store(Request $request){
    $theme = new Config();
    $theme->name = 'theme';
    $theme->value = $request->input('theme_name');
    $theme->description = $request->input('theme_description');

    $theme_file = $request->file('theme_file');
    if ($theme_file != null){
      $fileName = $theme_file->getClientOriginalName();
      $tmpFile = $theme_file->move(base_path() . '/public/temp', $fileName);
    }
    $extractTo = base_path().'/resources/views/themes/'.$theme->value.'/';

    $zip = new ZipArchive();
    $zip_opened = $zip->open(base_path() .'/public/temp/'.$fileName, ZipArchive::CREATE);

    if ($zip_opened === TRUE) {
      $zip->extractTo($extractTo);
      $zip->close();
      $theme->save();
      unset($tmpFile);
      return Redirect::to('admin/themes')
          ->with('message', 'The theme has been added');
    } else {
      return Redirect::to('admin/themes')
          ->with('message', 'Ops! there was an error uploading your file');
    }

  }
}
