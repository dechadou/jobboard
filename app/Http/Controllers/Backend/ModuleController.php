<?php namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ModuleFormRequest;
use App\Module;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

class ModuleController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('admin.module.list')->with('modules', Module::all());
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return View::make('admin.module.edit')->with('module', Module::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(ModuleFormRequest $request, $id)
	{
    $module                 = Module::find($id);
    $module->name          = $request->input('name');
    $module->description    = $request->input('description');
    $module->data = json_encode($request->input('data'));
    $module->status         = $request->input('active');

    $module->save();

    return Redirect::to('admin/modules/edit/'.$module->id)
        ->with('message', 'Your Module has been updated!');
	}



}
