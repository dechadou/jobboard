<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(array('prefix' => '/'), function()
{
  Route::get('/', ['as'=> 'home','uses' => 'FrontEnd\HomeController@index']);

  /*login*/
  Route::get('login', 'FrontEnd\LoginController@index');
  Route::get('login/service/{service}', 'FrontEnd\LoginController@loginWith');
  Route::post('login/auth', 'FrontEnd\LoginController@auth');
  Route::get('login/forgot', 'FrontEnd\LoginController@forgotPassword');
  Route::post('login/forgot/send', 'FrontEnd\LoginController@createToken');
  Route::post('login/checkEmail', 'FrontEnd\LoginController@checkEmail');
  Route::get('password/reset/{token}','FrontEnd\LoginController@resetPassword');
  Route::post('password/confirm', 'FrontEnd\LoginController@passwordChange');
  Route::get('login/register', 'FrontEnd\LoginController@register');
  Route::get('login/create', 'FrontEnd\LoginController@create');
  Route::post('login/create/{type}', 'FrontEnd\LoginController@createAccount');
  Route::get('login/create', 'FrontEnd\LoginController@create');
  Route::get('login/success/{type}/{id}', 'FrontEnd\LoginController@success');
  Route::get('logout', 'FrontEnd\LoginController@logOut');

  /* Common Pages */
  Route::get('categories', 'FrontEnd\CommonPagesController@categories');
  Route::get('cities', 'FrontEnd\CommonPagesController@cities');
  Route::get('employer/{employer_name}', 'FrontEnd\CommonPagesController@employer');
  Route::get('industry', 'FrontEnd\CommonPagesController@industry');
  Route::get('jobs/types/{type}', 'FrontEnd\CommonPagesController@jobTypes');

  /* Jobs */
  Route::get('jobs', 'FrontEnd\JobsController@index');
  Route::get('jobs/view/{id}/{slug}', 'FrontEnd\JobsController@show');
  Route::get('jobs/plan', 'FrontEnd\JobsController@selectPlan');
  Route::get('jobs/save', 'FrontEnd\JobsController@saveNowPostLater');
  Route::get('jobs/post', 'FrontEnd\JobsController@create');
  Route::post('jobs/add', 'FrontEnd\JobsController@store');
  Route::post('jobs/checkout', 'FrontEnd\JobsController@checkout');
  Route::post('jobs/checkout/save', 'FrontEnd\JobsController@saveNowPostLaterCheckout');
  Route::get('jobs/checkout/makeFeatured/{job_id}', 'FrontEnd\JobsController@checkoutFeatured');
  Route::post('jobs/checkout/makeFeatured/step2', 'FrontEnd\JobsController@checkoutFeaturedStep2');
  Route::get('jobs/checkout/renewal/{job_id}', 'FrontEnd\JobsController@renewal');
  Route::post('jobs/checkout/redeem', 'FrontEnd\JobsController@redeemCoupon');
  Route::post('jobs/checkout/step2', 'FrontEnd\JobsController@checkoutStep2');
  Route::get('jobs/checkout/success', 'FrontEnd\JobsController@success');
  Route::post('jobs/checkout/bulkPosts', 'FrontEnd\JobsController@bulkPosts');
  Route::post('jobs/view/{id}/apply', 'FrontEnd\ProfileController@addJob');
  Route::get('jobs/loadMore/{updated_at}', 'FrontEnd\JobsController@loadMore');

  /* Search */
  Route::get('search/{term}/{city}/{job_type}', 'FrontEnd\SearchController@search');
  Route::get('search/cities/{city}', 'FrontEnd\SearchController@searchByCity');
  Route::get('search/categories/{category}', 'FrontEnd\SearchController@searchByCategory');
  Route::get('search/industries/{industry}', 'FrontEnd\SearchController@searchByIndustry');

  /* Other */
  Route::get('countries/list/','FrontEnd\CountryController@find');
  Route::get('locations/list/','FrontEnd\CountryController@findLocation');
  Route::get('popular_links',function(){ echo 'Falta diseño'; });
  Route::get('faq',function(){ echo 'Falta diseño'; });
  Route::get('contact', 'FrontEnd\ContactController@index');
  Route::get('nodesign',function(){ echo 'Falta diseño'; });
  Route::get('loader', 'FrontEnd\BaseController@loader');

  /* Static Pages */
  Route::get('pages/{slug}','FrontEnd\PageController@index');

  /* Profile */
  Route::group(array('prefix' => '/profile','middleware' => 'CheckLogged'), function(){
    Route::get('/', 'FrontEnd\ProfileController@index');
    Route::get('applied', 'FrontEnd\ProfileController@index');
    Route::get('favourite','FrontEnd\ProfileController@favourite');
    Route::get('preferences','FrontEnd\ProfileController@preferences');
    Route::post('updateAlerts','FrontEnd\ProfileController@updateAlerts');
    Route::get('files','FrontEnd\ProfileController@files');
    Route::get('info','FrontEnd\ProfileController@info');
    Route::post('update', 'FrontEnd\ProfileController@update');
    Route::post('addFiles', 'FrontEnd\ProfileController@addFiles');
    Route::post('addFilesFromDropbox', 'FrontEnd\ProfileController@addFilesFromDropbox'); // Dropbox
    Route::post('addFilesFromHD', 'FrontEnd\ProfileController@addFilesFromHD'); // Dropbox
    Route::get('removeFiles/{file_id}', 'FrontEnd\ProfileController@removeFiles');
    Route::get('fav_add/{job_id}', 'FrontEnd\ProfileController@addFav');
    Route::get('fav_remove/{id}', 'FrontEnd\ProfileController@removeFav');
    Route::post('apply/{job_id}', 'FrontEnd\ProfileController@addJob');
    Route::post('updateAvatar', 'FrontEnd\ProfileController@updateAvatar');
    Route::post('updateProfile', 'FrontEnd\ProfileController@updateProfile');
  });

  /* Company Dashboard*/
  Route::group(array('prefix' => '/company', 'middleware' => 'CheckLogged'), function(){
    Route::get('/', 'FrontEnd\CompanyProfileController@index');
    Route::get('manage_jobs', 'FrontEnd\CompanyProfileController@manage_jobs');
    Route::get('job_applications', 'FrontEnd\CompanyProfileController@job_applications');
    Route::get('job_analytics', 'FrontEnd\CompanyProfileController@job_analytics');
    Route::get('featured_jobs', 'FrontEnd\CompanyProfileController@featured_jobs');
    Route::get('saved_drafts', 'FrontEnd\CompanyProfileController@saved_drafts');
    Route::get('job/{id}/{action}', 'FrontEnd\CompanyCardController@doAction');
    Route::post('job/{id}/store', 'FrontEnd\JobsController@update');
    Route::post('updateLogo', 'FrontEnd\CompanyProfileController@updateLogo');
    Route::post('updateProfile', 'FrontEnd\CompanyProfileController@updateProfile');
    Route::get('billing', 'FrontEnd\CompanyProfileController@billingInformation');
    Route::post('updateBillingInfo/{id}', 'FrontEnd\CompanyProfileController@updateBillingInfo');
    Route::post('addBillingInfo', 'FrontEnd\CompanyProfileController@addBillingInfo');
    Route::get('destroyBillingInfo/{id}', 'FrontEnd\CompanyProfileController@destroyBillingInfo');
  });

  /* Share */
  //Route::get('share/{service}', 'FrontEnd\ShareController@share');
  Route::group(array('prefix' => '/share'), function(){

    Route::get('twitter/{url}/{description}', function(){
      return redirect(Share::load(Route::input('url'), Route::input('description'))->twitter());
    });

    Route::get('facebook/{id}', function(){
      $job = \App\Jobs::find(Route::input('id'));
      $url = htmlentities(getPermaLink($job));
      return redirect(Share::load($url, $job->title)->facebook());
    });

    Route::get('gplus/{url}/{description}', function(){
      return redirect(Share::load(Route::input('url'), Route::input('description'))->gplus());
    });

  });


});



Route::group(array('prefix' => 'admin'), function()
{
  Route::get('/',[
      'middleware' => ['auth', 'roles'],
      'uses' => 'Backend\AdminController@index',
      'roles' => ['Root', 'Administrator']
  ]);

  /* Themes Section */
  Route::get('themes', 'Backend\ThemesController@index');
  Route::get('themes/add', 'Backend\ThemesController@create');
  Route::post('themes/add/store', 'Backend\ThemesController@store');
  Route::get('themes/edit/{id}', 'Backend\ThemesController@edit');
  Route::post('themes/edit/{id}/update', 'Backend\ThemesController@update');
  Route::get('themes/delete/{id}', 'Backend\ThemesController@destroy');

  /* Job Types */
  Route::get('jobtypes', 'Backend\JobTypesController@index');
  Route::get('jobtypes/add', 'Backend\JobTypesController@create');
  Route::get('jobtypes/show/{id}', 'Backend\JobTypesController@show');
  Route::post('jobtypes/add/store', 'Backend\JobTypesController@store');
  Route::get('jobtypes/edit/{id}', 'Backend\JobTypesController@edit');
  Route::post('jobtypes/edit/{id}/update', 'Backend\JobTypesController@update');
  Route::get('jobtypes/delete/{id}', 'Backend\JobTypesController@destroy');

  /* Industries */
  Route::get('industries', 'Backend\IndustriesController@index');
  Route::get('industries/add', 'Backend\IndustriesController@create');
  Route::get('industries/show/{id}', 'Backend\IndustriesController@show');
  Route::post('industries/add/store', 'Backend\IndustriesController@store');
  Route::get('industries/edit/{id}', 'Backend\IndustriesController@edit');
  Route::post('industries/edit/{id}/update', 'Backend\IndustriesController@update');
  Route::get('industries/delete/{id}', 'Backend\IndustriesController@destroy');

  /* Categories */
  Route::get('categories', 'Backend\CategoriesController@index');
  Route::get('categories/add', 'Backend\CategoriesController@create');
  Route::get('categories/show/{id}', 'Backend\CategoriesController@show');
  Route::post('categories/add/store', 'Backend\CategoriesController@store');
  Route::get('categories/edit/{id}', 'Backend\CategoriesController@edit');
  Route::post('categories/edit/{id}/update', 'Backend\CategoriesController@update');
  Route::get('categories/delete/{id}', 'Backend\CategoriesController@destroy');

  /* Config Section */
  Route::get('config', 'Backend\ConfigController@index');
  Route::post('config/update', 'Backend\ConfigController@store');

  /* RSS */
  Route::get('rss', 'Backend\RssController@index'); // List of RSS
  Route::get('rss/add', 'Backend\RssController@create'); // Add new RSS
  Route::post('rss/add/store', 'Backend\RssController@store'); // Store new RSS Function
  Route::get('rss/edit/{id}', 'Backend\RssController@edit'); // Edit RSS
  Route::post('rss/edit/{id}/update', 'Backend\RssController@update'); // Edit RSS Function
  Route::get('rss/delete/{id}', 'Backend\RssController@destroy'); // Delete RSS Feed
  Route::get('rss/show/{id}', 'Backend\RssController@show'); // Show RSS Detail

  /* Users */
  Route::get('users', 'Backend\UserController@index');
  Route::get('users/add', ['middleware' => ['roles'], 'uses' => 'Backend\UserController@create', 'roles' => ['Root'] ]);
  Route::post('users/add/store', 'Backend\UserController@store');
  Route::get('users/edit/{id}', 'Backend\UserController@edit');
  Route::get('users/show/{id}', 'Backend\UserController@show');
  Route::post('users/edit/{id}/update', 'Backend\UserController@update');

  /* Coupons */
  Route::get('coupons', 'Backend\CouponsController@index');
  Route::get('coupons/add', ['middleware' => ['roles'], 'uses' => 'Backend\CouponsController@create', 'roles' => ['Root'] ]);
  Route::post('coupons/add/store', 'Backend\CouponsController@store');
  Route::get('coupons/edit/{id}', 'Backend\CouponsController@edit');
  Route::post('coupons/edit/{id}/update', 'Backend\CouponsController@update');
  Route::get('coupons/delete/{id}', 'Backend\CouponsController@destroy');

  /* Pricing */
  Route::get('pricing', 'Backend\PricingController@index');
  Route::get('pricing/edit/{id}', 'Backend\PricingController@edit');
  Route::post('pricing/edit/{id}/update', 'Backend\PricingController@update');

  /* Roles */
  Route::get('roles',['middleware' => ['roles'], 'uses' => 'Backend\RoleController@index','roles' => ['Root'] ]);
  Route::get('roles/add',['middleware' => ['roles'], 'uses' => 'Backend\RoleController@create', 'roles' => ['Root'] ]);
  Route::post('roles/add/store', 'Backend\RoleController@store');
  Route::get('roles/edit/{id}', 'Backend\RoleController@edit');
  Route::post('roles/edit/{id}/update','Backend\RoleController@update');
  Route::get('roles/delete/{id}', 'Backend\RoleController@destroy');

  /* Static Pages */
  Route::get('pages', 'Backend\PageController@index');
  Route::get('pages/add', 'Backend\PageController@create');
  Route::post('pages/add/store', 'Backend\PageController@store');
  Route::get('pages/edit/{id}', 'Backend\PageController@edit');
  Route::post('pages/edit/{id}/update', 'Backend\PageController@update');
  Route::get('pages/delete/{id}', 'Backend\PageController@destroy');
  Route::get('pages/show/{id}', 'Backend\PageController@show');

  /* Modules */
  Route::get('modules', 'Backend\ModuleController@index');
  //Route::get('modules/add', 'Backend\PageController@create');
  //Route::post('pages/add/store', 'Backend\PageController@store');
  Route::get('modules/edit/{id}', 'Backend\ModuleController@edit');
  Route::post('modules/edit/{id}/update', 'Backend\ModuleController@update');
  //Route::get('pages/delete/{id}', 'Backend\PageController@destroy');
  //Route::get('pages/show/{id}', 'Backend\PageController@show');


  /* Companies */
  Route::get('companies', 'Backend\CompaniesController@index');
  Route::get('companies/edit/{id}', 'Backend\CompaniesController@edit');
  Route::post('companies/edit/{id}/update', 'Backend\CompaniesController@update');
  Route::get('companies/delete/{id}', 'Backend\CompaniesController@destroy');
  Route::get('companies/show/{id}', 'Backend\CompaniesController@show');
  Route::get('companies/{company_id}/jobs/edit/{job_id}', 'Backend\CompaniesController@editJob');
  Route::get('companies/{company_id}/jobs/delete/{job_id}', 'Backend\CompaniesController@destroyJob');
  Route::post('companies/{company_id}/jobs/update/{job_id}', 'Backend\CompaniesController@updateJob');
});


Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController'
]);


/* API */
Route::get('jobs-xml', 'Api\RestController@jobsXml');
Route::group(array('prefix' => 'rest'), function() {
  Route::get('users', 'Api\RestController@Users');
  Route::get('users/{user_id}', 'Api\RestController@showUser');
  Route::get('config', 'Api\RestController@config');
  Route::get('rss', 'Api\RestController@rss');
  Route::get('rss/{rss_id}', 'Api\RestController@showRss');
  Route::get('page', 'Api\RestController@page');
  Route::get('page/{page_id}', 'Api\RestController@showPage');
  Route::get('jobs', 'Api\RestController@jobs');
  Route::get('jobs/{job_id}', 'Api\RestController@showJob');
});
