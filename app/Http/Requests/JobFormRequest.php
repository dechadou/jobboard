<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class JobFormRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'title'  => 'required',
			'the_job' => 'required',
      'the_candidate' => 'required',
      'category_id' => 'required',
      'app_by_text' => 'required'
		];
	}
}
