<?php namespace App\Http\Middleware;
  /**
   * Created by PhpStorm.
   * User: rodrigo
   * Date: 14/5/15
   * Time: 12:04
   */

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckLogged{

  public function handle($request, Closure $next)
  {
    $user = Auth::user();
    if(!$user)
    {
      return redirect('/login');
    }
    return $next($request);
  }
}